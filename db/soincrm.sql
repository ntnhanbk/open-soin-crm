-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 05, 2015 at 01:51 PM
-- Server version: 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `soincrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `crm_attachments`
--

CREATE TABLE `crm_attachments` (
`id` bigint(20) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_type` varchar(20) DEFAULT NULL,
  `attachment_path` varchar(255) NOT NULL,
  `attachment` text NOT NULL,
  `date_added` int(10) DEFAULT NULL,
  `last_modified` int(10) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_campaigns`
--

CREATE TABLE `crm_campaigns` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_categories`
--

CREATE TABLE `crm_categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT '',
  `thumbnail` text,
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_comments`
--

CREATE TABLE `crm_comments` (
`id` bigint(20) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_type` varchar(20) DEFAULT NULL,
  `comment` text,
  `status` tinyint(4) DEFAULT '0',
  `date_added` int(10) DEFAULT NULL,
  `last_modified` int(10) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_contacts`
--

CREATE TABLE `crm_contacts` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `lead_id` int(11) NOT NULL DEFAULT '0',
  `img` varchar(255) DEFAULT '',
  `thumbnail` text,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `viber` varchar(255) NOT NULL,
  `whatsapp` varchar(255) NOT NULL,
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_countries`
--

CREATE TABLE `crm_countries` (
`id` int(11) NOT NULL,
  `iso2` char(2) DEFAULT NULL,
  `short_name` varchar(80) DEFAULT NULL,
  `long_name` varchar(80) DEFAULT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `un_member` varchar(12) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `cctld` varchar(5) DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT '0',
  `featured` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_deals`
--

CREATE TABLE `crm_deals` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `quote_id` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(5) NOT NULL DEFAULT 'USD',
  `amt` float NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_deliverables`
--

CREATE TABLE `crm_deliverables` (
`id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `description` text,
  `est_request` float NOT NULL DEFAULT '0',
  `est_quote` float NOT NULL DEFAULT '0',
  `est_deal` float NOT NULL DEFAULT '0',
  `est_order` float NOT NULL DEFAULT '0',
  `in_request` tinyint(4) DEFAULT '0',
  `in_quote` tinyint(4) NOT NULL DEFAULT '0',
  `in_deal` tinyint(4) NOT NULL DEFAULT '0',
  `in_order` tinyint(4) NOT NULL DEFAULT '0',
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_expenses`
--

CREATE TABLE `crm_expenses` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(5) NOT NULL DEFAULT 'USD',
  `amt` float NOT NULL DEFAULT '0',
  `receiver` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_invoices`
--

CREATE TABLE `crm_invoices` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(5) NOT NULL DEFAULT 'USD',
  `amt` float NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_leads`
--

CREATE TABLE `crm_leads` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_logs`
--

CREATE TABLE `crm_logs` (
`id` bigint(20) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(50) DEFAULT NULL,
  `access_controller` varchar(100) DEFAULT NULL,
  `access_method` varchar(100) DEFAULT NULL,
  `description` text,
  `date_added` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_metas`
--

CREATE TABLE `crm_metas` (
`id` bigint(20) NOT NULL,
  `object_id` int(11) DEFAULT '0',
  `object_type` varchar(255) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_milestones`
--

CREATE TABLE `crm_milestones` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `milestone` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_orders`
--

CREATE TABLE `crm_orders` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `deal_id` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(5) NOT NULL DEFAULT 'USD',
  `amt` float NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_pricing_payments`
--

CREATE TABLE `crm_pricing_payments` (
`id` int(11) NOT NULL,
  `token_key` varchar(255) NOT NULL,
  `payment_gateway` varchar(120) NOT NULL,
  `package_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `package_title` varchar(255) NOT NULL,
  `package_time` int(11) NOT NULL,
  `package_time_type` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `amt` decimal(18,2) NOT NULL,
  `currency` varchar(60) NOT NULL,
  `author_id` int(11) NOT NULL,
  `expired_date` int(11) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `date_added` int(11) NOT NULL,
  `confirm_date` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_products`
--

CREATE TABLE `crm_products` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_type` varchar(10) NOT NULL DEFAULT 'product',
  `img` varchar(255) DEFAULT '',
  `thumbnail` text,
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_quotes`
--

CREATE TABLE `crm_quotes` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `request_id` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(5) NOT NULL DEFAULT 'USD',
  `amt` float NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_receivers`
--

CREATE TABLE `crm_receivers` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT NULL,
  `thumbnail` longtext,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_requests`
--

CREATE TABLE `crm_requests` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_tasks`
--

CREATE TABLE `crm_tasks` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `tags` varchar(255) NOT NULL,
  `start_date` int(11) NOT NULL DEFAULT '0',
  `deadline` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL,
  `last_modified` int(11) NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_tokens`
--

CREATE TABLE `crm_tokens` (
`id` int(11) unsigned NOT NULL,
  `token` text NOT NULL,
  `type` varchar(60) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_added` int(10) NOT NULL,
  `expired_date` int(10) DEFAULT '0',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crm_users`
--

CREATE TABLE `crm_users` (
`id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` varchar(20) DEFAULT 'sub-admin',
  `secret_key` varchar(9) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `thumbnail` text NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_logo` longtext,
  `expired_date` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `crm_users`
--

INSERT INTO `crm_users` (`id`, `username`, `first_name`, `last_name`, `display_name`, `password`, `role`, `secret_key`, `email`, `img`, `thumbnail`, `company_name`, `company_logo`, `expired_date`, `date_added`, `last_modified`, `author_id`, `disabled`, `deleted`) VALUES
(1, 'admin', 'Soin', 'CRM', 'Soin CRM', '0fZTU0gzMjl7w', 'admin', '0fa031cb9', 'info@soinmedia.com', '', 'a:5:{s:9:"thumbnail";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:21:"255x255-9Mg3HG1lH.jpg";s:5:"width";i:255;s:6:"height";i:255;}s:10:"banner_img";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:21:"570x328-9Mg3HG1lH.jpg";s:5:"width";i:570;s:6:"height";i:328;}s:5:"small";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:21:"100x100-9Mg3HG1lH.jpg";s:5:"width";i:100;s:6:"height";i:100;}s:4:"mini";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:19:"50x50-9Mg3HG1lH.jpg";s:5:"width";i:50;s:6:"height";i:50;}s:4:"full";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:13:"9Mg3HG1lH.jpg";s:5:"width";i:793;s:6:"height";i:960;}}', 'Social Interactive Company Limited', 'a:1:{s:4:"full";a:4:{s:6:"folder";s:8:"2015/01/";s:8:"filename";s:13:"jGqYDgu3z.png";s:5:"width";i:223;s:6:"height";i:46;}}', 2678400, NULL, 1425545597, 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crm_attachments`
--
ALTER TABLE `crm_attachments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_campaigns`
--
ALTER TABLE `crm_campaigns`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_categories`
--
ALTER TABLE `crm_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_comments`
--
ALTER TABLE `crm_comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_contacts`
--
ALTER TABLE `crm_contacts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_countries`
--
ALTER TABLE `crm_countries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_deals`
--
ALTER TABLE `crm_deals`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_deliverables`
--
ALTER TABLE `crm_deliverables`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_expenses`
--
ALTER TABLE `crm_expenses`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_invoices`
--
ALTER TABLE `crm_invoices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_leads`
--
ALTER TABLE `crm_leads`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_logs`
--
ALTER TABLE `crm_logs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_metas`
--
ALTER TABLE `crm_metas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_milestones`
--
ALTER TABLE `crm_milestones`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_orders`
--
ALTER TABLE `crm_orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_pricing_payments`
--
ALTER TABLE `crm_pricing_payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_products`
--
ALTER TABLE `crm_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_quotes`
--
ALTER TABLE `crm_quotes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_receivers`
--
ALTER TABLE `crm_receivers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_requests`
--
ALTER TABLE `crm_requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_tasks`
--
ALTER TABLE `crm_tasks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_tokens`
--
ALTER TABLE `crm_tokens`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_users`
--
ALTER TABLE `crm_users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crm_attachments`
--
ALTER TABLE `crm_attachments`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_campaigns`
--
ALTER TABLE `crm_campaigns`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_categories`
--
ALTER TABLE `crm_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_comments`
--
ALTER TABLE `crm_comments`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_contacts`
--
ALTER TABLE `crm_contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_countries`
--
ALTER TABLE `crm_countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_deals`
--
ALTER TABLE `crm_deals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_deliverables`
--
ALTER TABLE `crm_deliverables`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_expenses`
--
ALTER TABLE `crm_expenses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_invoices`
--
ALTER TABLE `crm_invoices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_leads`
--
ALTER TABLE `crm_leads`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_logs`
--
ALTER TABLE `crm_logs`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_metas`
--
ALTER TABLE `crm_metas`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_milestones`
--
ALTER TABLE `crm_milestones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_orders`
--
ALTER TABLE `crm_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_pricing_payments`
--
ALTER TABLE `crm_pricing_payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_products`
--
ALTER TABLE `crm_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_quotes`
--
ALTER TABLE `crm_quotes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_receivers`
--
ALTER TABLE `crm_receivers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_requests`
--
ALTER TABLE `crm_requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_tasks`
--
ALTER TABLE `crm_tasks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_tokens`
--
ALTER TABLE `crm_tokens`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crm_users`
--
ALTER TABLE `crm_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
