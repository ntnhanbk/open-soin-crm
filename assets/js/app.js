$(document).ready(function () {
    init();
});

function init() {

    $('html').on('click', "#ObjectModal .btn-save", function() {
        var ele = $(this);
        var form_data = $('#ObjectModal form').serialize();
        var form_action = $('#ObjectModal form').attr('action');

        if (ele.hasClass('ajax-form')) {
            var options = {
                url: $('#ObjectModal form').data('action'),
                type: 'post',
                dataType: 'json',
                beforeSend: function()
                {
                    ele.addClass('disabled');
                },
                uploadProgress: function(event, position, total, percentComplete)
                {

                },
                success: function(result)
                {
                    if (result.status=='error'){
                        $('#ObjectModal .modal-status').html(result.error_messages);
                    }else{
                        $('#ObjectModal .modal-status').html(result.success_messages);
                        $('#ObjectModal .modal-body').html('');
                        $('#ObjectModal .modal-footer').html(result.action_group);
                    }
                },
                complete: function(response)
                {
                    ele.removeClass('disabled');
                },
                error: function()
                {

                }
            };

            $('#ObjectModal form').ajaxForm(options);
            $('#ObjectModal form').submit();
        }

        if (!ele.hasClass('ajax-form')) {
            $.ajax({
                type: 'post',
                url: form_action,
                data: form_data,
                dataType: 'json',
                beforeSend: function () {
                    ele.addClass('disabled');
                },
                complete: function () {
                    ele.removeClass('disabled');
                },
                success: function (result) {
                    if (result.status=='error'){
                        $('#ObjectModal .modal-status').html(result.error_messages);
                    }else{
                        $('#ObjectModal .modal-status').html(result.success_messages);
                        $('#ObjectModal .modal-body').html('');
                        $('#ObjectModal .modal-footer').html(result.action_group);
                    }
                }
            });
        }
    });

    $('html').on('click', ".btn-object-modal", function() {
        var modal_title = $(this).attr('modal-title');
        $('#ObjectModal .modal-title').html(modal_title);

        $('#ObjectModal .modal-status').html('');

        var modal_size = $(this).attr('modal-size');
        $('#ObjectModal .modal-dialog').removeClass('modal-lg');
        $('#ObjectModal .modal-dialog').removeClass('modal-sm');
        $('#ObjectModal .modal-dialog').addClass(modal_size);

        $('#ObjectModal .modal-body').html('<div class="text-center"><i class="fa fa-spin fa-spinner"></i> loading ...</div>');

        var modal_url = $(this).attr('modal-url');
        $.ajax({
            type: "GET",
            url: modal_url,
            dataType: "json",
            cache: false,
            crossDomain: "true",
            success: function (response) {
                $('#ObjectModal .modal-body').html(response.item_view);
                if (response.action_group != undefined)
                    $('#ObjectModal .modal-footer').html(response.action_group);
            },
            error: function (xhr, status) {

            }
        });


        $('#ObjectModal').modal();

        return false;
    });

    /*$(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });*/

}