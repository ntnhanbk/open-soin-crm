<?php

class DeliverablesController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page'] = "deliverables";
    }

    public function actionIndex($request_id = 0, $order_by = '', $order_asc = '', $p = 1)
    {
        if ($request_id == 0)
            $this->redirect(HelperUrl::baseUrl() . 'requests');

        $RequestModel = new RequestModel();
        $request = $RequestModel->get($request_id);

        $DeliverableModel = new DeliverableModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['request_id'] = $request_id;
        $args['parent_id'] = 0;

        $deliverables = $DeliverableModel->gets($args, 0);
        $total = $DeliverableModel->counts($args);

        $total_request = 0;
        $total_quote = 0;
        $total_deal = 0;
        $total_order = 0;

        foreach ($deliverables as $k => $v) {
            $args1 = array('parent_id' => $v['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
            $deliverables[$k]['subdeliverable-list'] = $DeliverableModel->gets($args1, 0);

            if ($deliverables[$k]['subdeliverable-list']) $deliverables[$k]['est_request'] = 0;
            if ($deliverables[$k]['subdeliverable-list']) $deliverables[$k]['est_quote'] = 0;
            if ($deliverables[$k]['subdeliverable-list']) $deliverables[$k]['est_deal'] = 0;
            if ($deliverables[$k]['subdeliverable-list']) $deliverables[$k]['est_order'] = 0;

            $tmp = $deliverables[$k];
            $total_request += $tmp['in_request'] * $tmp['est_request'];
            $total_quote += $tmp['in_quote'] * $tmp['est_quote'];
            $total_deal += $tmp['in_deal'] * $tmp['est_deal'];
            $total_order += $tmp['in_order'] * $tmp['est_order'];

            foreach ($deliverables[$k]['subdeliverable-list'] as $sk => $sv) {
                $args2 = array('parent_id' => $sv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] = $DeliverableModel->gets($args2, 0);

                if ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list']) $deliverables[$k]['subdeliverable-list'][$sk]['est_request'] = 0;
                if ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list']) $deliverables[$k]['subdeliverable-list'][$sk]['est_quote'] = 0;
                if ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list']) $deliverables[$k]['subdeliverable-list'][$sk]['est_deal'] = 0;
                if ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list']) $deliverables[$k]['subdeliverable-list'][$sk]['est_order'] = 0;

                $tmp = $deliverables[$k]['subdeliverable-list'][$sk];
                $total_request += $tmp['in_request'] * $tmp['est_request'];
                $total_quote += $tmp['in_quote'] * $tmp['est_quote'];
                $total_deal += $tmp['in_deal'] * $tmp['est_deal'];
                $total_order += $tmp['in_order'] * $tmp['est_order'];

                foreach ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] as $ssk => $ssv) {
                    $args3 = array('parent_id' => $ssv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                    $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk]['subdeliverable-list'] = $DeliverableModel->gets($args3, 0);

                    $tmp = $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk];
                    $total_request += $tmp['in_request'] * $tmp['est_request'];
                    $total_quote += $tmp['in_quote'] * $tmp['est_quote'];
                    $total_deal += $tmp['in_deal'] * $tmp['est_deal'];
                    $total_order += $tmp['in_order'] * $tmp['est_order'];
                }
            }
        }

        $this->viewData['total_request'] = $total_request;
        $this->viewData['total_quote'] = $total_quote;
        $this->viewData['total_deal'] = $total_deal;
        $this->viewData['total_order'] = $total_order;

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "deliverables/index/request_id/$request_id/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $deliverables;

        $this->viewData['request'] = $request;

        $this->render('index', $this->viewData);
    }

    public function actionAdd($request_id = 0, $parent_id = 0)
    {
        $this->layout = 'modal';

        $RequestModel = new RequestModel();
        $requests = $RequestModel->gets(array(), 0);

        $MilestoneModel = new MilestoneModel();
        $milestones = $MilestoneModel->gets(array('request_id' => $request_id), 0);

        $data['requests'] = $requests;
        $data['request_id'] = $request_id;
        $data['parent_id'] = $parent_id;

        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['request_id'] = trim($_POST['request_id']);
        $args['parent_id'] = trim($_POST['parent_id']);
        $args['title'] = trim($_POST['title']);
        $args['description'] = trim($_POST['description']);
        $args['sort_order'] = trim($_POST['sort_order']);

        $errors = array();
        $messages = array();

        if (!$args['request_id'])
            $errors[] = "Please choose a <strong>Request</strong>";

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Deliverable Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DeliverableModel = new DeliverableModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $deliverable_id = $DeliverableModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Deliverable List',
                    'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $args['request_id'],
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $DeliverableModel = new DeliverableModel();
        $deliverable = $DeliverableModel->get($id);

        $data['item'] = $deliverable;
        $data['statuses'] = Yii::app()->params['app_data']['deliverable_statuses'];

        $results['item_view'] = $this->renderPartial('view', $data, true);

        $actions = [];

        $actions[] = array(
            'label' => 'Edit Deliverable',
            'url' => '#',
            'class' => 'btn btn-primary btn-object-modal',
            'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'deliverables/edit/id/' . $deliverable['id'] . '" modal-title="<i class=\'fa fa-edit\'></i> Edit Deliverable"',
            'id' => ''
        );
        $actions[] = array('type' => 'close-modal');

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $DeliverableModel = new DeliverableModel();
        $deliverable = $DeliverableModel->get($id);

        $RequestModel = new RequestModel();
        $requests = $RequestModel->gets(array(), 0);

        $MilestoneModel = new MilestoneModel();
        $data['item'] = $deliverable;
        $data['requests'] = $requests;

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $DeliverableModel = new DeliverableModel();
        $deliverable = $DeliverableModel->get($id);

        $args['title'] = trim($_POST['title']);
        $args['description'] = trim($_POST['description']);
        $args['sort_order'] = trim($_POST['sort_order']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Deliverable Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DeliverableModel = new DeliverableModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $DeliverableModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Deliverable List',
                    'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $deliverable['request_id'],
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $DeliverableModel = new DeliverableModel();
        $deliverable = $DeliverableModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $deliverable
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }


    public function actionDelete_handler($id)
    {
        $DeliverableModel = new DeliverableModel();
        $deliverable = $DeliverableModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$deliverable[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DeliverableModel = new DeliverableModel();

            $deliverable_id = $DeliverableModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $deliverable);
            $actions = array(
                array(
                    'label' => 'Deliverable List',
                    'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $deliverable['request_id'],
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionPrint($request_id = 0)
    {
        set_time_limit(0);
        define("DOMPDF_ENABLE_REMOTE", true);

        $UserModel = new UserModel();
        $user = $UserModel->get(Yii::app()->user->id);

        $DeliverableModel = new DeliverableModel();
        $RequestModel = new RequestModel();
        $ContactModel = new ContactModel();

        $request = $RequestModel->get($request_id);
        if (!$request)
            throw new CHttpException(404, 'Request is not found');

        $contact = $ContactModel->get($request['contact_id']);

        $args = array('request_id' => $request_id, 'parent_id' => 0);

        $deliverables = $DeliverableModel->gets($args, 0);

        foreach ($deliverables as $k => $v) {
            $args1 = array('parent_id' => $v['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
            $deliverables[$k]['subdeliverable-list'] = $DeliverableModel->gets($args1, 0);

            foreach ($deliverables[$k]['subdeliverable-list'] as $sk => $sv) {
                $args2 = array('parent_id' => $sv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] = $DeliverableModel->gets($args2, 0);
                foreach ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] as $ssk => $ssv) {
                    $args3 = array('parent_id' => $ssv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                    $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk]['subdeliverable-list'] = $DeliverableModel->gets($args3, 0);
                }
            }
        }

        $html = $this->renderPartial('print', array(
            'items' => $deliverables,
            'request' => $request,
            'contact' => $contact,
            'user' => $user
        ), true);


        $pdf = Yii::app()->dompdf;
        $pdf->dompdf->set_paper('a4'); //can set a5, a6…
        $pdf->generate($html, 'Deliverable_' . date('Ymd') . '.pdf', false);
        return false;
    }

    public function actionPrint_quotation($request_id = 0, $field = 'est_request')
    {
        set_time_limit(0);
        define("DOMPDF_ENABLE_REMOTE", true);

        $currency = Helper::request('currency', 'USD');
        $rate = Helper::request('rate', 0);

        $UserModel = new UserModel();
        $user = $UserModel->get(Yii::app()->user->id);

        $DeliverableModel = new DeliverableModel();
        $RequestModel = new RequestModel();
        $ContactModel = new ContactModel();

        $request = $RequestModel->get($request_id);
        if (!$request)
            throw new CHttpException(404, 'Request is not found');

        $contact = $ContactModel->get($request['contact_id']);

        $in_field = str_replace('est_', 'in_', $field);
        $args = array('request_id' => $request_id, 'parent_id' => 0, $in_field => 1);

        $deliverables = $DeliverableModel->gets($args, 0);
        $total = 0;

        foreach ($deliverables as $k => $v) {
            $args1 = array('parent_id' => $v['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC', $in_field => 1);
            $deliverables[$k]['subdeliverable-list'] = $DeliverableModel->gets($args1, 0);

            $tmp = $deliverables[$k];
            $total += $tmp[$in_field] * $tmp[$field];

            foreach ($deliverables[$k]['subdeliverable-list'] as $sk => $sv) {
                $args2 = array('parent_id' => $sv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC', $in_field => 1);
                $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] = $DeliverableModel->gets($args2, 0);

                $tmp = $deliverables[$k]['subdeliverable-list'][$sk];
                $total += $tmp[$in_field] * $tmp[$field];

                foreach ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] as $ssk => $ssv) {
                    $args3 = array('parent_id' => $ssv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC', $in_field => 1);
                    $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk]['subdeliverable-list'] = $DeliverableModel->gets($args3, 0);

                    $tmp = $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk];
                    $total += $tmp[$in_field] * $tmp[$field];
                }
            }
        }

        $html = $this->renderPartial('print_quotation', array(
            'items' => $deliverables,
            'request' => $request,
            'contact' => $contact,
            'user' => $user,
            'field' => $field,
            'currency' => $currency,
            'rate' => $rate,
            'total' => $total
        ), true);


        $pdf = Yii::app()->dompdf;
        $pdf->dompdf->set_paper('a4'); //can set a5, a6…
        $pdf->generate($html, 'Deliverable_' . date('Ymd') . '.pdf', false);
        return false;
    }


    public function actionIn_request($request_id = 0, $field = 'in_request')
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($request_id);

        $DeliverableModel = new DeliverableModel();

        $args = array();
        $args['s'] = Helper::request('s');

        $args['request_id'] = $request_id;
        $args['parent_id'] = 0;

        $deliverables = $DeliverableModel->gets($args, 0);

        foreach ($deliverables as $k => $v) {
            $args1 = array('parent_id' => $v['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
            $deliverables[$k]['subdeliverable-list'] = $DeliverableModel->gets($args1, 0);

            foreach ($deliverables[$k]['subdeliverable-list'] as $sk => $sv) {
                $args2 = array('parent_id' => $sv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] = $DeliverableModel->gets($args2, 0);
                foreach ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] as $ssk => $ssv) {
                    $args3 = array('parent_id' => $ssv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                    $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk]['subdeliverable-list'] = $DeliverableModel->gets($args3, 0);
                }
            }
        }

        $data['items'] = $deliverables;
        $data['request'] = $request;
        $data['field'] = $field;

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('in_request', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionIn_request_handler($id, $field = 'in_request')
    {
        $in_request = $_POST['in_request'];

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "Update Successfully!";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DeliverableModel = new DeliverableModel();

            // uncheck all
            $deliverables = $DeliverableModel->gets(array('parent_id' => 0, 'request_id' => $id), 0);
            foreach ($deliverables as $k => $v) {
                $DeliverableModel->update(array($field => 0, 'last_modified' => time(), 'id' => $v['id']));
                $subdeliverables = $DeliverableModel->gets(array('parent_id' => $v['id']), 0);
                foreach ($subdeliverables as $sk => $sv) {
                    $DeliverableModel->update(array($field => 0, 'last_modified' => time(), 'id' => $sv['id']));
                    $sub_subdeliverables = $DeliverableModel->gets(array('parent_id' => $sv['id']), 0);
                    foreach ($sub_subdeliverables as $ssk => $ssv) {
                        $DeliverableModel->update(array($field => 0, 'last_modified' => time(), 'id' => $ssv['id']));
                    }
                }
            }

            // check
            if ($in_request) {
                foreach ($in_request as $deliverable_id) {
                    $args[$field] = 1;
                    $args['last_modified'] = time();
                    $args['id'] = $deliverable_id;
                    $DeliverableModel->update($args);
                }
            }

            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $in_request);
            $actions = array(
                array(
                    'label' => 'Deliverable List',
                    'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $id,
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }


    public function actionEst_request($request_id = 0, $field = 'est_request', $copy_from = '')
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($request_id);

        $DeliverableModel = new DeliverableModel();

        $args = array();
        $args['s'] = Helper::request('s');

        $args['request_id'] = $request_id;
        $args['parent_id'] = 0;

        $deliverables = $DeliverableModel->gets($args, 0);

        foreach ($deliverables as $k => $v) {
            $args1 = array('parent_id' => $v['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
            $deliverables[$k]['subdeliverable-list'] = $DeliverableModel->gets($args1, 0);

            foreach ($deliverables[$k]['subdeliverable-list'] as $sk => $sv) {
                $args2 = array('parent_id' => $sv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] = $DeliverableModel->gets($args2, 0);
                foreach ($deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'] as $ssk => $ssv) {
                    $args3 = array('parent_id' => $ssv['id'], 'order_by' => 'sort_order', 'order_asc' => 'ASC');
                    $deliverables[$k]['subdeliverable-list'][$sk]['subdeliverable-list'][$ssk]['subdeliverable-list'] = $DeliverableModel->gets($args3, 0);
                }
            }
        }

        $data['items'] = $deliverables;
        $data['request'] = $request;
        $data['field'] = $field;
        $data['copy_from'] = $copy_from;

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('est_request', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEst_request_handler($id, $field = 'est_request')
    {
        $est_request = $_POST['est_request'];

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "Update Successfully!";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DeliverableModel = new DeliverableModel();

            // check
            if ($est_request) {
                foreach ($est_request as $k => $v) {
                    $args[$field] = $v;
                    $args['last_modified'] = time();
                    $args['id'] = $k;
                    $DeliverableModel->update($args);
                }
            }

            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $est_request);
            $actions = array(
                array(
                    'label' => 'Deliverable List',
                    'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $id,
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}