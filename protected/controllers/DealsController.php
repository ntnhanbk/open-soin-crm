<?php

class DealsController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "sales";
        Yii::app()->params['page'] = "deals";
    }

    public function actionIndex($order_by = 'date_added', $order_asc = 'desc', $p = 1, $status = 0)
    {
        $DealModel = new DealModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $deals = $DealModel->gets($args, $p, $ppp);
        $total = $DealModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "deals/index/order_by/$order_by/order_asc/$order_asc/status/$status/p/", $total, $p) : "";
        $this->viewData['items'] = $deals;

        $this->render('index', $this->viewData);
    }

    public function actionAdd($quote_id = 0)
    {
        $QuoteModel = new QuoteModel();
        $quotes = $QuoteModel->gets(array('status' => 0), 0);

        $quote = $QuoteModel->get($quote_id);
        if ($quote)
            $data['selected_quote'] = $quote;

        $data['quotes'] = $quotes;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['deal_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['quote_id'] = trim($_POST['quote_id']);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($args['quote_id'] == 0)
            $errors[] = "Please choose a <strong>Quote</strong>";

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Deal Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DealModel = new DealModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            // add deal
            $deal_id = $DealModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

            // set converted for quote
            $QuoteModel = new QuoteModel();
            $args1['id'] = $args['quote_id'];
            $args1['status'] = 1; // Converted
            $QuoteModel->update($args1);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args1);


            $actions = array(
                array(
                    'label' => 'Deal List',
                    'url' => HelperUrl::baseUrl() . 'deals',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $DealModel = new DealModel();
        $deal = $DealModel->get($id);

        $QuoteModel = new QuoteModel();
        $quotes = $QuoteModel->gets(array(), 0);

        $data['item'] = $deal;
        $data['quotes'] = $quotes;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['deal_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $DealModel = new DealModel();
        $deal = $DealModel->get($id);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['date_added'] = strtotime($_POST['date_added']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Deal Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DealModel = new DealModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $DealModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Deal List',
                    'url' => HelperUrl::baseUrl() . 'deals',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $DealModel = new DealModel();
        $deal = $DealModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $deal
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $DealModel = new DealModel();
        $deal = $DealModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$deal[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $DealModel = new DealModel();

            $deal_id = $DealModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $deal);
            $actions = array(
                array(
                    'label' => 'Deal List',
                    'url' => HelperUrl::baseUrl() . 'deals',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $DealModel = new DealModel();
        $deal = $DealModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $deal
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/deal/id/' . $deal['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($deal['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/deal/id/' . $deal['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($deal['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Edit Deal',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'deals/edit/id/' . $deal['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($deal['title']) . '"',
                'id' => ''
            ),
        );

        if ($deal['status'] == 0)
            $actions[] = array(
                'type' => 'btn',
                'url' => '#',
                'id' => 'btn-convert',
                'class' => 'btn btn-success btn-object-modal',
                'label' => 'Convert to Order',
                'attributes' => 'modal-size="" modal-url="' . HelperUrl::baseUrl() . 'orders/add/deal_id/' . $id . '" modal-title="<i class=\'fa fa-plus\'></i> Convert Deal to Order"',
            );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionView_by_month($month)
    {
        $start_date = strtotime('1 '.$month . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);

        $DealModel = new DealModel();
        $deals = $DealModel->gets(array('created_from' => $start_date, 'created_to' => $end_date), 0);

        $results['item_view'] = $this->renderPartial('view_by_month', array(
            'items' => $deals
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

}