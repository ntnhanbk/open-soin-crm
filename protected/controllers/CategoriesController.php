<?php

class CategoriesController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "data";
        Yii::app()->params['page'] = "categories";
    }

    public function actionIndex($order_by = 'title', $order_asc = 'desc', $p = 1)
    {
        $CategoryModel = new CategoryModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $categories = $CategoryModel->gets($args, $p, $ppp);
        $total = $CategoryModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "categories/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $categories;

        $this->render('index', $this->viewData);
    }

    public function actionAdd()
    {
        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', null, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['title'] = trim($_POST['title']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Category Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $CategoryModel = new CategoryModel();
            $args['img'] = '';
            $args['thumbnail'] = '';
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $category_id = $CategoryModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Category List',
                    'url' => HelperUrl::baseUrl() . 'categories',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $CategoryModel = new CategoryModel();
        $category = $CategoryModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $category
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/category/id/' . $category['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($category['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/category/id/' . $category['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($category['title']) . '"',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $CategoryModel = new CategoryModel();
        $category = $CategoryModel->get($id);

        $results['item_view'] = $this->renderPartial('edit', array(
            'item' => $category
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $CategoryModel = new CategoryModel();
        $category = $CategoryModel->get($id);
        $args['title'] = trim($_POST['title']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Category Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $CategoryModel = new CategoryModel();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;
            $args['id'] = $id;

            $CategoryModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Category List',
                    'url' => HelperUrl::baseUrl() . 'categories',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $CategoryModel = new CategoryModel();
        $category = $CategoryModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $category
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $CategoryModel = new CategoryModel();
        $category = $CategoryModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$category[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $CategoryModel = new CategoryModel();

            $category_id = $CategoryModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $category);
            $actions = array(
                array(
                    'label' => 'Category List',
                    'url' => HelperUrl::baseUrl() . 'categories',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}