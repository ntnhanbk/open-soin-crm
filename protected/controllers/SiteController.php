<?php

class SiteController extends CController
{
    private $viewData;

    public function actionIndex()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');
        //$this->redirect(HelperUrl::baseUrl() . 'home');

        $RequestModel = new RequestModel();
        $this->viewData['requests'] = $RequestModel->gets(array('status' => 0), 0);
        $this->viewData['request_count'] = $RequestModel->counts(array());

        $QuoteModel = new QuoteModel();
        $this->viewData['quotes'] = $QuoteModel->gets(array('status' => 0), 0);
        $this->viewData['quote_count'] = $QuoteModel->counts(array());
        $this->viewData['quote_sum'] = $QuoteModel->sum(array('status' => 0));

        $DealModel = new DealModel();
        $this->viewData['deals'] = $DealModel->gets(array('status' => 0), 0);
        $this->viewData['deal_count'] = $DealModel->counts(array());
        $this->viewData['deal_sum'] = $DealModel->sum(array('status' => 0));

        $OrderModel = new OrderModel();
        $this->viewData['orders'] = $OrderModel->gets(array('status' => 0), 0);
        $this->viewData['order_count'] = $OrderModel->counts(array());
        $this->viewData['order_sum'] = $OrderModel->sum(array('status' => 0));

        $MilestoneModel = new MilestoneModel();
        $this->viewData['upcoming_milestones'] = $MilestoneModel->gets(array('status' => 0), 1, 6);

        $InvoiceModel = new InvoiceModel();
        $this->viewData['invoices'] = $InvoiceModel->gets(array('status' => 1), 0);
        $this->viewData['invoice_count'] = $InvoiceModel->counts(array());
        $this->viewData['invoice_sum'] = $InvoiceModel->sum(array('status' => 1));

        $ExpenseModel = new ExpenseModel();
        $this->viewData['expenses'] = $ExpenseModel->gets(array('status' => 1), 0);
        $this->viewData['expense_count'] = $ExpenseModel->counts(array());
        $this->viewData['expense_sum'] = $ExpenseModel->sum(array('status' => 1));

        $this->viewData['all_expenses'] = $ExpenseModel->get_all_group_by_receiver();

        $start_date = strtotime(date('1 M Y') . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);

        $sum = $OrderModel->sum(array('created_from' => $start_date, 'created_to' => $end_date));
        $this->viewData['total_orders'] = ($sum) ? $sum : 0;

        $sum = $InvoiceModel->sum(array('created_from' => $start_date, 'created_to' => $end_date));
        $this->viewData['total_invoices'] = ($sum) ? $sum : 0;

        $sum = $ExpenseModel->sum(array('created_from' => $start_date, 'created_to' => $end_date));
        $this->viewData['total_expenses'] = ($sum) ? $sum : 0;

        $basefrom = strtotime(date('d M Y') . " 00:00:00");
        $baseto = strtotime(date('d M Y') . " 23:59:59");
        $chart_date = array();
        for ($i = 6; $i >= 0; $i--) {
            $from = strtotime('-' . $i . ' day', $basefrom);
            $to = strtotime('-' . $i . ' day', $baseto);

            $chart_date[$i] = "'" . date('M d', $from) . "'";

            $sum = $QuoteModel->sum(array('created_from' => $from, 'created_to' => $to));
            $chart_quotes[$i] = ($sum) ? round($sum, 2) : 0;

            $sum = $DealModel->sum(array('created_from' => $from, 'created_to' => $to));
            $chart_deals[$i] = ($sum) ? round($sum, 2) : 0;

            $sum = $OrderModel->sum(array('created_from' => $from, 'created_to' => $to));
            $chart_orders[$i] = ($sum) ? round($sum, 2) : 0;

            $sum = $InvoiceModel->sum(array('created_from' => $from, 'created_to' => $to));
            $chart_invoices[$i] = ($sum) ? round($sum, 2) : 0;

            $sum = $ExpenseModel->sum(array('created_from' => $from, 'created_to' => $to));
            $chart_expenses[$i] = ($sum) ? round($sum, 2) : 0;
        }
        $this->viewData['chart_date'] = implode(',', $chart_date);
        $this->viewData['chart_quotes'] = implode(',', $chart_quotes);
        $this->viewData['chart_deals'] = implode(',', $chart_deals);
        $this->viewData['chart_orders'] = implode(',', $chart_orders);
        $this->viewData['chart_invoices'] = implode(',', $chart_invoices);
        $this->viewData['chart_expenses'] = implode(',', $chart_expenses);

        $base_start_date = strtotime(date('1 M Y') . " 00:00:00");
        $monthly_reports = array();
        for ($i = 0; $i <= 5; $i++) {
            $from = strtotime('-' . $i . ' month', $base_start_date);
            $to = Helper::get_end_date_of_month($from);

            $sum = $QuoteModel->sum(array('created_from' => $from, 'created_to' => $to));
            $monthly_reports[date('M Y', $from)]['quotes'] = ($sum) ? round($sum, 2) : 0;

            $sum = $DealModel->sum(array('created_from' => $from, 'created_to' => $to));
            $monthly_reports[date('M Y', $from)]['deals'] = ($sum) ? round($sum, 2) : 0;

            $sum = $OrderModel->sum(array('created_from' => $from, 'created_to' => $to));
            $monthly_reports[date('M Y', $from)]['orders'] = ($sum) ? round($sum, 2) : 0;

            $sum = $InvoiceModel->sum(array('created_from' => $from, 'created_to' => $to));
            $monthly_reports[date('M Y', $from)]['invoices'] = ($sum) ? round($sum, 2) : 0;

            $sum = $ExpenseModel->sum(array('created_from' => $from, 'created_to' => $to));
            $monthly_reports[date('M Y', $from)]['expenses'] = ($sum) ? round($sum, 2) : 0;
        }

        $this->viewData['monthly_reports'] = $monthly_reports;

        $this->render('index', $this->viewData);
    }

    public function actionCost()
    {
        Yii::app()->params['page_group'] = 'cost-structures';

        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $ExpenseModel = new ExpenseModel();
        $this->viewData['all_expenses'] = $ExpenseModel->get_all_group_by_receiver();


        $OrderModel = new OrderModel();
        $this->viewData['orders'] = $OrderModel->gets(array('status' => 0), 0);

        $this->render('cost', $this->viewData);
    }

    public function actionError()
    {
        $this->render('error');
    }

    public function actionSign_in()
    {
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            $error = CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login())
                $this->redirect(HelperUrl::baseUrl());
        }

        $this->layout = 'signin';
        $this->render('signin', array('model' => $model));
    }

    public function actionSign_out()
    {
        Yii::app()->user->logout();

        return $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');
    }


    public function actionForgot()
    {
        $form_result = array(
            'status' => 'pending'
        );

        if ($_POST)
            $form_result = $this->do_forgot();

        $this->layout = 'signin';
        $this->viewData['form_result'] = $form_result;
        $this->render('forgot', $this->viewData);
    }

    private function do_forgot()
    {
        $Validator = new FormValidator();
        $UserModel = new UserModel();
        $TokenModel = new TokenModel();

        $errors = array();

        $email = Helper::request('email');

        if (!$email || !$Validator->is_email($email)) {
            $errors[] = "<strong>Email address</strong> is invalid";
        } else {
            $user = $UserModel->get_by_email($email);
            if (!$user) {
                $errors[] = "<strong>Email address</strong> isn't exists";
            }
        }

        if (count($errors)) {
            $result['status'] = 'error';
            $result['message'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
            return $result;
        }

        /** Set token key */
        $token_key = md5($email . time() . $user['secret_key']);
        $TokenModel->delete_all_tokens($user['id'], 'forgot-password');

        $TokenModel->add(array(
            'token' => $token_key,
            'type' => 'forgot-password',
            'user_id' => $user['id'],
            'date_added' => time(),
            'expired_date' => strtotime('+1 day'),
            'disabled' => 0,
        ));

        /** Send email */
        $email_content = $this->renderPartial('email/forgot', array(
            'user' => $user,
            'token' => $token_key
        ), true);
        $subject = "How to reset your password";
        Helper::email($user['email'], $user['display_name'], $subject, $email_content);

        return $this->redirect(HelperUrl::baseUrl() . 'site/forgot?sent=true&email=' . urlencode($email));
    }

    public function actionReset_password($token = '')
    {
        /** Validate token */
        $TokenModel = new TokenModel();

        $token_info = $TokenModel->get_by_token($token);
        if (!$token_info || $token_info['type'] != 'forgot-password')
            return $this->redirect(HelperUrl::baseUrl() . 'site/forgot?token=invalid');
        if ($token_info['expired_date'] < time() || $token_info['disabled'])
            return $this->redirect(HelperUrl::baseUrl() . 'site/forgot?token=expired');

        /** Reset password */
        $UserModel = new UserModel();
        $user = $UserModel->get($token_info['user_id']);
        if (!$user)
            return $this->redirect(HelperUrl::baseUrl() . 'site/forgot?token=invalid');

        $new_raw_password = Helper::generate_random(6);
        $new_password = crypt($new_raw_password, $user['secret_key']);

        $UserModel->update(array(
            'password' => $new_password,
            'id' => $user['id']
        ));

        $TokenModel->update(array(
            'id' => $token_info['id'],
            'disabled' => 1
        ));

        /** Send email */
        $email_content = $this->renderPartial('email/reset', array(
            'user' => $user,
            'new_password' => $new_raw_password
        ), true);
        $subject = "Your new password";
        Helper::email($user['email'], $user['display_name'], $subject, $email_content);

        $this->layout = 'signin';
        return $this->render('reset', array(
            'user' => $user
        ));

    }

    public function actionSign_up()
    {
        $form_result = null;
        if ($_POST)
            $form_result = $this->do_sign_up();

        $this->layout = 'signin';
        $this->viewData['form_result'] = $form_result;

        $this->render('signup', $this->viewData);
    }

    private function do_sign_up()
    {
        $Validator = new FormValidator();
        $UserModel = new UserModel();
        $TokenModel = new TokenModel();
        $errors = array();

        /** Get input data */
        $first_name = trim(Helper::request('first_name'));
        $last_name = trim(Helper::request('last_name'));
        $email = trim(Helper::request('email'));
        $username = trim(Helper::request('username'));
        $password = trim(Helper::request('password'));
        $confirm_password = trim(Helper::request('confirm_password'));

        /** Validate input data */
        if (!$first_name || $first_name == '')
            $errors[] = "<strong>First name </strong> is empty";

        if (!$last_name || $last_name == '')
            $errors[] = "<strong>Last name </strong> is empty";

        if (!$Validator->is_email($email))
            $errors[] = "<strong>Email address </strong> is invalid";
        else if ($UserModel->get_by_email($email))
            $errors[] = "<strong>" . $email . "</strong> is exists";

        if (!$username || $username == '')
            $errors[] = "<strong>Username </strong> is empty";
        else if ($UserModel->get_by_username($username))
            $errors[] = "<strong>Username </strong> is exsits";

        if (!$password || $password == '')
            $errors[] = "<strong>Password</strong> is empty";

        if (!$confirm_password || $confirm_password == '')
            $errors[] = "<strong>Confirm password</strong> is empty";

        if ($confirm_password && $confirm_password != ''
            && $password && $password != ''
            && $password != $confirm_password
        ) {
            $errors[] = "<strong>Confirm password </strong> is not match";
        }

        if (count($errors)) {
            $result['status'] = 'error';
            $result['message'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
            return $result;
        }

        /** Add new user (role: sub-admin) */
        $secret_key = Helper::generate_secret_key($username, $email);
        $encoded_password = crypt($password, $secret_key);

        $args = array(
            'username' => $username,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'display_name' => $first_name . ' ' . $last_name,
            'password' => $encoded_password,
            'role' => 'sub-admin',
            'secret_key' => $secret_key,
            'email' => $email,
            'img' => '',
            'thumbnail' => '',
            'date_added' => time(),
            'last_modified' => 0,
            'author_id' => 0,
            'disabled' => 1,
        );

        $user_id = $UserModel->add($args);
        Helper::add_log($user_id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        /** Set token key */
        $token_key = md5($email . time() . $secret_key);

        $TokenModel->add(array(
            'token' => $token_key,
            'type' => 'new-account',
            'user_id' => $user_id,
            'date_added' => time(),
            'expired_date' => strtotime('+30 days'),
            'disabled' => 0,
        ));

        /** Send email */
        $email_content = $this->renderPartial('email/signup', array(
            'user' => $args,
            'token' => $token_key,
            'raw_password' => $password
        ), true);
        $subject = "Verify information";
        Helper::email($args['email'], $args['display_name'], $subject, $email_content);

        return $this->redirect(HelperUrl::baseUrl() . 'site/sign_up?sent=true');
    }


    public function actionVerify_account($token = '')
    {
        /** Validate token */
        $TokenModel = new TokenModel();

        $token_info = $TokenModel->get_by_token($token);
        if (!$token_info || $token_info['type'] != 'new-account')
            return $this->redirect(HelperUrl::baseUrl() . 'site/verify_account_message?token=invalid');

        if ($token_info['expired_date'] < time() || $token_info['disabled'])
            return $this->redirect(HelperUrl::baseUrl() . 'site/verify_account_message?token=expired');

        /** Active account */
        $UserModel = new UserModel();
        $user = $UserModel->get($token_info['user_id']);
        if (!$user)
            return $this->redirect(HelperUrl::baseUrl() . 'site/verify_account_message?token=invalid');

        $UserModel->update(array(
            'disabled' => 0,
            'id' => $user['id']
        ));

        $TokenModel->update(array(
            'id' => $token_info['id'],
            'disabled' => 1
        ));

        /** Send email */
        $email_content = $this->renderPartial('email/verify-account', array(
            'user' => $user
        ), true);
        $subject = "Welcome to SoinCRM system";
        Helper::email($user['email'], $user['display_name'], $subject, $email_content);

        $this->layout = 'signin';
        return $this->render('reset', array(
            'user' => $user
        ));

        return $this->redirect(HelperUrl::baseUrl() . 'site/verify_account_message?token=success&email=' . urlencode($user['email']));
    }

    public function actionVerify_account_message()
    {
        $this->layout = 'signin';
        return $this->render('verify-account-message');
    }


    public function actionProfile_handler()
    {
        $UserModel = new UserModel();
        $Validator = new FormValidator();
        $args = array();
        $errors = array();
        $messages = array();

        $username = Yii::app()->user->username;
        $user = $UserModel->get_by_username($username);

        $first_name = trim(Helper::request('first_name'));
        $last_name = trim(Helper::request('last_name'));
        $email = trim(Helper::request('email'));

        if (!$Validator->is_email($email)) {
            $errors[] = "<strong>Email address</strong> Invalid";
        } else if ($email != $user['email'] && !$UserModel->get_by_email($email)) {
            $errors[] = "<strong>Email</strong> Exists";
        }

        if ($Validator->is_empty_string($first_name))
            $errors[] = "Please enter <strong>First Name</strong>";

        if ($Validator->is_empty_string($last_name))
            $errors[] = "Please enter <strong>Last Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "Update Information Successfully!";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $args['first_name'] = $first_name;
            $args['last_name'] = $last_name;
            $args['display_name'] = $first_name . ' ' . $last_name;
            $args['email'] = $email;
            $args['company_name'] = trim(Helper::request('company_name'));
            $args['last_modified'] = time();
            $args['id'] = $user['id'];

            if (isset($_FILES['company_logo']) && $_FILES['company_logo']['tmp_name'] != '') {
                $file = $_FILES['company_logo'];
                $photo_info = Helper::resize_images($file);
                if ($photo_info) {
                    $args['company_logo'] = $photo_info['thumbnail'];
                }
            }

            if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['tmp_name'] != '') {
                $file = $_FILES['thumbnail'];
                $photo_info = Helper::resize_images($file);
                if ($photo_info) {
                    $args['thumbnail'] = $photo_info['thumbnail'];
                }
            }

            $UserModel->update($args);

            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'type' => 'close-modal'
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }


    public function actionProfile()
    {
        $UserModel = new UserModel();
        $username = Yii::app()->user->username;
        $user = $UserModel->get_by_username($username);

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('profile',
            array(
                'item' => $user
            ),
            true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-primary ajax-form btn-save'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionPublic($code = '')
    {
        $receiver_codes = Yii::app()->params['app_data']['receiver_codes'];

        $receiver = isset($receiver_codes[$code]) ? $receiver_codes[$code] : '';
        if ($receiver == '')
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->layout = 'public';

        $ExpenseModel = new ExpenseModel();
        $this->viewData['expenses'] = $ExpenseModel->gets(array('receiver' => $receiver, 'author_id' => 1), 0, 0);

        $sum_results = array();
        $k = -1;
        $sums = $ExpenseModel->sum_expenses(1, $receiver);
        foreach ($sums as $s) {
            $k++;
            $sum_results[$s['receiver']][$k]['status'] = $s['status'];
            $sum_results[$s['receiver']][$k]['currency'] = $s['currency'];
            $sum_results[$s['receiver']][$k]['total'] = $s['total'];
        }
        $this->viewData['sum_results'] = $sum_results;

        $this->render('public', $this->viewData);
    }


    public function actionChange()
    {
        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('change', null, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionChange_handler()
    {
        $UserModel = new UserModel();
        $args = array();
        $errors = array();
        $messages = array();

        $username = Yii::app()->user->username;
        $user = $UserModel->get_by_username($username);
        $password = $user['password'];

        $old_password = trim(Helper::request('old_password'));
        $new_password = trim(Helper::request('new_password'));
        $confirm_new_password = trim(Helper::request('confirm_new_password'));


        if (!$old_password || empty($old_password) || $password != crypt($old_password, $user['secret_key'])) {
            $errors[] = "Old password is invalid. Please enter old password";
        } else if (!$new_password || empty($new_password)) {
            $errors[] = "Please enter new password";
        } else if (!$confirm_new_password || empty($confirm_new_password) || $new_password != $confirm_new_password) {
            $errors[] = "Confirm new password is invalid. Please enter confirm new password";
        }

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "Change Password successfully!";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {

            $args['password'] = crypt($new_password, $user['secret_key']);
            $args['id'] = $user['id'];

            $UserModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'type' => 'close-modal'
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionLanding()
    {
        $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');
        
        $this->layout = '//layouts/landing';

        $this->viewData['pricings'] = Helper::get_pricing_list();
        return $this->render('landing', $this->viewData);
    }

    public function actionPayment($package = 'free')
    {
        $package_id = $package;
        $package = Helper::get_pricing_item($package);
        if (!$package)
            throw new CHttpException(404, 'Page not found');

        $UserModel = new UserModel();
        $user = $UserModel->get(Yii::app()->user->id);

        $PaymentModel = new PaymentModel();

        $item_name = "[Soin CRM] " . $package['title'];
        $business = Yii::app()->params['paypal_address'];
        $amount = $package['amt'];
        $currency_code = $package['currency'];

        $token_key = Helper::generate_secret_key($user['username'], $user['email']);
        $args = array(
            'package_id' => $package_id,
            'title' => $user['display_name'],
            'package_title' => $package['title'],
            'package_time' => $package['time'],
            'package_time_type' => $package['time_type'],
            'payment_gateway' => 'paypal',
            'token_key' => $token_key,
            'admin_email' => Yii::app()->params['paypal_address'],
            'amt' => $amount,
            'currency' => $currency_code,
            'author_id' => Yii::app()->user->id,
            'expired_date' => strtotime('+1 month'),
            'date_added' => time(),
            'confirm_date' => 0,
            'last_modified' => time(),
            'txn_id' => '',
            'status' => 0,
            'disabled' => 0,
            'deleted' => 0
        );

        $payment_id = $PaymentModel->add($args);

        $Paypal = new PaypalHelper();
        $Paypal->sandbox = Yii::app()->params['paypay_sandbox_pointer'];
        $Paypal->business = $business;
        $Paypal->item_name = $item_name;
        $Paypal->amount = $amount;
        $Paypal->currency_code = $currency_code;
        $Paypal->custom = $payment_id;
        $Paypal->return = HelperUrl::baseUrl(true) . 'site/invoice/token/' . $token_key;
        $Paypal->notify_url = HelperUrl::baseUrl(true) . 'paypal/ipn/type/pricing';

        $paypal_url = $Paypal->get_payment_url();

        return $this->redirect($paypal_url);
    }

    public function actionInvoice($token)
    {
        $PaymentModel = new PaymentModel();
        $payment = $PaymentModel->get_by_token($token);

        if (!$payment)
            return $this->redirect(HelperUrl::baseUrl());

        $this->viewData['invoice'] = $payment;
        return $this->render('invoice', $this->viewData);
    }

}