<?php

class LogsController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        if (Yii::app()->user->id != 1)
            $this->redirect(HelperUrl::baseUrl());

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "system";
        Yii::app()->params['page'] = "logs";
    }

    public function actionIndex($order_by = 'id', $order_asc = 'desc', $p = 1)
    {
        $LogModel = new LogModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $logs = $LogModel->gets($args, $p, $ppp);
        $total = $LogModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "logs/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $logs;

        $this->render('index', $this->viewData);
    }

    public function actionView($id)
    {
        $LogModel = new LogModel();
        $log = $LogModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $log
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

}