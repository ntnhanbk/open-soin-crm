<?php

class OrdersController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "post_sales";
        Yii::app()->params['page'] = "orders";
    }

    public function actionIndex($order_by = 'date_added', $order_asc = 'desc', $p = 1, $status = 0)
    {
        $OrderModel = new OrderModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $orders = $OrderModel->gets($args, $p, $ppp);
        $total = $OrderModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "orders/index/order_by/$order_by/order_asc/$order_asc/status/$status/p/", $total, $p) : "";
        $this->viewData['items'] = $orders;

        $this->render('index', $this->viewData);
    }

    public function actionAdd($deal_id = 0)
    {
        $DealModel = new DealModel();
        $deals = $DealModel->gets(array('status' => 0), 0);

        $deal = $DealModel->get($deal_id);
        if ($deal)
            $data['selected_deal'] = $deal;

        $data['deals'] = $deals;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['order_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['deal_id'] = trim($_POST['deal_id']);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($args['deal_id'] == 0)
            $errors[] = "Please choose a <strong>Deal</strong>";

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Order Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $OrderModel = new OrderModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            //  add order
            $order_id = $OrderModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

            // set converted for deal
            $DealModel = new DealModel();
            $args1['id'] = $args['deal_id'];
            $args1['status'] = 1; // Converted
            $DealModel->update($args1);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args1);


            $actions = array(
                array(
                    'label' => 'Order List',
                    'url' => HelperUrl::baseUrl() . 'orders',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $DealModel = new DealModel();
        $deals = $DealModel->gets(array(), 0);

        $data['item'] = $order;
        $data['deals'] = $deals;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['order_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['date_added'] = strtotime($_POST['date_added']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Order Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $OrderModel = new OrderModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $OrderModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Order List',
                    'url' => HelperUrl::baseUrl() . 'orders',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $order
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$order[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $OrderModel = new OrderModel();

            $order_id = $OrderModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $order);
            $actions = array(
                array(
                    'label' => 'Order List',
                    'url' => HelperUrl::baseUrl() . 'orders',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $MilestoneModel = new MilestoneModel();
        $milestones = $MilestoneModel->gets(array('order_id' => $order['id']), 0);

        $DealModel = new DealModel();
        $deal = $DealModel->get($order['deal_id']);

        $QuoteModel = new QuoteModel();
        $quote = $QuoteModel->get($deal['quote_id']);

        $RequestModel = new RequestModel();
        $request = $RequestModel->get($quote['request_id']);

        $CommentModel = new CommentModel();
        $AttachmentModel = new AttachmentModel();

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $order,
            'milestones' => $milestones,
            'deal' => $deal,
            'quote' => $quote,
            'request' => $request,
            'order_comments' => $CommentModel->gets(array('object_type' => 'order', 'object_id' => $order['id']), 0),
            'deal_comments' => $CommentModel->gets(array('object_type' => 'deal', 'object_id' => $deal['id']), 0),
            'quote_comments' => $CommentModel->gets(array('object_type' => 'quote', 'object_id' => $quote['id']), 0),
            'request_comments' => $CommentModel->gets(array('object_type' => 'request', 'object_id' => $request['id']), 0),

            'order_attachments' => $AttachmentModel->gets(array('object_type' => 'order', 'object_id' => $order['id']), 0),
            'deal_attachments' => $AttachmentModel->gets(array('object_type' => 'deal', 'object_id' => $deal['id']), 0),
            'quote_attachments' => $AttachmentModel->gets(array('object_type' => 'quote', 'object_id' => $quote['id']), 0),
            'request_attachments' => $AttachmentModel->gets(array('object_type' => 'request', 'object_id' => $request['id']), 0),
        ), true);

        $actions = array(
            array(
                'label' => '<i class="fa fa-refresh"></i>',
                'url' => '#',
                'class' => 'btn btn-default btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'orders/view/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
            array(
                'type' => 'close-modal'
            ),

            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/order/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/order/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),

            array(
                'label' => 'Edit Order',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'orders/edit/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Financial Report',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'orders/financial_report/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionView_by_month($month)
    {
        $start_date = strtotime('1 ' . $month . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);

        $OrderModel = new OrderModel();
        $orders = $OrderModel->gets(array('created_from' => $start_date, 'created_to' => $end_date), 0);

        $results['item_view'] = $this->renderPartial('view_by_month', array(
            'items' => $orders
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionFinancial_report($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $InvoiceModel = new InvoiceModel();
        $invoices = $InvoiceModel->gets(array('order_id' => $order['id']), 0);

        $ExpenseModel = new ExpenseModel();
        $expenses = $ExpenseModel->gets(array('order_id' => $order['id']), 0);
        $chart_expenses = $ExpenseModel->get_all_group_by_receiver($order['id']);

        $results['item_view'] = $this->renderPartial('financial_report', array(
            'item' => $order,
            'invoices' => $invoices,
            'expenses' => $expenses,
            'chart_expenses' => $chart_expenses,
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

            array(
                'type' => 'btn',
                'url' => '#',
                'id' => 'btn-convert',
                'class' => 'btn btn-success btn-object-modal',
                'label' => 'Add New Invoice',
                'attributes' => 'modal-size="" modal-url="' . HelperUrl::baseUrl() . 'invoices/add/order_id/' . $id . '" modal-title="<i class=\'fa fa-plus\'></i> Add New Invoice"',
            ),

            array(
                'type' => 'btn',
                'url' => '#',
                'id' => 'btn-convert',
                'class' => 'btn btn-danger btn-object-modal',
                'label' => 'Add New Expense',
                'attributes' => 'modal-size="" modal-url="' . HelperUrl::baseUrl() . 'expenses/add/order_id/' . $id . '" modal-title="<i class=\'fa fa-plus\'></i> Add New Expense"',
            )
        );


        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }


    public function actionMilestones($id)
    {
        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $MilestoneModel = new MilestoneModel();
        $milestones = $MilestoneModel->gets(array('order_id' => $order['id']), 0);

        $results['item_view'] = $this->renderPartial('milestones', array(
            'item' => $order,
            'milestones' => $milestones,
        ), true);

        $actions = array(
            array(
                'label' => '<i class="fa fa-refresh"></i>',
                'url' => '#',
                'class' => 'btn btn-default btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'orders/milestones/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Edit Order',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'orders/edit/id/' . $order['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($order['title']) . '"',
                'id' => ''
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

}