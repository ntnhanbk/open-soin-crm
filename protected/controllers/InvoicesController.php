<?php

class InvoicesController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "post_sales";
        Yii::app()->params['page'] = "invoices";
    }

    public function actionIndex($order_by = '', $order_asc = '', $p = 1, $status = 0)
    {
        $InvoiceModel = new InvoiceModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $invoices = $InvoiceModel->gets($args, $p, $ppp);
        $total = $InvoiceModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "invoices/index/order_by/$order_by/order_asc/$order_asc/status/$status/p/", $total, $p) : "";
        $this->viewData['items'] = $invoices;

        $this->render('index', $this->viewData);
    }

    public function actionAdd($order_id = 0)
    {
        $OrderModel = new OrderModel();
        $orders = $OrderModel->gets(array(), 0);
        $data['orders'] = $orders;

        $order = $OrderModel->get($order_id);
        if ($order) {
            $data['selected_order'] = $order;

            $InvoiceModel = new InvoiceModel();
            $data['remain_amt'] = round($order['amt'] - $InvoiceModel->sum(array('order_id' => $order['id'])),2);
        }

        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['invoice_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['order_id'] = trim($_POST['order_id']);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Invoice Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $InvoiceModel = new InvoiceModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $invoice_id = $InvoiceModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Invoice List',
                    'url' => HelperUrl::baseUrl() . 'invoices',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);

        $OrderModel = new OrderModel();
        $orders = $OrderModel->gets(array(), 0);

        $data['item'] = $invoice;
        $data['orders'] = $orders;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['invoice_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);
        $args['order_id'] = trim($_POST['order_id']);
        $args['title'] = trim($_POST['title']);
        $args['currency'] = trim($_POST['currency']);
        $args['amt'] = trim($_POST['amt']);
        $args['date_added'] = strtotime($_POST['date_added']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Invoice Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $InvoiceModel = new InvoiceModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $InvoiceModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Invoice List',
                    'url' => HelperUrl::baseUrl() . 'invoices',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $invoice
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$invoice[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $InvoiceModel = new InvoiceModel();

            $invoice_id = $InvoiceModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $invoice);
            $actions = array(
                array(
                    'label' => 'Invoice List',
                    'url' => HelperUrl::baseUrl() . 'invoices',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $invoice
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/invoice/id/' . $invoice['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($invoice['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/invoice/id/' . $invoice['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($invoice['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Edit Invoice',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'invoices/edit/id/' . $invoice['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($invoice['title']) . '"',
                'id' => ''
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionView_by_month($month)
    {
        $start_date = strtotime('1 '.$month . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);

        $InvoiceModel = new InvoiceModel();
        $invoices = $InvoiceModel->gets(array('created_from' => $start_date, 'created_to' => $end_date), 0);

        $results['item_view'] = $this->renderPartial('view_by_month', array(
            'items' => $invoices
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

}