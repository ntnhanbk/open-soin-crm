<?php

class ContactsController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "pre_sales";
        Yii::app()->params['page'] = "contacts";
    }

    public function actionIndex($order_by = 'title', $order_asc = 'desc', $p = 1)
    {
        $ContactModel = new ContactModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $contacts = $ContactModel->gets($args, $p, $ppp);
        $total = $ContactModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "contacts/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $contacts;

        $this->render('index', $this->viewData);
    }

    public function actionAdd($lead_id = 0)
    {
        $LeadModel = new LeadModel();
        $leads = $LeadModel->gets(array('status' => 0), 0);
        $lead = $LeadModel->get($lead_id);

        $data = array();
        if ($lead)
            $data['selected_lead'] = $lead;

        $data['leads'] = $leads;

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['lead_id'] = trim($_POST['lead_id']);
        $args['title'] = trim($_POST['title']);
        $args['phone'] = trim($_POST['phone']);
        $args['email'] = trim($_POST['email']);
        $args['skype'] = trim($_POST['skype']);
        $args['viber'] = trim($_POST['viber']);
        $args['whatsapp'] = trim($_POST['whatsapp']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Contact Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ContactModel = new ContactModel();
            $args['img'] = '';
            $args['thumbnail'] = '';
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            // add contact
            $contact_id = $ContactModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

            // set converted for lead
            $LeadModel = new LeadModel();
            $args1['id'] = $args['lead_id'];
            $args1['status'] = 1; // Converted
            $LeadModel->update($args1);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args1);

            $actions = array(
                array(
                    'label' => 'Contact List',
                    'url' => HelperUrl::baseUrl() . 'contacts',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $ContactModel = new ContactModel();
        $contact = $ContactModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $contact
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/contact/id/' . $contact['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($contact['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/contact/id/' . $contact['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($contact['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Edit',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'contacts/edit/id/' . $contact['id'] . '" modal-title="<i class=\'fa fa-edit\'></i> ' . CHtml::encode($contact['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Delete',
                'url' => '#',
                'class' => 'btn btn-danger btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'contacts/delete/id/' . $contact['id'] . '" modal-title="<i class=\'fa fa-trash\'></i> ' . CHtml::encode($contact['title']) . '"',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $ContactModel = new ContactModel();
        $contact = $ContactModel->get($id);

        $results['item_view'] = $this->renderPartial('edit', array(
            'item' => $contact
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $ContactModel = new ContactModel();
        $contact = $ContactModel->get($id);
        $args['title'] = trim($_POST['title']);
        $args['phone'] = trim($_POST['phone']);
        $args['email'] = trim($_POST['email']);
        $args['skype'] = trim($_POST['skype']);
        $args['viber'] = trim($_POST['viber']);
        $args['whatsapp'] = trim($_POST['whatsapp']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Contact Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ContactModel = new ContactModel();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;
            $args['id'] = $id;

            $ContactModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Contact List',
                    'url' => HelperUrl::baseUrl() . 'contacts',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $ContactModel = new ContactModel();
        $contact = $ContactModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $contact
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $ContactModel = new ContactModel();
        $contact = $ContactModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$contact[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ContactModel = new ContactModel();

            $contact_id = $ContactModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $contact);
            $actions = array(
                array(
                    'label' => 'Contact List',
                    'url' => HelperUrl::baseUrl() . 'contacts',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}