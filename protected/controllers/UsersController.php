<?php

class UsersController extends CController
{
    private $validator;
    private $viewData = array();
    public $UserModel;

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        if (Yii::app()->user->id != 1)
            $this->redirect(HelperUrl::baseUrl());

        $this->validator = new FormValidator();
        $this->UserModel = new UserModel();
        Yii::app()->params['page_group'] = "system";
        Yii::app()->params['page'] = "users";
    }

    public function actionIndex($order_by = 'display_name', $order_asc = 'desc', $p = 1)
    {
        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $user = $this->UserModel->gets($args, $p, $ppp);
        $total = $this->UserModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "users/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $user;
        $this->viewData['user_roles'] = Helper::get_app_data('user_roles');
        $this->render('index', $this->viewData);
    }

    public function actionAdd()
    {
        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', null, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args = array();
        $errors = array();
        $messages = array();

        $first_name = trim(Helper::request('first_name'));
        $last_name = trim(Helper::request('last_name'));
        $email = trim(Helper::request('email'));
        $password = trim(Helper::request('password'));
        $confirm_password = trim(Helper::request('confirm_password'));
        $username = trim(Helper::request('username'));
        $role = trim(Helper::request('role'));


        if ($this->validator->is_empty_string($username))
            $errors[] = "Please enter <strong>Username</strong>";

        if ($this->validator->is_empty_string($password))
            $errors[] = "Please enter <strong>PassWord</strong>";
        elseif ($password != $confirm_password)
            $errors[] = "<strong>Confirm password</strong> is invalid. Please enter confirm password";

        if ($this->validator->is_empty_string($email)) {
            $errors[] = "Please enter <strong>Email</strong>";
        } else if (!$this->validator->is_email($email)) {
            $errors[] = "<strong>Email</strong> Invalid";
        }

        if ($this->validator->is_empty_string($first_name))
            $errors[] = "Please enter <strong>First Name</strong>";

        if ($this->validator->is_empty_string($last_name))
            $errors[] = "Please enter <strong>Last Name</strong>";


        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>User $first_name  $last_name</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {

            $secret_key = md5(Helper::generate_random(20));

            $args['username'] = $username;
            $args['password'] = crypt($password, $secret_key);
            $args['first_name'] = $first_name;
            $args['last_name'] = $last_name;
            $args['display_name'] = $first_name . ' ' . $last_name;
            $args['role'] = $role;
            $args['secret_key'] = $secret_key;
            $args['email'] = $email;
            $args['date_added'] = time();

            $user_id = $this->UserModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'User List',
                    'url' => HelperUrl::baseUrl() . 'users',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $user = $this->UserModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $user
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $user = $this->UserModel->get($id);

        $results['item_view'] = $this->renderPartial('edit', array(
            'item' => $user
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );
        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $args = array();
        $errors = array();
        $messages = array();

        $change_password = false;
        $first_name = trim(Helper::request('first_name'));
        $last_name = trim(Helper::request('last_name'));
        $email = trim(Helper::request('email'));
        $password = trim(Helper::request('password'));
        $confirm_password = trim(Helper::request('confirm_password'));
        $username = trim(Helper::request('username'));
        $role = trim(Helper::request('role'));

        if ($this->validator->is_empty_string($username))
            $errors[] = "Please enter <strong>Username</strong>";

        if ($password && $password != '' && $password != $confirm_password)
            $errors[] = "<strong>Confirm password</strong> is invalid. Please enter confirm password";
        if ($password && $password != '' && $password == $confirm_password)
            $change_password = true;

        if ($this->validator->is_empty_string($email)) {
            $errors[] = "Please enter <strong>Email</strong>";
        } else if (!$this->validator->is_email($email)) {
            $errors[] = "<strong>Email</strong> Invalid";
        }

        if ($this->validator->is_empty_string($first_name))
            $errors[] = "Please enter <strong>First Name</strong>";

        if ($this->validator->is_empty_string($last_name))
            $errors[] = "Please enter <strong>Last Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>User $first_name $last_name</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {

            $args['username'] = $username;
            if ($change_password)
                $args['password'] = crypt($password, $password);
            $args['first_name'] = $first_name;
            $args['last_name'] = $last_name;
            $args['display_name'] = $first_name . ' ' . $last_name;
            $args['role'] = $role;
            $args['secret_key'] = $username;
            $args['email'] = $email;
            $args['last_modified'] = time();
            $args['id'] = $id;


            $this->UserModel->update($args);

            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Campaign List',
                    'url' => HelperUrl::baseUrl() . 'users',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {

        $user = $this->UserModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $user
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {

        $user = $this->UserModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$user[display_name]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {

            $this->UserModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $user);
            $actions = array(
                array(
                    'label' => 'User List',
                    'url' => HelperUrl::baseUrl() . 'users',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionSign_in_as($id)
    {
        $UserModel = new UserModel();
        $user = $UserModel->get($id);

        $model = new LoginForm;
        $model->sign_in_as($user['username']);
        $this->redirect(HelperUrl::baseUrl());

    }
}