<?php
class PaypalController extends CController {

    public function init() {

    }

    public function actionIpn($type) {
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        if (Yii::app()->params['paypay_sandbox_pointer'])
            $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
        else
            $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if( !($res = curl_exec($ch)) ) {
            curl_close($ch);
            exit;
        }
        curl_close($ch);


        if (strcmp ($res, "VERIFIED") == 0) {

            $payment_status = $_REQUEST['payment_status'];
            $payment_amount = $_REQUEST['mc_gross'];
            $payment_currency = $_REQUEST['mc_currency'];
            $txn_id = $_REQUEST['txn_id'];
            $receiver_email = $_REQUEST['receiver_email'];
            //$payer_email = $_REQUEST['payer_email'];
            $custom = $_REQUEST['custom'];

            $address = "";


            if ($type == 'pricing') {
                if ($payment_status != 'Completed') {
                    return;
                }

                $txn_object = $this->check_txn_id($txn_id);
                /*if ($txn_object)
                    return;*/

                $payment = $this->get_payment($custom);

                if (isset($payment) == false || $payment['payment_gateway'] != 'paypal') return;

                if ($payment['admin_email'] != $receiver_email)
                    return;
                if ($payment['amt'] != $payment_amount )
                    return;
                if ($payment['currency'] != $payment_currency)
                    return;

                $this->update_payment(array(
                    'status' => 1,
                    'confirm_date' => time(),
                    'txn_id' => $txn_id,
                    'id' => $payment['id']
                ));

                $user = $this->get_user($payment['author_id']);

                if (!$user) return;

                $this->update_user(array(
                    'expired_date' => strtotime('+' . $payment['package_time'] . ' ' . $payment['package_time_type'], $user['expired_date']),
                    'id' => $payment['author_id']
                ));
            }



        } else if (strcmp ($res, "INVALID") == 0) {
        }
    }



    public function check_txn_id($txn_id) {
        $sql = "Select *
                From {{pricing_payments}}
                Where txn_id = ?";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute(array($txn_id));
        return $command->queryRow();
    }

    public function get($id) {
        $sql = "Select tr.*, usr.display_name, usr.email, usr.address, usr.phone
                From {{pricing_payments}} tr
                Left Join {{users}} usr On usr.id = tr.author_id
                Where tr.id = ?";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute(array($id));
        return $command->queryRow();
    }

    public function get_payment($id) {
        $sql = "Select *
                From {{pricing_payments}}
                Where id = ?";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute(array($id));
        return $command->queryRow();
    }

    public function get_user($id) {
        $sql = "Select *
                From {{users}}
                Where id = ?";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute(array($id));
        return $command->queryRow();
    }
    public function update_payment($args) {

        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);


        $table = "{{pricing_payments}}";

        $sql = 'update ' . $table . ' set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }
    public function update_user($args) {

        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);


        $table = "{{users}}";

        $sql = 'update ' . $table . ' set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }

}