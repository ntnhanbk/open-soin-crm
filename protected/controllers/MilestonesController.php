<?php

class MilestonesController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "post_sales";
        Yii::app()->params['page'] = "milestones";
    }

    public function actionIndex($order_by = 'milestone', $order_asc = 'asc', $p = 1, $order_id = 0, $status = 0)
    {
        $this->viewData['order_id'] = $order_id;
        $MilestoneModel = new MilestoneModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        if ($order_id != 0)
            $args['order_id'] = $order_id;

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $milestones = $MilestoneModel->gets($args, $p, $ppp);
        $total = $MilestoneModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "milestones/index/order_by/$order_by/order_asc/$order_asc/status/$status/order_id/$order_id/p/", $total, $p) : "";
        $this->viewData['items'] = $milestones;

        $this->render('index', $this->viewData);
    }

    public function actionDeadline($order_id = 0)
    {
        $this->layout = 'modal';

        $MilestoneModel = new MilestoneModel();

        $args['status'] = 0;
        if ($order_id)
            $args['order_id'] = $order_id;

        $milestones = $MilestoneModel->gets($args,0);

        $data['items'] = $milestones;
        $data['order_id'] = $order_id;

        $results['item_view'] = $this->renderPartial('deadline', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionDeadline_handler($order_id = 0)
    {
        $messages[] = "<strong>Deadline</strong> is updated";
        $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);

        $days = trim($_POST['days']);

        $MilestoneModel = new MilestoneModel();

        $args['status'] = 0;
        if ($order_id)
            $args['order_id'] = $order_id;

        $milestones = $MilestoneModel->gets($args,0);

        foreach ($milestones as $k => $v) {
            $deadline = strtotime($days . ' day', $v['milestone']);
            $MilestoneModel->update(array('id' => $v['id'], 'milestone' => $deadline));
        }

        $actions = array(
            array(
                'label' => 'Milestone List',
                'url' => HelperUrl::baseUrl() . 'milestones/index/order_id/'.$order_id,
                'class' => 'btn btn-success',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionAdd($order_id = 0)
    {
        $OrderModel = new OrderModel();
        $orders = $OrderModel->gets(array(), 0);
        $data['orders'] = $orders;

        $order = $OrderModel->get($order_id);
        if ($order) {
            $data['selected_order'] = $order;

            $MilestoneModel = new MilestoneModel();
            $data['remain_amt'] = round($order['amt'] - $MilestoneModel->sum(array('order_id' => $order['id'])), 2);
        }

        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['milestone_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['order_id'] = trim($_POST['order_id']);
        $args['title'] = trim($_POST['title']);
        $args['milestone'] = strtotime($_POST['milestone']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Milestone Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $MilestoneModel = new MilestoneModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $milestone_id = $MilestoneModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Milestone List',
                    'url' => HelperUrl::baseUrl() . 'milestones',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);

        $OrderModel = new OrderModel();
        $orders = $OrderModel->gets(array(), 0);

        $data['item'] = $milestone;
        $data['orders'] = $orders;
        $data['currencies'] = Yii::app()->params['app_data']['currencies'];
        $data['statuses'] = Yii::app()->params['app_data']['milestone_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);
        $args['order_id'] = trim($_POST['order_id']);
        $args['title'] = trim($_POST['title']);
        $args['milestone'] = strtotime($_POST['milestone']);
        $args['date_added'] = strtotime($_POST['date_added']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Milestone Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $MilestoneModel = new MilestoneModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $MilestoneModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Milestone List',
                    'url' => HelperUrl::baseUrl() . 'milestones',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $milestone
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$milestone[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $MilestoneModel = new MilestoneModel();

            $milestone_id = $MilestoneModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $milestone);
            $actions = array(
                array(
                    'label' => 'Milestone List',
                    'url' => HelperUrl::baseUrl() . 'milestones',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $milestone
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/milestone/id/' . $milestone['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($milestone['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/milestone/id/' . $milestone['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($milestone['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Edit Milestone',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'milestones/edit/id/' . $milestone['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($milestone['title']) . '"',
                'id' => ''
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

}