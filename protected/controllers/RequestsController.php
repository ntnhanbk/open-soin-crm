<?php

class RequestsController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "sales";
        Yii::app()->params['page'] = "requests";
    }

    public function actionIndex($order_by = 'date_added', $order_asc = 'desc', $p = 1, $status = 0)
    {
        $RequestModel = new RequestModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $requests = $RequestModel->gets($args, $p, $ppp);
        $total = $RequestModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "requests/index/order_by/$order_by/order_asc/$order_asc/status/$status/p/", $total, $p) : "";
        $this->viewData['items'] = $requests;

        $this->render('index', $this->viewData);
    }

    public function actionAdd()
    {
        $ContactModel = new ContactModel();
        $contacts = $ContactModel->gets(array(), 0);

        $data['contacts'] = $contacts;
        $data['statuses'] = Yii::app()->params['app_data']['request_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['contact_id'] = trim($_POST['contact_id']);
        $args['title'] = trim($_POST['title']);
        $args['description'] = trim($_POST['description']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Request Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $RequestModel = new RequestModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $request_id = $RequestModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Request List',
                    'url' => HelperUrl::baseUrl() . 'requests',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $request
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-default btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/request/id/' . $request['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($request['title']) . '"',
                'id' => ''
            ),

            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-default btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/request/id/' . $request['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($request['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Deliverables',
                'url' => HelperUrl::baseUrl() . 'deliverables/index/request_id/' . $request['id'],
                'class' => 'btn btn-info',
                'attributes' => '',
                'id' => ''
            ),
            array(
                'label' => 'Edit Request',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'requests/edit/id/' . $request['id'] . '" modal-title="<i class=\'fa fa-info\'></i> ' . CHtml::encode($request['title']) . '"',
                'id' => ''
            ),
        );

        if ($request['status'] == 0)
            $actions[] = array(
                'type' => 'btn',
                'url' => '#',
                'id' => 'btn-convert',
                'class' => 'btn btn-success btn-object-modal',
                'label' => 'Convert to Quote',
                'attributes' => 'modal-size="" modal-url="' . HelperUrl::baseUrl() . 'quotes/add/request_id/' . $id . '" modal-title="<i class=\'fa fa-plus\'></i> Convert Request to Quote"',
            );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);

        $ContactModel = new ContactModel();
        $contacts = $ContactModel->gets(array(), 0);

        $data['item'] = $request;
        $data['contacts'] = $contacts;
        $data['statuses'] = Yii::app()->params['app_data']['request_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);
        $args['contact_id'] = trim($_POST['contact_id']);
        $args['title'] = trim($_POST['title']);
        $args['description'] = trim($_POST['description']);
        $args['date_added'] = strtotime($_POST['date_added']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Request Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $RequestModel = new RequestModel();
            $args['last_modified'] = time();
            $args['id'] = $id;

            $RequestModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Request List',
                    'url' => HelperUrl::baseUrl() . 'requests',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $request
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$request[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $RequestModel = new RequestModel();

            $request_id = $RequestModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $request);
            $actions = array(
                array(
                    'label' => 'Request List',
                    'url' => HelperUrl::baseUrl() . 'requests',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}