<?php

class CommentsController extends CController
{
    private $validator;

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page'] = "comments";
    }

    public function actionIndex($type = '', $id = 0)
    {
        $CommentModel = new CommentModel();

        $args = array();
        $args['object_id'] = $id;
        $args['object_type'] = $type;

        $comments = $CommentModel->gets($args, 0);

        $data['items'] = $comments;
        $data['object_id'] = $id;
        $data['object_type'] = $type;
        $data['statuses'] = Yii::app()->params['app_data']['comment_short_statuses'];

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('index', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $CommentModel = new CommentModel();
        $args['object_id'] = trim($_POST['object_id']);
        $args['object_type'] = trim($_POST['object_type']);
        $args['comment'] = trim($_POST['comment']);

        if ($args['object_type']=='deliverable'){
            // update existing comments
            if (isset($_POST['statuses'])){
            $statuses = $_POST['statuses'];
            foreach ($statuses as $k => $v) {
                $CommentModel->update(array('id' => $k, 'status' => $v));
            }
            }
        }

        // add new comment

        $errors = array();
        $messages = array();

        /*if ($this->validator->is_empty_string($args['comment']))
            $errors[] = "Please enter <strong>Comment</strong>";*/

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "Comments are updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success' && $args['comment']!='') {
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $comment_id = $CommentModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
        }

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'View Comments',
                'url' => '#',
                'class' => 'btn btn-warning btn-object-modal',
                'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/' . $args['object_type'] . '/id/' . $args['object_id'] . '" modal-title="<i class=\'fa fa-info\'></i> View Comments"',
                'id' => ''
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }
}