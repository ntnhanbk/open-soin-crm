<?php

class LeadsController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "pre_sales";
        Yii::app()->params['page'] = "leads";
    }

    public function actionIndex($order_by = 'le.title', $order_asc = 'desc', $p = 1)
    {
        $LeadModel = new LeadModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $leads = $LeadModel->gets($args, $p, $ppp);
        $total = $LeadModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "leads/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $leads;

        $this->render('index', $this->viewData);
    }

    public function actionAdd()
    {
        $CampaignModel = new CampaignModel();
        $campaigns = $CampaignModel->gets(array(), 0);

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', array('campaigns' => $campaigns), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['campaign_id'] = trim($_POST['campaign_id']);
        $args['title'] = trim($_POST['title']);
        $args['phone'] = trim($_POST['phone']);
        $args['email'] = trim($_POST['email']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Lead Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $LeadModel = new LeadModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $lead_id = $LeadModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Lead List',
                    'url' => HelperUrl::baseUrl() . 'leads',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $lead
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/lead/id/' . $lead['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($lead['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/lead/id/' . $lead['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($lead['title']) . '"',
                'id' => ''
            ),

        );

        if ($lead['status'] == 0)
            $actions[] = array(
                'type' => 'btn',
                'url' => '#',
                'id' => 'btn-convert',
                'class' => 'btn btn-success btn-object-modal',
                'label' => 'Convert to Contact',
                'attributes' => 'modal-size="" modal-url="' . HelperUrl::baseUrl() . 'contacts/add/lead_id/' . $id . '" modal-title="<i class=\'fa fa-plus\'></i> Convert Lead to Contact"',
            );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);

        $CampaignModel = new CampaignModel();
        $campaigns = $CampaignModel->gets(array(), 0);


        $results['item_view'] = $this->renderPartial('edit', array(
            'item' => $lead,
            'campaigns' => $campaigns
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);
        $args['campaign_id'] = trim($_POST['campaign_id']);
        $args['title'] = trim($_POST['title']);
        $args['phone'] = trim($_POST['phone']);
        $args['email'] = trim($_POST['email']);

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Lead Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $LeadModel = new LeadModel();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;
            $args['id'] = $id;

            $LeadModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Lead List',
                    'url' => HelperUrl::baseUrl() . 'leads',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $lead
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$lead[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $LeadModel = new LeadModel();

            $lead_id = $LeadModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $lead);
            $actions = array(
                array(
                    'label' => 'Lead List',
                    'url' => HelperUrl::baseUrl() . 'leads',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}