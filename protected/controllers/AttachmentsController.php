<?php

class AttachmentsController extends CController
{
    private $validator;
    private $upload_size = 2097152;
    // pdf, jpg, png, gif, doc, docx, xls, xlsx
    private $valid_file_types = array(
        'application/pdf', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/gif', 'application/msword',
        'application/excel', 'application/vnd.ms-excel', 'application/x-excel', 'application/x-msexcel',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page'] = "attachments";
    }

    public function actionIndex($type = '', $id = 0)
    {
        $AttachmentModel = new AttachmentModel();

        $args = array();
        $args['object_id'] = $id;
        $args['object_type'] = $type;

        $attachments = $AttachmentModel->gets($args, 0);

        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('index', array('items' => $attachments, 'object_id' => $id, 'object_type' => $type), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-primary ajax-form btn-save'
            ),
        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['object_id'] = trim($_POST['object_id']);
        $args['object_type'] = trim($_POST['object_type']);

        $errors = array();
        $messages = array();


        $file = $_FILES['file'];
        if ($file && !empty($file['tmp_name'])) {
            $file_info = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($file_info, $file['tmp_name']);

            if (!in_array($mime, $this->valid_file_types)) {
                $errors[] = "<strong>File Type</strong> is incorrect.";
            } else if ($file['size'] > $this->upload_size) {
                $errors[] = "<strong>File Size</strong> is larger than 2MB.";
            }
        }else{
            $errors[] = "Please choose a file";
        }

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
        }

        if ($results['status'] == 'success') {
            $AttachmentModel = new AttachmentModel();

            $attachment = Helper::upload_files($file, $this->upload_size);
            if (isset($attachment) && $attachment && isset($attachment[0])) {
                $args['attachment'] = serialize($attachment[0]);
                $args['attachment_path'] = $attachment[0]['old_name'];
            }

            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $attachment_id = $AttachmentModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

            $messages[] = "<strong>$args[attachment_path]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);

            $actions = array(
                array(
                    'type' => 'close-modal'
                ),
                array(
                    'label' => 'View Attachments',
                    'url' => '#',
                    'class' => 'btn btn-warning btn-object-modal',
                    'attributes' => 'modal-size="modal-lg" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/'.$args['object_type'].'/id/' . $args['object_id'] . '" modal-title="<i class=\'fa fa-info\'></i> View Attachments"',
                    'id' => ''
                ),
            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }
}