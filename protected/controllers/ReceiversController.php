<?php

class ReceiversController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "post_sales";
        Yii::app()->params['page'] = "receivers";
    }

    public function actionIndex($order_by = 'title', $order_asc = 'desc', $p = 1)
    {
        $ReceiverModel = new ReceiverModel();
        $ExpenseModel = new ExpenseModel();

        $args = array(
            'author_id' => Yii::app()->user->id
        );
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $receivers = $ReceiverModel->gets($args, $p, $ppp);
        $total = $ReceiverModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "receivers/index/order_by/$order_by/order_asc/$order_asc/p/", $total, $p) : "";
        $this->viewData['items'] = $receivers;

        $sum_results = array();
        $k = -1;
        $sums = $ExpenseModel->sum_expenses();
        foreach ($sums as $s) {
            $k++;
            $sum_results[$s['receiver']][$k]['status'] = $s['status'];
            $sum_results[$s['receiver']][$k]['currency'] = $s['currency'];
            $sum_results[$s['receiver']][$k]['total'] = $s['total'];
        }
        $this->viewData['sum_results'] = $sum_results;

        $this->render('index', $this->viewData);
    }

    public function actionAdd()
    {
        $this->layout = 'modal';
        $results['item_view'] = $this->renderPartial('add', null, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['title'] = trim($_POST['title']);
        $args['address'] = trim(Helper::request('address'));
        $args['country'] = trim(Helper::request('country'));
        $args['phone'] = trim(Helper::request('phone'));
        $args['email'] = trim(Helper::request('email'));
        $args['job_name']  = trim(Helper::request('job_name'));
        $args['company']  = trim(Helper::request('company'));

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Receiver Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ReceiverModel = new ReceiverModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $receiver_id = $ReceiverModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Receiver List',
                    'url' => HelperUrl::baseUrl() . 'receivers',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $ReceiverModel = new ReceiverModel();
        $receiver = $ReceiverModel->get($id);

        $results['item_view'] = $this->renderPartial('view', array(
            'item' => $receiver
        ), true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),

            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/receiver/id/' . $receiver['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($receiver['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/receiver/id/' . $receiver['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($receiver['title']) . '"',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $ReceiverModel = new ReceiverModel();
        $receiver = $ReceiverModel->get($id);

        $results['item_view'] = $this->renderPartial('edit', array(
            'item' => $receiver
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $ReceiverModel = new ReceiverModel();
        $receiver = $ReceiverModel->get($id);
        $args['title'] = trim($_POST['title']);
        $args['address'] = trim(Helper::request('address'));
        $args['country'] = trim(Helper::request('country'));
        $args['phone'] = trim(Helper::request('phone'));
        $args['email'] = trim(Helper::request('email'));
        $args['job_name']  = trim(Helper::request('job_name'));
        $args['company']  = trim(Helper::request('company'));

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Receiver Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ReceiverModel = new ReceiverModel();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;
            $args['id'] = $id;

            $ReceiverModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Receiver List',
                    'url' => HelperUrl::baseUrl() . 'receivers',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $ReceiverModel = new ReceiverModel();
        $receiver = $ReceiverModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $receiver
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $ReceiverModel = new ReceiverModel();
        $receiver = $ReceiverModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$receiver[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $ReceiverModel = new ReceiverModel();

            $receiver_id = $ReceiverModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $receiver);
            $actions = array(
                array(
                    'label' => 'Receiver List',
                    'url' => HelperUrl::baseUrl() . 'receivers',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}