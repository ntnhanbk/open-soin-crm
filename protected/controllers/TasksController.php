<?php

class TasksController extends CController
{
    private $validator;
    private $viewData = array();

    public function init()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(HelperUrl::baseUrl() . 'site/sign_in');

        $this->validator = new FormValidator();
        Yii::app()->params['page_group'] = "pre_sales";
        Yii::app()->params['page'] = "tasks";
    }

    public function actionIndex($order_by = '', $order_asc = '', $p = 1, $status = 0)
    {
        $TaskModel = new TaskModel();

        $args = array();
        $args['s'] = Helper::request('s');
        $ppp = Yii::app()->getParams()->itemAt('ppp');

        $args['order_by'] = $order_by;
        $args['order_asc'] = $order_asc;
        $this->viewData['next_order_asc'] = ($order_asc == 'asc') ? 'desc' : 'asc';

        $args['status'] = $status;

        $tasks = $TaskModel->gets($args, $p, $ppp);
        $total = $TaskModel->counts($args);

        $this->viewData['total'] = $total;
        $this->viewData['paging'] = $total > $ppp ? Helper::get_paging($ppp, HelperUrl::baseUrl() . "tasks/index/order_by/$order_by/order_asc/$order_asc/status/$status/p/", $total, $p) : "";
        $this->viewData['items'] = $tasks;

        $this->render('index', $this->viewData);
    }

    public function actionDeadline()
    {
        $this->layout = 'modal';

        $TaskModel = new TaskModel();
        $tasks = $TaskModel->gets(array('status' => 0),0);
        $data['items'] = $tasks;

        $results['item_view'] = $this->renderPartial('deadline', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionDeadline_handler()
    {
        $messages[] = "<strong>Deadline</strong> is updated";
        $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);

        $days = trim($_POST['days']);

        $TaskModel = new TaskModel();
        $tasks = $TaskModel->gets(array('status' => 0),0);
        foreach ($tasks as $k => $v) {
            $deadline = strtotime($days . ' day', $v['deadline']);
            $TaskModel->update(array('id' => $v['id'], 'deadline' => $deadline));
        }

        $actions = array(
            array(
                'label' => 'Task List',
                'url' => HelperUrl::baseUrl() . 'tasks',
                'class' => 'btn btn-success',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionAdd()
    {
        $this->layout = 'modal';

        $data['statuses'] = Yii::app()->params['app_data']['task_statuses'];

        $results['item_view'] = $this->renderPartial('add', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'type' => 'submit-form'
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);

    }

    public function actionAdd_handler()
    {
        $args['title'] = trim($_POST['title']);
        $args['tags'] = trim($_POST['tags']);
        $args['start_date'] = strtotime($_POST['start_date']);
        $args['deadline'] = strtotime($_POST['deadline']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Task Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is added";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $TaskModel = new TaskModel();
            $args['date_added'] = time();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;

            $task_id = $TaskModel->add($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Task List',
                    'url' => HelperUrl::baseUrl() . 'tasks',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionView($id)
    {
        $TaskModel = new TaskModel();
        $task = $TaskModel->get($id);

        $data['item'] = $task;
        $data['statuses'] = Yii::app()->params['app_data']['task_statuses'];

        $results['item_view'] = $this->renderPartial('view', $data, true);

        $actions = array(
            array(
                'type' => 'close-modal'
            ),
            array(
                'label' => 'Comments',
                'url' => '#',
                'class' => 'btn btn-primary btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'comments/index/type/task/id/' . $task['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($task['title']) . '"',
                'id' => ''
            ),
            array(
                'label' => 'Attachments',
                'url' => '#',
                'class' => 'btn btn-success btn-object-modal',
                'attributes' => 'modal-size="modal-md" modal-url="' . HelperUrl::baseUrl() . 'attachments/index/type/task/id/' . $task['id'] . '" modal-title="<i class=\'fa fa-attachment\'></i> ' . CHtml::encode($task['title']) . '"',
                'id' => ''
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit($id)
    {
        $TaskModel = new TaskModel();
        $task = $TaskModel->get($id);

        $data['item'] = $task;
        $data['statuses'] = Yii::app()->params['app_data']['task_statuses'];

        $results['item_view'] = $this->renderPartial('edit', $data, true);

        $actions = array(
            array('type' => 'close-modal'),
            array('type' => 'submit-form'),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionEdit_handler($id)
    {
        $TaskModel = new TaskModel();
        $task = $TaskModel->get($id);

        $args['title'] = trim($_POST['title']);
        $args['tags'] = trim($_POST['tags']);
        $args['start_date'] = strtotime($_POST['start_date']);
        $args['deadline'] = strtotime($_POST['deadline']);
        $args['status'] = isset($_POST['status']) ? $_POST['status'] : 0;

        $errors = array();
        $messages = array();

        if ($this->validator->is_empty_string($args['title']))
            $errors[] = "Please enter <strong>Task Name</strong>";

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$args[title]</strong> is updated";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $TaskModel = new TaskModel();
            $args['last_modified'] = time();
            $args['author_id'] = Yii::app()->user->id;
            $args['id'] = $id;

            $TaskModel->update($args);
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);
            $actions = array(
                array(
                    'label' => 'Task List',
                    'url' => HelperUrl::baseUrl() . 'tasks',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

    public function actionDelete($id)
    {
        $TaskModel = new TaskModel();
        $task = $TaskModel->get($id);

        $results['item_view'] = $this->renderPartial('delete', array(
            'item' => $task
        ), true);

        $actions = array(
            array('type' => 'close-modal'),
            array(
                'type' => 'submit-form',
                'class' => 'btn btn-danger btn-save',
                'id' => '',
                'label' => 'Confirm',
            ),

        );

        $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        echo json_encode($results);
    }

    public function actionDelete_handler($id)
    {
        $TaskModel = new TaskModel();
        $task = $TaskModel->get($id);

        $errors = array();
        $messages = array();

        if (count($errors)) {
            $results['status'] = 'error';
            $results['error_messages'] = $this->renderPartial('application.views._shared.error_messages', array('errors' => $errors), true);
        } else {
            $results['status'] = 'success';
            $messages[] = "<strong>$task[title]</strong> is deleted";
            $results['success_messages'] = $this->renderPartial('application.views._shared.success_messages', array('messages' => $messages), true);
        }

        if ($results['status'] == 'success') {
            $TaskModel = new TaskModel();

            $task_id = $TaskModel->update(array('id' => $id, 'deleted' => 1));
            Helper::add_log(Yii::app()->user->id, Yii::app()->controller->id, Yii::app()->controller->action->id, $task);
            $actions = array(
                array(
                    'label' => 'Task List',
                    'url' => HelperUrl::baseUrl() . 'tasks',
                    'class' => 'btn btn-success',
                    'id' => ''
                ),

            );

            $results['action_group'] = $this->renderPartial('application.views._shared.action_group', array('actions' => $actions), true);

        }

        echo json_encode($results);
    }

}