<?php

return array(

    'user_roles' => array(
        'admin' => 'Admin',
        'sub-admin' => 'Sub Admin',
    ),

    'currencies' => array(
        'USD' => 'USD - US Dollars',
        'SGD' => 'SGD - SG Dollars',
        'GBP' => 'GBP - Pounds Sterling',
        'CAD' => 'CAD - Canadian Dollars',
        'EUR' => 'EUR - Euros',
        'VND' => 'VND - Vietnam Dong',
        'JPY' => 'JPY - Yen',
    ),

    'comment_statuses' => array(
        0 => 'New',
        1 => 'Resolved',
        2 => 'Completed',
        -1 => 'Invalid'
    ),

    'comment_short_statuses' => array(
        0 => 'N',
        1 => 'R',
        2 => 'C',
        -1 => 'I'
    ),

    'deliverable_statuses' => array(
        0 => 'New',
        1 => 'Processing',
        2 => 'Resolved',
        3 => 'Testing',
        4 => 'Tested',
        5 => 'Feedback',
        6 => 'Completed',
        -1 => 'Invalid'
    ),

    'languages' => array(
        'en' => 'English (United States)',
        'vn' => 'Tiếng Việt',
    ),

    'payment_terms' => array(
        1 => '30 Days',
        2 => '60 Days',
        3 => '7 Days',
        4 => 'Cash on Delivery',
    ),

    'thumbnail_sizes' => array(
        'thumbnail' => array('w' => 200, 'h' => 200, 'crop' => true),
        'small' => array('w' => 100, 'h' => 100, 'crop' => true),
        'mini' => array('w' => 50, 'h' => 50, 'crop' => true)
    ),

    'company_thumbnail_sizes' => array(
        'thumbnail' => array('w' => 200, 'h' => 200, 'crop' => true),
        'small' => array('w' => 100, 'h' => 100, 'crop' => true),
        'mini' => array('w' => 50, 'h' => 50, 'crop' => true)
    ),

    'user_thumbnail_sizes' => array(
        'thumbnail' => array('w' => 200, 'h' => 200, 'crop' => true),
        'small' => array('w' => 100, 'h' => 100, 'crop' => true),
        'mini' => array('w' => 50, 'h' => 50, 'crop' => true)
    ),

    'campaign_statuses' => array(
        0 => 'New',
        -1 => 'Invalid'
    ),

    'campaign_statuses' => array(
        0 => 'New',
        -1 => 'Invalid'
    ),

    'task_statuses' => array(
        0 => 'New',
        1 => 'Done',
        -1 => 'Invalid'
    ),

    'lead_statuses' => array(
        0 => 'New',
        1 => 'Converted',
        -1 => 'Invalid'
    ),

    'request_statuses' => array(
        0 => 'New',
        1 => 'Converted',
        -1 => 'Invalid'
    ),

    'quote_statuses' => array(
        0 => 'New',
        1 => 'Converted',
        -1 => 'Invalid'
    ),

    'deal_statuses' => array(
        0 => 'New',
        1 => 'Converted',
        -1 => 'Invalid'
    ),

    'order_statuses' => array(
        0 => 'New',
        1 => 'Completed',
        2 => 'Delivered',
        3 => 'Paid',
        -1 => 'Invalid'
    ),

    'milestone_statuses' => array(
        0 => 'New',
        1 => 'Completed',
        -1 => 'Invalid'
    ),

    'invoice_statuses' => array(
        0 => 'New',
        1 => 'Done & Not Paid',
        2 => 'Paid',
        -1 => 'Invalid',
    ),

    'expense_statuses' => array(
        0 => 'New',
        1 => 'Done & Not Paid',
        2 => 'Paid',
        -1 => 'Invalid',
    ),

    'timezone' => array(
        0 => '(UTC -12:00) International Dateline West',
        1 => '(UTC -11:00) Midway Island, Samoa',
        2 => '(UTC -10:00) Hawaii',
        3 => '(UTC -09:00) Alaska',
        4 => '(UTC -08:00) Pacific Time (US & Canada); Tijuana',
        5 => '(UTC -07:00) Mountain Time (US & Canada)',
        6 => '(UTC -06:00) Central Time (US & Canada)',
        7 => '(UTC -05:00) Eastern Time (US & Canada)',
        8 => '(UTC -04:00) Atlantic Time (Canada)',
        9 => '(UTC -03:30) NewfoundLand Time (Canada)',
        10 => '(UTC -03:00) Buenos Aires, Georgetown',
        11 => '(UTC -02:00) Mid-Atlantic',
        12 => '(UTC -01:00) Cape Verde Is.',
        13 => '(UTC  00:00) Dublin, Edinburgh, Lisbon, London',
        14 => '(UTC +01:00) Amsterdam, Berlin, Bern, Rome, Paris, Stockholm, Vienna',
        15 => '(UTC +02:00) Athens, Bucharest, Istanbul, Minsk',
        16 => '(UTC +03:00) Moscow, St. Petersburg, Volgograd',
        17 => '(UTC +03:30) Tehran',
        18 => '(UTC +04:00) Abu Dhabi, Muscat',
        19 => '(UTC +04:30) Kabul',
        20 => '(UTC +05:00) Islamabad, Karachi, Tashkent',
        21 => '(UTC +05:30) Calcutta, Chennai, Mumbai,New Delhi',
        22 => '(UTC +05:45) Kathmandu',
        23 => '(UTC +06:00) Astana,Almaty, Dhaka, Novosibirsk',
        24 => '(UTC +06:30) Rangoon (Yangon, Burma)',
        25 => '(UTC +07:00) Bangkok, Hanoi, Jakarta',
        26 => '(UTC +08:00) Beijing, Chongqing, Hong Kong, Urumqi',
        27 => '(UTC +09:00) Osaka, Sapporo, Tokyo',
        28 => '(UTC +09:30) Adelaide, Darwin',
        29 => '(UTC +10:00) Canberra, Melbourne, Sydney, Vladvostok',
        30 => '(UTC +11:00) Magadan, Solomon Is., New Caledonia',
        31 => '(UTC +12:00) Auckland, Fiji, Kamchatka, Marshall Is.',
        32 => "(UTC +13:00) Nuku'alofa",
    ),

    'receiver_codes' => array(
        'maKraBkLFZ.JU' => 'Hung Pham',
        '292MaLFtKebfc' => 'Nguyen Dao',
        '369MaLFtKebfc' => 'Loi Nguyen',
    ),

    'api_errors' => array(
        'NoName' => "Username is empty. Please enter your username",
        'Illegal' => "Your account is not exists in system.",
        'NotExists' => "The username you provided doesn't exist",
        'EmptyPass' => "Your password is empty. Please enter your password",
        'WrongPass' => "The password you provided is incorrect",
        'WrongPluginPass' => "Same as WrongPass, returned when an authentication plugin rather than MediaWiki itself rejected the password",
        'CreateBlocked' => "The wiki tried to automatically create a new account for you, but your IP address has been blocked from account creation",
        'Throttled' => "You've logged in too many times in a short time. See also throttling",
        'Blocked' => "Your account is blocked",
        'mustbeposted' => "The login module requires a POST request",
        'NeedToken' => "Either you did not provide the login token or the sessionid cookie. Request again with the token and cookie given in this response",
        'API_TOKEN' => 'API Token is Invalid',
        'ACCESS_TOKEN' => 'Access Token is Invalid or Expired'
    ),

    'api_token' => 'y1kanskdy1ihaisuydh813ydasasdt187eas',

    'thumbnails' => array(
        'thumbnail' => array('w' => 255, 'h' => 255, 'crop' => true),
        'banner_img' => array('w' => 570, 'h' => 328, 'crop' => true),
        'small' => array('w' => 100, 'h' => 100, 'crop' => true),
        'mini' => array('w' => 50, 'h' => 50, 'crop' => true)
    ),

    'pricing_packages' => array(
        'free' => array(
            'amt' => 0,
            'currency' => 'USD',
            'time' => 1,
            'time_type' => 'month',
            'accounts' => 5,
            'is_suggest' => false,
            'is_payment' => true,
            'url' => null,
            'title' => 'Basic',
            'description' => 'Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.',
            'features' => array(
                'Feature Name 1',
                'Feature Name 2',
                'Feature Name 3',
                'Feature Name 4',
                'Feature Name 5',
                'Feature Name 6',
            )
        ),
        'standard' => array(
            'amt' => 10,
            'currency' => 'USD',
            'time' => 1,
            'time_type' => 'month',
            'accounts' => 10,
            'is_suggest' => true,
            'is_payment' => true,
            'url' => null,
            'title' => 'Standard',
            'description' => 'Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.',
            'features' => array(
                'Feature Name 1',
                'Feature Name 2',
                'Feature Name 3',
                'Feature Name 4',
                'Feature Name 5',
                'Feature Name 6',
                'Feature Name 7',
                'Feature Name 8'
            )
        ),
        'premium' => array(
            'amt' => 20,
            'currency' => 'USD',
            'time' => 1,
            'time_type' => 'month',
            'accounts' => 20,
            'is_suggest' => false,
            'is_payment' => true,
            'url' => null,
            'title' => 'Premium',
            'description' => 'Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.',
            'features' => array(
                'Feature Name 1',
                'Feature Name 2',
                'Feature Name 3',
                'Feature Name 4',
                'Feature Name 5',
                'Feature Name 6',
                'Feature Name 7',
                'Feature Name 8'
            )
        ),
        /*'contact' => array(
            'amt' => 0,
            'currency' => 'USD',
            'time' => 1,
            'time_type' => 'month',
            'accounts' => 100,
            'is_suggest' => false,
            'is_payment' => false,
            'url' => 'site/contact',
            'title' => 'Contact Us',
            'description' => 'Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.',
            'features' => array(
                'Feature Name 1',
                'Feature Name 2',
                'Feature Name 3',
                'Feature Name 4',
                'Feature Name 5',
                'Feature Name 6'
            )
        )*/
    )

);
