<?php
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Soin CRM',
    'defaultController' => 'site',
    'preload' => array('log'),
    'timeZone' => 'Asia/Ho_Chi_Minh',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'application.widgets.*',
    ),
    'modules' => array(
        'api' => array(),
    ),
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
        ),

        'dompdf' => array(
            'class' => 'application.components.dompdf.yiidompdf'
        ),

        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array_merge(
                require_once('routing.php'), array(
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                )
            ),
        ),

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=soincrm2',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'soinmedia',
            'charset' => 'utf8',
            'tablePrefix' => 'crm_'
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                /*array(
                   'class' => 'CWebLogRoute',
                ),*/
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'upload_dir' => '/Applications/Mamp/htdocs/soin-crm-2.0/upload/',
        'upload_url' => "http://soincrm2.dev/upload/",
        'ppp' => 20,
        'app_data' => require_once('app_data.php'),

        /** Paypal config */
        'paypay_sandbox_pointer' => true,
        'paypal_address' => 'mtcm_business@gmail.com',
        'payment_amount' => '75',
        'payment_currency' => 'USD',
    ),
);