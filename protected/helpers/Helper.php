<?php

class Helper
{

    public static function add_log($admin_id, $controller, $method, $description = "")
    {
        $LogModel = new LogModel();
        $args = array(
            'user_id' => $admin_id,
            'user_ip' => $_SERVER['REMOTE_ADDR'],
            'access_controller' => $controller,
            'access_method' => $method,
            'description' => serialize($description),
            'date_added' => time()
        );
        $LogModel->add($args);
    }

    public static function get_paging($ppp, $link_server, $total, $current_page)
    {
        $p = new Paginator();
        $p->items_per_page = $ppp;
        $p->current_page = $current_page;
        $p->link_server = $link_server;
        $p->items_total = $total;
        $p->paginate();
        return $p->display_pages();
    }

    public static function request($name, $default = null)
    {
        if (isset($_REQUEST[$name]))
            return $_REQUEST[$name];
        return $default;
    }

    public static function date($date, $format = 'M d, Y')
    {
        return date($format, $date);
    }

    public static function upload_files($files, $allow_size = 3145728, $destination = "default")
    {
        $total = count($files['name']);
        if ($total <= 0)
            return false;

        $validator = new FormValidator();
        $uploaded_items = array();

        if ($total == 1) {
            $file_info = pathinfo($files['name']);
            $file_name = self::base32UUID() . "." . $file_info['extension'];
            if ($destination == "default")
                $destination = "media/" . date('Y') . '/' . date('m') . '/';
            $upload_dir = HelperUrl::upload_dir() . $destination;
            self::make_folder($upload_dir);
            try {

                if (!$validator->is_valid_file($files, $allow_size))
                    return false;

                move_uploaded_file($files['tmp_name'], $upload_dir . $file_name);
                $new_file = array('name' => $file_name, 'size' => $files['size'], 'type' => $files['type'], 'url' => $destination . $file_name, 'old_name' => $files['name']);
                //var_dump($newFile);
                array_push($uploaded_items, $new_file);
            } catch (Exception $er) {
                echo $er->getMessage();
            }
        } else {
            for ($i = 0; $i < $total; $i++) {
                $file_info = pathinfo($files['name'][$i]);
                $file_name = self::base32UUID() . "." . $file_info['extension'];
                if ($destination == "default")
                    $destination = "media/" . date('Y') . '/' . date('m') . '/';
                $upload_dir = HelperUrl::upload_dir() . $destination;
                self::make_folder($upload_dir);
                try {
                    $file = array('name' => $files['name'][$i], 'type' => $files['type'][$i], 'tmp_name' => $files['tmp_name'][$i], 'error' => $files['error'][$i], 'size' => $files['size'][$i]);
                    if (!$validator->is_valid_file($file, $allow_size))
                        continue;

                    move_uploaded_file($files['tmp_name'][$i], $upload_dir . $file_name);
                    $new_file = array('name' => $file_name, 'size' => $files['size'][$i], 'type' => $files['type'][$i], 'url' => $destination . $file_name, 'old_name' => $files['name'][$i]);
                    //var_dump($newFile);
                    array_push($uploaded_items, $new_file);
                } catch (Exception $er) {
                    echo $er->getMessage();
                }
            }
        }
        return $uploaded_items;
    }


    public static function make_folder($folderpath)
    {
        @mkdir($folderpath, 0777, true);
        @chmod($folderpath, 0777);
        // chmod parent folder
        $folder = pathinfo($folderpath);
        @chmod($folder['dirname'], 0777);
    }

    public static function base32UUID()
    {
        $result = self::_custombase_convert(strrev(uniqid()), '0123456789abcdef', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        while (strlen($result) != 9) {
            $result = self::_custombase_convert(strrev(uniqid()), '0123456789abcdef', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        }
        return $result;
    }

    private static function _custombase_convert($numstring, $baseFrom = "0123456789", $baseTo = "0123456789")
    {
        $numstring = (string)$numstring;
        $baseFromLen = strlen($baseFrom);
        $baseToLen = strlen($baseTo);
        if ($baseFrom == "0123456789") { // No analyzing needed, because $numstring is already decimal
            $decVal = (int)$numstring;
        } else {
            $decVal = 0;
            for ($len = (strlen($numstring) - 1); $len >= 0; $len--) {
                $char = substr($numstring, 0, 1);
                $pos = strpos($baseFrom, $char);
                if ($pos !== FALSE) {
                    $decVal += $pos * ($len > 0 ? pow($baseFromLen, $len) : 1);
                }
                $numstring = substr($numstring, 1);
            }
        }
        if ($baseTo == "0123456789") { // No converting needed, because $numstring needs to be converted to decimal
            $numstring = (string)$decVal;
        } else {
            $numstring = FALSE;
            $nslen = 0;
            $pos = 1;
            while ($decVal > 0) {
                $valPerChar = pow($baseToLen, $pos);
                $curChar = floor($decVal / $valPerChar);

                if ($curChar >= $baseToLen) {
                    $pos++;
                } else {
                    $decVal -= ($curChar * $valPerChar);
                    if ($numstring === FALSE) {
                        $numstring = str_repeat($baseTo{1}, $pos);
                        $nslen = $pos;
                    }

                    $numstring = substr($numstring, 0, ($nslen - $pos)) . $baseTo{(int)$curChar} . substr($numstring, (($nslen - $pos) + 1));
                    $pos--;
                }
            }
            if ($numstring === FALSE)
                $numstring = $baseTo{1};
        }
        return $numstring;
    }

    public static function label_task_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Converted:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_task_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['task_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Converted:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_lead_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Converted:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_lead_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['lead_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Converted:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_request_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Converted:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_request_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['request_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Converted:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_quote_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Converted:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_quote_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['quote_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Converted:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_deal_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Converted:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_deal_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['deal_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Converted:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_order_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Completed:
                $str_status = 'warning';
                break;
            case 2: // Delivered:
                $str_status = 'danger';
                break;
            case 3: // Paid:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }


    public static function display_order_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['order_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Completed:
                $str_status .= 'label-warning';
                break;
            case 2: // Delivered:
                $str_status .= 'label-danger';
                break;
            case 3: // Paid:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }


    public static function label_milestone_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Completed:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_milestone_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['milestone_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Completed:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }


    public static function label_invoice_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Done & Not Paid:
                $str_status = 'danger';
                break;
            case 2: // Paid
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_invoice_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['invoice_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Done & Not Paid:
                $str_status .= 'label-danger';
                break;
            case 2: // Paid
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_expense_status($status = 0)
    {
        switch ($status) {
            case 0: // New
                $str_status = 'info';
                break;
            case 1: // Done & Not Paid:
                $str_status = 'danger';
                break;
            case 2: // Paid
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_expense_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['expense_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-info';
                break;
            case 1: // Done & Not Paid:
                $str_status .= 'label-danger';
                break;
            case 2: // Paid
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }


    public static function get_app_data($key = null) {
        $app_data = Yii::app()->params['app_data'];
        if (!$key)
            return $app_data;

        if (isset($app_data[$key]))
            return $app_data[$key];

        return null;
    }

    public static function email($to, $name = '', $subject, $message, $footer = true, $from = 'no-reply@realcrm.soinsofts.com')
    {
        $header = "Content-type: text/html; charset=UTF-8\r\n" .
            "From: SoinCRM Support Team <" . $from . ">\r\n" .
            "Date: " . date('r') . "\r\n";
        @mail($to, "[SoinCRM] " . $subject, $message, $header);
    }

    public static function generate_random($length = 10) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public static function generate_secret_key($username, $email) {
        $secret_key = Ultilities::base32UUID();
        return $secret_key;
    }

    public static function make_url_clickable($s) {
        return preg_replace (
            "/(?<!a href=\")(?<!src=\")((http|ftp)+(s)?:\/\/[^<>\s]+)/i",
            "<a href=\"\\0\" target=\"blank\">\\0</a>",
            $s
        );
    }

    public static function get_end_date_of_month($first_day){
        return strtotime(date('d M Y', strtotime(date('d-m-Y', strtotime('1-' . date('m-Y', $first_day) . ' +1 month')) . ' 23:59 -1 day')) . " 23:59:59");
    }

    public static function get_thumbnail($sizes, $size = 'thumbnail')
    {
        $sizes = unserialize($sizes);
        if (isset($sizes[$size]['filename']))
            if (file_exists(HelperUrl::upload_dir() . "media/" . $sizes[$size]['folder'] . $sizes[$size]['filename']))
                return HelperUrl::upload_url(true) . "media/" . $sizes[$size]['folder'] . $sizes[$size]['filename'];
        return HelperUrl::baseUrl(true) . "assets/imgs/default.png";
    }

    public static function label_deliverable_status($status = 0)
    {
        switch ($status) {
            case -1: // New
                $str_status = 'draft';
                break;
            case 0: // New
                $str_status = 'default';
                break;
            case 1: // Processing:
                $str_status = 'info';
                break;
            case 2: // Resolved:
                $str_status = 'primary';
                break;
            case 3: // Testing:
                $str_status = 'info';
                break;
            case 4: // Tested:
                $str_status = 'danger';
                break;
            case 5: // Feedback:
                $str_status = 'warning';
                break;
            case 6: // Completed:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_deliverable_status($status = 0)
    {
        $statuses = Yii::app()->params['app_data']['deliverable_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case -1: // New
                $str_status .= 'label-default';
                break;
            case 0: // New
                $str_status .= 'label-default';
                break;
            case 1: // Processing:
                $str_status .= 'label-info';
                break;
            case 2: // Resolved:
                $str_status .= 'label-primary';
                break;
            case 3: // Testing:
                $str_status .= 'label-info';
                break;
            case 4: // Tested
                $str_status .= 'label-danger';
                break;
            case 5: // Feedback
                $str_status .= 'label-warning';
                break;
            case 6: // Completed
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function label_comment_status($status = 0)
    {
        $status = intval($status);
        switch ($status) {
            case 0: // New
                $str_status = 'default';
                break;
            case 1: // Resolved:
                $str_status = 'primary';
                break;
            case 2: // Completed:
                $str_status = 'success';
                break;
            case -1: // Invalid
                $str_status = 'default';
                break;
        }
        return $str_status;
    }

    public static function display_comment_status($status = 0)
    {
        $status = intval($status);
        $statuses = Yii::app()->params['app_data']['comment_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-default';
                break;
            case 1: // Resolved:
                $str_status .= 'label-primary';
                break;
            case 2: // Completed:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }

    public static function display_comment_short_status($status = 0)
    {
        $status = intval($status);
        $statuses = Yii::app()->params['app_data']['comment_short_statuses'];
        $str_status = '<span class="label ';

        switch ($status) {
            case 0: // New
                $str_status .= 'label-default';
                break;
            case 1: // Resolved:
                $str_status .= 'label-primary';
                break;
            case 2: // Completed:
                $str_status .= 'label-success';
                break;
            case -1: // Invalid
                $str_status .= 'label-default';
                break;
        }

        $str_status .= '">' . $statuses[$status] . '</span>';
        return $str_status;
    }


    public static function do_resize($remote_url, $sizes, $filename, $upload_dir, $old_filename = '')
    {
        $data = array();
        $img = new SimpleImage();
        $img->load($remote_url);
        self::make_folder($upload_dir);
        if ($old_filename)
            @unlink($upload_dir . $old_filename);
        $filepath = $upload_dir . $filename;
        $width = $img->getWidth();
        $height = $img->getHeight();
        $img->resizeToThumb($width, $height);
        $img->save_with_default_imagetype($filepath);

        foreach ($sizes as $size_name => $size) {
            $img->load($filepath);

            if ($size['w'] == 0) {
                $new_filename = $size['h'] . 'h-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['h'] . 'h-' . $old_filename;
            } elseif ($size['h'] == 0) {
                $new_filename = $size['w'] . 'w-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['w'] . 'w-' . $old_filename;
            } else {
                $new_filename = $size['w'] . 'x' . $size['h'] . '-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['w'] . 'x' . $size['h'] . '-' . $old_filename;
            }
            $folder = str_replace(HelperUrl::upload_dir() . "media/", '', $upload_dir);

            $new_size = '';
            if ($size['w'] == 0) {
                if ($height > $size['h'])
                    $new_size = $img->resizeToHeight($size['h']);
            } elseif ($size['h'] == 0) {
                if ($width > $size['w'])
                    $new_size = $img->resizeToWidth($size['w']);
            } else {
                if ($height >= $size['h'] && $width >= $size['w'])
                    $new_size = $img->resizeToThumb($size['w'], $size['h']);
            }

            if ($new_size) {
                if ($old_filename)
                    @unlink($upload_dir . $new_oldfilename);
                $img->save_with_default_imagetype($upload_dir . '/' . $new_filename);
                $data[$size_name] = array(
                    'folder' => $folder,
                    'filename' => $new_filename,
                    'width' => $new_size['w'],
                    'height' => $new_size['h']
                );
            }
        }

        $data['full'] = array(
            'folder' => $folder,
            'filename' => $filename,
            'width' => $width,
            'height' => $height
        );
        return $data;
    }

    public static function resize_images($file, $sizes = null, $old_name = '')
    {
        if (!$sizes)
            $sizes = self::get_app_data('thumbnails');
        $image_info = getimagesize($file['tmp_name']);

        $img = self::base32UUID() . "." . self::image_types($image_info['mime']);
        $upload_dir = HelperUrl::upload_dir() . "media/" . date('Y') . '/' . date('m') . '/';
        $thumbnail = serialize(self::do_resize($file['tmp_name'], $sizes, $img, $upload_dir, $old_name));
        return array('img' => $img, 'thumbnail' => $thumbnail);
    }

    public static function image_types($type)
    {
        $image_type = array('image/png' => 'png', 'image/jpeg' => 'jpg', 'image/gif' => 'gif');
        return $image_type[$type];
    }

    public static function is_serialized($data)
    {
        // if it isn't a string, it isn't serialized
        if (!is_string($data))
            return false;
        $data = trim($data);
        if ('N;' == $data)
            return true;
        if (!preg_match('/^([adObis]):/', $data, $badions))
            return false;
        switch ($badions[1]) {
            case 'a' :
            case 'O' :
            case 's' :
                if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data))
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data))
                    return true;
                break;
        }
        return false;
    }

    public static function display_user_role($role) {
        $roles = self::get_app_data('user_roles');
        if (isset($roles[$role]))
            return $roles[$role];
        return 'Anonymous';
    }

    public static function get_pricing_list() {
        return self::get_app_data('pricing_packages');
    }

    public static function get_pricing_item($item) {
        $arr = self::get_pricing_list();
        return isset($arr[$item]) ? $arr[$item] : null;
    }

    public static function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array()) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }


}