<?php
class PaypalHelper {
    public $sandbox = true;
    public $business;
    public $item_name;
    public $quantity = 1;
    public $amount;
    public $currency_code = "USD";
    public $custom;
    public $return;
    public $notify_url;

    public function get_payment_url() {
        $url = "https://www.sandbox.paypal.com/webscr?cmd=_xclick";
        if ($this->sandbox == false) {
            $url = "https://www.paypal.com/webscr?cmd=_xclick";
        }
        $url .= "&add=" . $this->quantity . "&business=" . $this->business . "&item_name=" . $this->item_name . "&amount=" . $this->amount . "&currency_code=" . $this->currency_code . "&custom=" . $this->custom . "&return=" . $this->return . "&notify_url=" . $this->notify_url;

        return $url;
    }

}