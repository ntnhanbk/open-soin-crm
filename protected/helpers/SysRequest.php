<?php
class SysRequest {
    public static function params($name, $default = null) {
        if (isset(Yii::app()->params[$name]) && Yii::app()->params[$name])
            return Yii::app()->params[$name];
        return $default;
    }

    public static function set_params($name, $content) {
        Yii::app()->params[$name] = $content;
    }

    public static function find($name, $default = null) {
        if (isset($_REQUEST[$name]))
            return $_REQUEST[$name];
        return $default;
    }

    public static function is_post() {
        if (isset($_POST) && $_POST)
            return true;
        return false;
    }

    public static function post($name, $default = null) {
        if (isset($_POST[$name]))
            return $_POST[$name];
        return $default;
    }

    public static function get($name, $default = null) {
        if (isset($_GET[$name]))
            return $_GET[$name];
        return $default;
    }

    public static function file($name, $default = null) {
        if (isset($_FILES[$name]))
            return $_FILES[$name];
        return $default;
    }

    public static function redirect($url,$terminate=true,$statusCode=302)
    {
        Yii::app()->getRequest()->redirect($url,$terminate,$statusCode);
    }

    public static function is_ajax_request() {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }
}