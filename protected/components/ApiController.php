<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ApiController extends CController
{

    public function init()
    {

    }

    /** User */
    protected $_user = null;

    /** Result */
    protected $result = array(
        'status' => 0, /** Success: 1 or Error: 0 */
        'message' => '', /** Array message */
        'content' => array(),
    );

    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    /**
     * @return array action filters
     */

    public function CheckPermission()
    {
        $controller = $this->controllerID();
        $method = $this->methodID();
        HelperGlobal::AccessControl($controller, $method);
        HelperGlobal::check_online();
    }

    public function controllerID()
    {
        return Yii::app()->getController()->getId();
    }

    public function methodID()
    {
        return Yii::app()->getController()->getAction()->getId();
    }

    public function load_404()
    {
        $this->renderFile(Yii::app()->basePath . "/views/layouts/404.php");
        die;
    }

    public function load_401()
    {
        $this->renderFile(Yii::app()->basePath . "/views/layouts/401.php");
        die;
    }

    /* Functions to set header with status code. eg: 200 OK ,400 Bad Request etc..*/
    public function setHeader($status)
    {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Nintriva <nintriva.com>");
    }

    public function _getStatusCodeMessage($status)
    {
        $codes = Array(
            200 => 'OK',
            304 => 'Not Modified',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway timeout'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';

            echo $body;
        }
        Yii::app()->end();
    }

    public function _checkAuth()
    {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $_SERVER['HTTP_X_USERNAME'];
        $password = $_SERVER['HTTP_X_PASSWORD'];

        // Find the user
        $UserModel = new UserModel();
        $user = $UserModel->get_by_username($username);
        if ($user === null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!$UserModel->validate_password($password, $user['password'], $user['secret_key'])) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }


    protected function login_errors($key = null) {
        $arr = Helper::get_app_data('api_errors');
        if (!$key) return $arr;
        return isset($arr[$key]) ? $arr[$key] : null;
    }

    public function _checkAuth_by_token() {
        /** Get token key */
        $token_key = trim(Yii::app()->request->getParam('access_key'));

        $TokenModel = new TokenModel();
        $token = $TokenModel->get_by_token($token_key);

        if (!$token || $token['expired_date'] <= time()) {
            $this->result = array (
                'status' => 0,
                'message' => $this->login_errors('ACCESS_TOKEN'),
                'content' => array()
            );
            $this->_sendResponse(401, CJSON::encode($this->result));
        }

        $UserModel = new UserModel();
        $user = $UserModel->get($token['user_id']);
        if (!$user) {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('ACCESS_TOKEN'),
                'content' => array()
            );
            $this->_sendResponse(401, CJSON::encode($this->result));
        }

        $this->_user = $user;
    }

    public function _checkAPI_token() {
        $api_token = trim(Yii::app()->request->getParam('api_token'));

        if ($api_token != Helper::get_app_data('api_token')) {
            $this->result = array (
                'status' => 0,
                'message' => $this->login_errors('API_TOKEN'),
                'content' => array()
            );
            $this->_sendResponse(401, CJSON::encode($this->result));
        }
    }

    public function _load_404($message = 'Page Not Found') {
        $this->result = array (
            'status' => 0,
            'message' => $message,
            'content' => array()
        );
        $this->_sendResponse(404, CJSON::encode($this->result));
    }

    public function _load_401($message = 'You must be authorized to view this page.') {
        $this->result = array (
            'status' => 0,
            'message' => $message,
            'content' => array()
        );
        $this->_sendResponse(401, CJSON::encode($this->result));
    }

    public function load_token_key($token_key = null) {
        if (!$token_key) {
            $token_key = Helper::request('access_key');
        }

        $TokenModel = new TokenModel();
        $token = $TokenModel->get_by_token($token_key);
        if (!$token)
            $this->_load_401();

        if ($token['expired_date'] <= time()) {
            $this->_load_401();
        }
    }

}