<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

    public function authenticate()
    {
        $UserModel = new UserModel();
        $record = $UserModel->get_by_username($this->username);
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($record['password'] !== crypt($this->password, $record['secret_key']))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $record['id'];
            $this->setState('username', $record['username']);
            $this->setState('role', $record['role']);
            $this->setState('fullname', $record['first_name'].' '.$record['last_name']);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }


    public function sign_in_as()
    {
        $UserModel = new UserModel();
        $record = $UserModel->get_by_username($this->username);
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else {
            $this->_id = $record['id'];
            $this->setState('username', $record['username']);
            $this->setState('role', $record['role']);
            $this->setState('fullname', $record['first_name'].' '.$record['last_name']);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
}