<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lead Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Leads</li>
        </ol>
    </div>
</div>



<div class="container-fluid">
        <div class="text-right space10">
            <a href="" class="btn btn-primary btn-object-modal" modal-size="" modal-url="<?php echo HelperUrl::baseUrl(); ?>leads/add" modal-title="<i class='fa fa-plus'></i> Add New Lead"><i class="fa fa-plus"></i> Add New Lead</a>
        </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Leads
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th width="25%"><a href="<?php echo HelperUrl::baseUrl() . "leads/index/order_by/campaign_title/order_asc/$next_order_asc"; ?>">Campaign</a></th>
                    <th width="20%"><a href="<?php echo HelperUrl::baseUrl() . "leads/index/order_by/title/order_asc/$next_order_asc"; ?>">Lead Name</a></th>
                    <th width="10%"><a href="<?php echo HelperUrl::baseUrl() . "leads/index/order_by/phone/order_asc/$next_order_asc"; ?>">Phone</a></th>
                    <th width="20%"><a href="<?php echo HelperUrl::baseUrl() . "leads/index/order_by/email/order_asc/$next_order_asc"; ?>">Email</a></th>
                    <th class="text-center" width="10%">
                        <a href="<?php echo HelperUrl::baseUrl() . "leads/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created Date</a>
                    </th>
                    <th class="text-center" width="15%" colspan="5">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="10">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td><?php echo CHtml::encode($v['campaign_title']) ?></td>
                        <td>

                            <a class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'leads/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo Helper::display_request_status($v['status']);?>
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>

                        </td>
                        <td><?php echo CHtml::encode($v['phone']) ?></td>
                        <td><?php echo CHtml::encode($v['email']) ?></td>
                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'leads/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'leads/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>


    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>

