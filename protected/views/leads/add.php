<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>leads/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Campaign</label>
        <select class="form-control" name="campaign_id">
            <option value="0">-- Choose Campaign --</option>
            <?php foreach ($campaigns as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Lead Name</label>
        <input type="text" class="form-control" name="title" placeholder="">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone" placeholder="">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" placeholder="">
    </div>
</form>