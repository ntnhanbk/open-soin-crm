<form method="post" action="<?php echo HelperUrl::baseUrl() . 'leads/edit_handler/id/' . $item['id'] ?>"
      enctype="multipart/form-data">
    <div class="form-group">
        <label>Campaign</label>
        <select class="form-control" name="campaign_id">
            <option value="0">-- Choose Campaign --</option>
            <?php foreach ($campaigns as $k => $v): ?>
                <option
                    value="<?php echo $v['id']; ?>" <?php echo ($item['campaign_id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Lead Name</label>
        <input type="text" class="form-control" name="title" placeholder=""
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone" placeholder=""
               value="<?php echo CHtml::encode($item['phone']) ?>">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" placeholder=""
               value="<?php echo CHtml::encode($item['email']) ?>">
    </div>
</form>