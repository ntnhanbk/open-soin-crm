<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['title']) ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="20%"><strong>Campaign</strong></td>
        <td width="80%"><?php echo CHtml::encode($item['campaign_title']) ?></td>
    </tr>
    <tr>
        <td><strong>Phone</strong></td>
        <td><?php echo CHtml::encode($item['phone']) ?></td>
    </tr>
    <tr>
        <td><strong>Email</strong></td>
        <td><?php echo CHtml::encode($item['email']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>