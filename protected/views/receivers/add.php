<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>receivers/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Receiver Name *</label>
        <input type="text" class="form-control" name="title">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone">
    </div>
    <div class="form-group">
        <label>Address</label>
        <input type="text" class="form-control" name="address">
    </div>
    <div class="form-group">
        <label>Job</label>
        <input type="text" class="form-control" name="job_name">
    </div>
    <div class="form-group">
        <label>Company</label>
        <input type="text" class="form-control" name="company">
    </div>
</form>