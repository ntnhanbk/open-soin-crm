<form method="post" action="<?php echo HelperUrl::baseUrl() . 'receivers/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Receiver Name *</label>
        <input type="text" class="form-control" name="title"
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div><div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email"
               value="<?php echo CHtml::encode($item['email']) ?>">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone"
               value="<?php echo CHtml::encode($item['phone']) ?>">
    </div>
    <div class="form-group">
        <label>Address</label>
        <input type="text" class="form-control" name="address"
               value="<?php echo CHtml::encode($item['address']) ?>">
    </div>
    <div class="form-group">
        <label>Job</label>
        <input type="text" class="form-control" name="job_name"
               value="<?php echo CHtml::encode($item['job_name']) ?>">
    </div>
    <div class="form-group">
        <label>Company</label>
        <input type="text" class="form-control" name="company"
               value="<?php echo CHtml::encode($item['company']) ?>">
    </div>
</form>