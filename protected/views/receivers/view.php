<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['title']) ?></th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td width="40%"><strong>Email</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['email']) ?></td>
    </tr>

    <tr>
        <td width="40%"><strong>Phone</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['phone']) ?></td>
    </tr>

    <tr>
        <td width="40%"><strong>Address</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['address']) ?></td>
    </tr>

    <tr>
        <td width="40%"><strong>Job</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['job_name']) ?></td>
    </tr>

    <tr>
        <td width="40%"><strong>Company</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['company']) ?></td>
    </tr>

    <tr>
        <td width="40%"><strong>Created Date</strong></td>
        <td width="60%"><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>