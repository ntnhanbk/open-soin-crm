<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="25%">Order Name</th>
        <th width="35%">Expense Name</th>
        <th width="25%" class="text-right">Amount</th>
        <th class="text-center" width="15%">Created</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="4">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php foreach ($items as $k => $v): ?>
        <tr>
            <td><?php echo CHtml::encode($v['order_title']) ?></td>
            <td>
                <a class="btn-object-modal" modal-size=""
                   modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view/id/' . $v['id'] ?>"
                   modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                    <?php echo Helper::display_expense_status($v['status']); ?>
                    <?php echo CHtml::encode($v['title']) ?>
                </a>
            </td>
            <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
            <td class="text-center"><?php echo CHtml::encode(Helper::date($v['date_added'])) ?></td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>