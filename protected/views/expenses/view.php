<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2">
            <?php echo Helper::display_expense_status($item['status']);?>
            <?php echo CHtml::encode($item['title']) ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="20%"><strong>Order Name</strong></td>
        <td width="80%"><?php echo CHtml::encode($item['order_title']) ?></td>
    </tr>
    <tr>
        <td><strong>Amount</strong></td>
        <td><?php echo CHtml::encode($item['currency'].' '.number_format($item['amt'],2)) ?></td>
    </tr>
    <tr>
        <td><strong>Receiver</strong></td>
        <td><?php echo CHtml::encode($item['receiver_title']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>