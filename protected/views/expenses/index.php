<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Expense Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Expenses</li>
        </ol>
    </div>
</div>


<div class="container-fluid">

    <div class="space10">
        <div class="clearfix">
            <div class="pull-left">
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>expenses/index/status/0" class="btn btn-info">New Expenses</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>expenses/index/status/1" class="btn btn-danger">Done & Not Paid Expenses</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>expenses/index/status/2" class="btn btn-success">Paid Expenses</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>expenses/index/status/-1" class="btn btn-default">Invalid Expenses</a>
                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="" class="btn btn-primary btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl(); ?>expenses/add"
                       modal-title="<i class='fa fa-plus'></i> Add New Expense"><i class="fa fa-plus"></i> Add New Expense</a>
                </div>
            </div>
        </div>
    </div>

    <div class="space10">&nbsp;</div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Expenses
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th width="15%"><a
                            href="<?php echo HelperUrl::baseUrl() . "expenses/index/order_by/order_title/order_asc/$next_order_asc"; ?>">Order
                            Name</a></th>
                    <th width="30%"><a
                            href="<?php echo HelperUrl::baseUrl() . "expenses/index/order_by/title/order_asc/$next_order_asc"; ?>">Expense
                            Name</a></th>
                    <th width="15%" class="text-right"><a
                            href="<?php echo HelperUrl::baseUrl() . "expenses/index/order_by/amt/order_asc/$next_order_asc"; ?>">Amount</a>
                    </th>
                    <th width="15%" class="text-left"><a
                            href="<?php echo HelperUrl::baseUrl() . "expenses/index/order_by/receiver_title/order_asc/$next_order_asc"; ?>">Receiver</a>
                    </th>
                    <th class="text-center" width="10%">
                        <a href="<?php echo HelperUrl::baseUrl() . "expenses/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created</a>
                    </th>
                    <th class="text-center" width="15%" colspan="5">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="9">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td><?php echo CHtml::encode($v['order_title']) ?></td>
                        <td>
                            <a class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo Helper::display_expense_status($v['status']); ?>
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>
                        </td>
                        <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
                        <td><?php echo CHtml::encode($v['receiver_title']) ?></td>
                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>

        </div>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>

