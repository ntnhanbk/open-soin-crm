<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="15%">Order Name</th>
        <th width="30%">Expense Name</th>
        <th width="15%" class="text-right">Amount</th>
        <th width="15%" class="text-left">Receiver</th>
        <th class="text-center" width="10%">Created</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="4">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php $sum=0; foreach ($items as $k => $v): $sum+=$v['amt'];?>
        <tr>
            <td><?php echo CHtml::encode($v['order_title']) ?></td>
            <td>
                <?php echo Helper::display_expense_status($v['status']); ?>
                <a class="btn-object-modal" modal-size=""
                   modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view/id/' . $v['id'] ?>"
                   modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                    <?php echo CHtml::encode($v['title']) ?>
                </a>
            </td>
            <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
            <td><?php echo CHtml::encode($v['receiver']) ?></td>
            <td class="text-center">
                <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tfooter>
        <tr>
            <td colspan="2" class="text-center"><strong>Total</strong></td>
            <td class="text-right"><strong>$ <?php echo number_format($sum);?></strong></td>
            <td colspan="2"></td>
        </tr>
    </tfooter>
    </tbody>
</table>