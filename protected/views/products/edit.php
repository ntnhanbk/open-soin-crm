<form method="post" action="<?php echo HelperUrl::baseUrl() . 'products/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Product Name</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Product Name"
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
</form>