<form method="post" action="<?php echo HelperUrl::baseUrl() . 'invoices/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Order</label>
        <select class="form-control" name="order_id">
            <option value="0">-- Choose Order --</option>
            <?php foreach ($orders as $k => $v): ?>
                <option
                    value="<?php echo $v['id']; ?>" <?php echo ($item['order_id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Invoice Name</label>
        <input type="text" class="form-control" name="title" placeholder=""
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-control" name="currency">
            <option value="0">-- Choose Currency --</option>
            <?php foreach ($currencies as $k => $v): ?>
                <option
                    value="<?php echo $k; ?>" <?php echo ($item['currency'] == $k) ? 'selected' : ''; ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control" name="amt" placeholder=""
               value="<?php echo CHtml::encode($item['amt']) ?>">
    </div>

    <div class="form-group">
        <label>Created Date</label>
        <input type="text" class="form-control datepicker" name="date_added" value="<?php echo date('m/d/Y H:i',$item['date_added']) ?>">
    </div>

    <div class="form-group">
        <label>Status</label><br/>
        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_invoice_status($k);?> <?php echo ($item['status']==$k)?'active':''; ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>" <?php echo ($item['status']==$k)?'checked':''; ?>> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>