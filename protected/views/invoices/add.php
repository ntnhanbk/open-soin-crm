<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>invoices/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Order</label>
        <select class="form-control" name="order_id">
            <option value="0">-- Choose Order --</option>
            <?php foreach ($orders as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>" <?php echo (isset($selected_order['id']) && $selected_order['id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Invoice Name</label>
        <input type="text" class="form-control" name="title" placeholder="" value="<?php echo (isset($selected_order)) ? $selected_order['title'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-control" name="currency">
            <option value="0">-- Choose Currency --</option>
            <?php foreach ($currencies as $k => $v): ?>
                <option value="<?php echo $k; ?>" <?php echo (isset($selected_order['currency']) && $selected_order['currency'] == $k) ? 'selected' : ''; ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control" name="amt" placeholder="" value="<?php echo (isset($selected_order)) ? $remain_amt : ''; ?>">
    </div>
    <div class="form-group">
        <label>Status</label><br/>

        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_invoice_status($k); ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>"> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>