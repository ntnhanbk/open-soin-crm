<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>attachments/add_handler"
      enctype="multipart/form-data">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th width="20%">User</th>
            <th>Attachment</th>
            <th width="20%">Date</th>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="3">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php foreach ($items as $k => $v): $attachment = unserialize($v['attachment'])?>
            <tr>
                <td><?php echo CHtml::encode($v['author_name']) ?></td>
                <td><a href='<?php echo HelperUrl::upload_url() . $attachment['url'] ?>' target='_blank'><i class="fa fa-cloud-download"></i> <?php echo CHtml::encode($v['attachment_path']) ?></a></td>
                <td><?php echo CHtml::encode(Helper::date($v['date_added'])) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td><?php echo Yii::app()->user->fullname ?></td>
            <td colspan="2">
                <input type="hidden" name="object_id" value="<?php echo CHtml::encode($object_id) ?>">
                <input type="hidden" name="object_type" value="<?php echo CHtml::encode($object_type) ?>">
                <input type="file" name="file"/>
            </td>
        </tr>
        </tbody>
    </table>
</form>