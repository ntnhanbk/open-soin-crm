<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>requests/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Contact</label>
        <select class="form-control" name="contact_id">
            <option value="0">-- Choose Contact --</option>
            <?php foreach ($contacts as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Request Name</label>
        <input type="text" class="form-control" name="title" placeholder="">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label>Status</label><br/>

        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_request_status($k); ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>"> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>