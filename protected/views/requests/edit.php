<form method="post" action="<?php echo HelperUrl::baseUrl() . 'requests/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Contact</label>
        <select class="form-control" name="contact_id">
            <option value="0">-- Choose Contact --</option>
            <?php foreach ($contacts as $k => $v): ?>
                <option
                    value="<?php echo $v['id']; ?>" <?php echo ($item['contact_id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Request Name</label>
        <input type="text" class="form-control" name="title" placeholder=""
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" rows="5"><?php echo CHtml::encode($item['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label>Created Date</label>
        <input type="text" class="form-control datepicker" name="date_added" value="<?php echo date('m/d/Y H:i',$item['date_added']) ?>">
    </div>


    <div class="form-group">
        <label>Status</label><br/>
        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_request_status($k);?> <?php echo ($item['status']==$k)?'active':''; ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>" <?php echo ($item['status']==$k)?'checked':''; ?>> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>

</form>

<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>