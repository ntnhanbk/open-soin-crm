<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>milestones/deadline_handler/order_id/<?php echo $order_id; ?>"
      enctype="multipart/form-data">
    <div class="form-group">
        <label>Change Date</label>
        <input type="text" class="form-control" name="days">
    </div>
</form>

<table class="table table-bordered table-striped space10">
    <thead>
    <tr>
        <th class="text-center" width="30%">Deadline</th>
        <th>Order</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="3">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php foreach ($items as $k => $v): ?>
        <tr>
            <td class="text-center"><?php echo CHtml::encode(Helper::date($v['milestone'], 'M d, Y H:i a')) ?></td>
            <td><?php echo CHtml::encode($v['order_title']) ?></td>
            <td>
                <a class="btn-object-modal" modal-size=""
                   modal-url="<?php echo HelperUrl::baseUrl() . 'milestones/view/id/' . $v['id'] ?>"
                   modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                    <?php echo Helper::display_milestone_status($v['status']); ?>
                    <?php echo CHtml::encode($v['title']) ?>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>