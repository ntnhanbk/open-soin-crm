<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>milestones/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Order</label>
        <select class="form-control" name="order_id">
            <option value="0">-- Choose Order --</option>
            <?php foreach ($orders as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>" <?php echo (isset($selected_order['id']) && $selected_order['id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Description</label>
        <input type="text" class="form-control" name="title" placeholder="" value="<?php echo (isset($selected_order)) ? $selected_order['title'] : ''; ?>">
    </div>

    <div class="form-group">
        <label>Milestone</label>
        <input type="text" class="form-control datepicker" name="milestone" value="<?php echo date('m/d/Y H:i',time()) ?>">
    </div>

    <div class="form-group">
        <label>Status</label><br/>

        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_milestone_status($k); ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>"> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>