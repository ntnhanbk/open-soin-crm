<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2">
            <?php echo Helper::display_milestone_status($item['status']);?>
            <?php echo CHtml::encode($item['title']) ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="20%"><strong>Order Name</strong></td>
        <td width="80%"><?php echo CHtml::encode($item['order_title']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>