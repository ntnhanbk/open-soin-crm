<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>comments/add_handler"
      enctype="multipart/form-data">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th width="10%" class="text-center">User</th>
            <th width="10%" class="text-center">Date</th>
            <th class="text-center">Comment</th>

            <?php if ($object_type == 'deliverable'): ?>
                <th class="text-center">Status</th>
                <th width="17%"></th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="5">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php foreach ($items as $k => $v): ?>
            <tr>
                <td class="text-center"><?php echo CHtml::encode($v['author_name']) ?></td>
                <td class="text-center"><?php echo CHtml::encode(Helper::date($v['date_added'])) ?></td>
                <td><?php echo CHtml::encode($v['comment']) ?></td>

                <?php if ($object_type == 'deliverable'): ?>
                    <td class="text-center">
                        <?php echo Helper::display_comment_status($v['status']); ?>
                    </td>
                    <td class="text-center">
                        <div class="btn-group" data-toggle="buttons">
                            <?php foreach ($statuses as $k => $kv): ?>
                                <label
                                    class="btn btn-sm btn-<?php echo Helper::label_comment_status($k); ?> <?php echo ($v['status'] == $k) ? 'active' : ''; ?>">
                                    <input type="radio" name="statuses[<?php echo $v['id']; ?>]"
                                           value="<?php echo $k; ?>" <?php echo ($v['status'] == $k) ? 'checked' : ''; ?>> <?php echo $kv; ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td class="text-center"><?php echo Yii::app()->user->fullname ?></td>
            <td class="text-center"><?php echo CHtml::encode(Helper::date(time())) ?></td>
            <td>
                <input type="hidden" name="object_id" value="<?php echo CHtml::encode($object_id) ?>">
                <input type="hidden" name="object_type" value="<?php echo CHtml::encode($object_type) ?>">
                <input type="text" class="form-control" name="comment" placeholder="Enter comment">
            </td>

            <?php if ($object_type == 'deliverable'): ?>
                <td colspan="2"></td>
            <?php endif; ?>
        </tr>
        </tbody>
    </table>
</form>