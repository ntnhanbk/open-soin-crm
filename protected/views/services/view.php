<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['title']) ?></th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td width="40%"><strong>Created Date</strong></td>
        <td width="60%"><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>