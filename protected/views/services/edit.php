<form method="post" action="<?php echo HelperUrl::baseUrl() . 'services/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Service Name</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Service Name"
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
</form>