<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Service Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Services</li>
        </ol>
    </div>
</div>



<div class="container-fluid">
        <div class="text-right space10">
            <a href="" class="btn btn-primary btn-object-modal" modal-size="" modal-url="<?php echo HelperUrl::baseUrl(); ?>services/add" modal-title="<i class='fa fa-plus'></i> Add New Service"><i class="fa fa-plus"></i> Add New Service</a>
        </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Service
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th><a href="<?php echo HelperUrl::baseUrl() . "services/index/order_by/title/order_asc/$next_order_asc"; ?>">Service Name</a></th>
                    <th class="text-center" width="20%">
                        <a href="<?php echo HelperUrl::baseUrl() . "services/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created Date</a>
                    </th>
                    <th class="text-center" width="15%" colspan="5">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="7">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td>

                            <a class="btn-object-modal" modal-size="modal-md"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'services/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>

                        </td>
                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'services/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'services/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>


    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>
