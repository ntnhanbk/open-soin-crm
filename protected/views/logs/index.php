<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Log Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Logs</li>
        </ol>
    </div>
</div>


<div class="container-fluid">
    <div class="text-right space10">
        <a href="" class="btn btn-primary btn-object-modal" modal-size=""
           modal-url="<?php echo HelperUrl::baseUrl(); ?>logs/add" modal-title="<i class='fa fa-plus'></i> Add New Log"><i
                class="fa fa-plus"></i> Add New Log</a>
    </div>


    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Logs
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th class="text-center"><a
                            href="<?php echo HelperUrl::baseUrl() . "logs/index/order_by/title/order_asc/$next_order_asc"; ?>">User</a>
                    </th>
                    <th class="text-center"><a
                            href="<?php echo HelperUrl::baseUrl() . "logs/index/order_by/user_ip/order_asc/$next_order_asc"; ?>">IP</a>
                    </th>
                    <th class="text-center"><a
                            href="<?php echo HelperUrl::baseUrl() . "logs/index/order_by/access_controller/order_asc/$next_order_asc"; ?>">Access</a>
                    </th>
                    <th class="text-center" width="20%">
                        <a href="<?php echo HelperUrl::baseUrl() . "logs/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created
                            Date</a>
                    </th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="7">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td class="text-center">

                            <a class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'logs/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>

                        </td>
                        <td class="text-center">
                            <?php echo CHtml::encode($v['user_ip']) ?>
                        </td>
                        <td class="text-center">
                            <?php echo CHtml::encode($v['access_controller'] . ' / ' . $v['access_method']) ?>
                        </td>
                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>

        </div>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>
