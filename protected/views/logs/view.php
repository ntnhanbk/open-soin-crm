<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['title']) ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="30%"><strong>IP</strong></td>
        <td width="70%"><?php echo CHtml::encode($item['user_ip']) ?></td>
    </tr>
    <tr>
        <td><strong>Access</strong></td>
        <td><?php echo CHtml::encode($item['access_controller'] . ' / ' . $item['access_method']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Description</strong></td>
        <td>
            <pre>
                                <?php print_r(unserialize($item['description'])); ?>
                            </pre>
        </td>
    </tr>
    </tbody>
</table>