<?php if (!isset(Yii::app()->user->role)) return $this->redirect(HelperUrl::baseUrl() . 'site/sign_out'); ?>

<?php $this->renderPartial('application.views._shared.header') ?>
<?php echo $content ?>
<?php $this->renderPartial('application.views._shared.footer') ?>