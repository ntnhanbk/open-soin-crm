<?php $this->renderPartial('application.views._shared.header') ?>
    <div class="container">
        <?php echo $content ?>
    </div>
<?php $this->renderPartial('application.views._shared.footer') ?>