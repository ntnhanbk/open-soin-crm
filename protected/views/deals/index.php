<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Deal Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Deals</li>
        </ol>
    </div>
</div>

<div class="container-fluid">

    <div class="space10">
        <div class="clearfix">
            <div class="pull-left">
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>deals/index/status/0" class="btn btn-info">New Deals</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>deals/index/status/1" class="btn btn-success">Converted Deals</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>deals/index/status/-1" class="btn btn-default">Invalid Deals</a>
                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="" class="btn btn-primary btn-object-modal" modal-size="" modal-url="<?php echo HelperUrl::baseUrl(); ?>deals/add" modal-title="<i class='fa fa-plus'></i> Add New Deal"><i class="fa fa-plus"></i> Add New Deal</a>
                </div>
            </div>
        </div>
    </div>

    <div class="space10">&nbsp;</div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <ul class="notes">
                    <?php foreach ($items as $k => $v): ?>
                        <li>
                            <div class="btn-object-modal" modal-size="modal-md"
                                 modal-url="<?php echo HelperUrl::baseUrl() . 'deals/view/id/' . $v['id'] ?>"
                                 modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <!--<small><?php /*echo date('h:i:s m-d-Y', $v['date_added']) */?></small>-->
                                <h4>
                                    <?php echo CHtml::encode($v['title']) ?>
                                </h4>
                                <h1 class="text-center">
                                    $<?php echo number_format($v['amt']) ?>
                                </h1>

                                <p class="text-center space30">
                                    <?php echo Helper::display_deal_status($v['status']) ?>
                                </p>

                                <a class="btn-object-modal" modal-size="modal-sm"
                                   modal-url="<?php echo HelperUrl::baseUrl() . 'deals/delete/id/' . $v['id'] ?>"
                                   modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <?php /* ?>
    <table class="table table-bordered table-striped space10">
        <thead>
        <tr>
            <th><a href="<?php echo HelperUrl::baseUrl() . "deals/index/order_by/title/order_asc/$next_order_asc"; ?>">Deal Name</a></th>
            <th><a href="<?php echo HelperUrl::baseUrl() . "quotes/index/order_by/quote_title/order_asc/$next_order_asc"; ?>">Quote Name</a></th>
            <th class="text-right"><a href="<?php echo HelperUrl::baseUrl() . "quotes/index/order_by/amt/order_asc/$next_order_asc"; ?>">Amount</a></th>
            <th class="text-center" width="20%">
                <a href="<?php echo HelperUrl::baseUrl() . "deals/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created Date</a>
            </th>
            <th class="text-center" width="15%" colspan="5">&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="9">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php foreach ($items as $k => $v): ?>
            <tr>
                <td>

                    <a class="btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'deals/view/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <?php echo Helper::display_deal_status($v['status']);?>
                        <?php echo CHtml::encode($v['title']) ?>
                    </a>

                </td>
                <td><?php echo CHtml::encode($v['quote_title']) ?></td>
                <td class="text-right"><?php echo CHtml::encode($v['currency'].' '.number_format($v['amt'],2)) ?></td>
                <td class="text-center">
                    <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-primary btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'deals/view/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-info"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-success btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'comments/index/type/deal/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-comment'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-comment"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-info btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'attachments/index/type/deal/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-file'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-file"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'deals/edit/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                       modal-url="<?php echo HelperUrl::baseUrl() . 'deals/delete/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <?php */ ?>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>
</div>