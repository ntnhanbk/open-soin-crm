<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>deals/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Quote</label>
        <select class="form-control" name="quote_id">
            <option value="0">-- Choose Quote --</option>
            <?php foreach ($quotes as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>" <?php echo (isset($selected_quote['id']) && $selected_quote['id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Deal Name</label>
        <input type="text" class="form-control" name="title" placeholder="" value="<?php echo (isset($selected_quote)) ? $selected_quote['title'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-control" name="currency">
            <option value="0">-- Choose Currency --</option>
            <?php foreach ($currencies as $k => $v): ?>
                <option value="<?php echo $k; ?>" <?php echo (isset($selected_quote['currency']) && $selected_quote['currency'] == $k) ? 'selected' : ''; ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control" name="amt" placeholder="" value="<?php echo (isset($selected_quote)) ? $selected_quote['amt'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Status</label><br/>

        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_deal_status($k); ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>"> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>