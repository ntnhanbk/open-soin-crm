
<div class="row space20">
    <div class="col-sm-6">
        <?php if (!isset($search) || $search == true): ?>
            <form class="form-inline" method="get" action="">
                <div class="form-group">
                    <input type="text" class="form-control search-query" name="s" placeholder="Keyword" value="<?php echo isset($_GET['s']) ? trim($_GET['s']) : ""; ?>"/>
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
        <?php endif; ?>
    </div>
    <div class="col-sm-6 text-right">
        <ol class="pagination">
            <?php echo $paging; ?>
        </ol>
    </div>
</div>
