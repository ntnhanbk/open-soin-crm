<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo HelperUrl::baseUrl(); ?>assets/css/img/favicon.ico">

    <title><?php echo CHtml::encode($this->pageTitle) ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo HelperUrl::baseUrl() . 'assets/insipinia/' ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl() . 'assets/insipinia/' ?>css/style.css" rel="stylesheet">

    <script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/jquery.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="pace-done mini-navbar">

<div id="wrapper">


    <?php $this->renderPartial('application.views._shared.sidebar') ?>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>

                    <form role="search" class="navbar-form-custom" method="get"
                          action="<?php echo HelperUrl::baseUrl() . 'invoices' ?>">
                        <div class="form-group">
                            <input type="text" placeholder="Search for invoices ..." class="form-control" name="s"
                                   id="top-search" value="<?php echo CHtml::encode(Helper::request('s')) ?>">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            Welcome <?php echo Yii::app()->user->username ?>
                            <span class="fa fa-caret-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#" class="btn-object-modal"
                                   modal-size=""
                                   modal-url="<?php echo HelperUrl::baseUrl(); ?>site/profile"
                                   modal-title="<?php echo Yii::app()->user->username; ?>">
                                    <i class="fa fa-cogs"></i>
                                    Account Settings
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn-object-modal" modal-size=""
                                   modal-url="<?php echo HelperUrl::baseUrl(); ?>site/change"
                                   modal-title="<?php echo Yii::app()->user->username; ?>">
                                    <i class="fa fa-fire"></i>
                                    Change Password
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo HelperUrl::baseUrl(); ?>site/sign_out"> <i
                                        class="fa fa-sign-out"></i>
                                    Sign Out</a></li>
                        </ul>
                    </li>


                    <li>
                        <a href="<?php echo HelperUrl::baseUrl(); ?>site/sign_out">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>