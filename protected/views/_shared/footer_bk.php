<div class="modal fade" id="ObjectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-status">
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/jquery.form.min.js"></script>

<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo HelperUrl::baseUrl(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/highcharts/js/modules/funnel.js"></script>
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/highcharts/js/modules/exporting.js"></script>

<script
    src="<?php echo HelperUrl::baseUrl(); ?>assets/js/app.js?v=<?php echo (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '1'; ?>
    >
    "></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=" <?php echo HelperUrl::baseUrl(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>