<?php $UserModel = new UserModel();
$user = $UserModel->get(Yii::app()->user->id);
?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>

                        <?php if (Helper::is_serialized($user['thumbnail'])): ?>
                            <img alt="image" class="img-circle" style="max-width: 70px"
                                 src="<?php echo Helper::get_thumbnail($user['thumbnail'], 'thumbnail') ?>"/>
                        <?php else: ?>
                            <img alt="image" class="img-circle"
                                 src="<?php echo HelperUrl::baseUrl() . 'assets/insipinia/' ?>img/profile_small.jpg"/>
                        <?php endif; ?>
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">
                                        <?php echo CHtml::encode(Yii::app()->user->fullname) ?>
                                    </strong>
                             </span> <span class="text-muted text-xs block">
                                    <?php echo Helper::display_user_role(Yii::app()->user->role) ?>
                                    <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a href="#" class="btn-object-modal"
                               modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl(); ?>site/profile"
                               modal-title="<?php echo Yii::app()->user->username; ?>">
                                Account Settings
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl(); ?>site/change"
                               modal-title="<?php echo Yii::app()->user->username; ?>">
                                Change Password
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>site/sign_out">Sign Out</a></li>
                    </ul>
                </div>
            </li>

            <li class="<?php if (Yii::app()->params['page_group'] == null) echo 'active'; ?>">
                <a href="<?php echo HelperUrl::baseUrl() ?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
            </li>

            <li class="<?php if (Yii::app()->params['page_group'] == 'cost-structures') echo 'active'; ?>">
                <a href="<?php echo HelperUrl::baseUrl() . 'site/cost' ?>"><i class="fa fa-pie-chart"></i> <span class="nav-label">
                        Cost Structures
                    </span></a>
            </li>


            <li class="<?php echo Yii::app()->params['page_group'] == 'pre_sales' ? 'active' : '' ?>">
                <a href="#">
                    <i class="fa fa-file-text-o"></i>
                    <span class="nav-label">Pre Sales</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo Yii::app()->params['page'] == 'campaigns' ? 'active' : '' ?>">
                        <a href="<?php echo HelperUrl::baseUrl(); ?>campaigns">Campaigns</a>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'leads' ? 'active' : '' ?>">
                        <a href="<?php echo HelperUrl::baseUrl(); ?>leads">Leads</a>
                    </li>
                    <li class="<?php echo Yii::app()->params['page'] == 'contacts' ? 'active' : '' ?>">
                        <a href="<?php echo HelperUrl::baseUrl(); ?>contacts">Contacts</a>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'tasks' ? 'active' : '' ?>">
                        <a href="<?php echo HelperUrl::baseUrl(); ?>tasks">Tasks</a>
                    </li>
                </ul>
            </li>


            <li class="<?php echo Yii::app()->params['page_group'] == 'sales' ? 'active' : '' ?>">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span class="nav-label">Sales</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo Yii::app()->params['page'] == 'requests' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>requests">Requests</a></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'quotes' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>quotes">Quotes</a></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'deals' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>deals">Deals</a></li>
                </ul>
            </li>


            <li class="<?php echo Yii::app()->params['page_group'] == 'post_sales' ? 'active' : '' ?>">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span class="nav-label">Post Sales</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo Yii::app()->params['page'] == 'orders' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>orders">Orders</a></li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'milestones' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>milestones">Milestones</a></li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'invoices' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>invoices">Invoices</a></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'expenses' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>expenses">Expenses</a></li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'receivers' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>receivers">Receivers</a></li>
                </ul>
            </li>


            <li class="<?php echo Yii::app()->params['page_group'] == 'data' ? 'active' : '' ?>">
                <a href="#">
                    <i class="fa fa-database"></i>
                    <span class="nav-label">Data</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo Yii::app()->params['page'] == 'categories' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>categories">Categories</a></li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'products' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>products">Products</a></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'services' ? 'active' : '' ?>"><a
                            href="<?php echo HelperUrl::baseUrl(); ?>services">Services</a></li>
                </ul>
            </li>


            <li class="<?php echo Yii::app()->params['page_group'] == 'system' ? 'active' : '' ?>">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span class="nav-label">System</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo Yii::app()->params['page'] == 'users' ? 'active' : '' ?>"><a href="<?php echo HelperUrl::baseUrl(); ?>users">Users</a></li>
                    <li class="divider"></li>
                    <li class="<?php echo Yii::app()->params['page'] == 'logs' ? 'active' : '' ?>"><a href="<?php echo HelperUrl::baseUrl(); ?>logs">System Logs</a></li>
                </ul>
            </li>


        </ul>

    </div>
</nav>