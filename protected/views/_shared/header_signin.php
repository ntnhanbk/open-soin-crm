<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo HelperUrl::baseUrl(); ?>assets/css/img/favicon.ico">

    <title><?php echo CHtml::encode($this->pageTitle) ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo HelperUrl::baseUrl() . 'assets/insipinia/' ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl() . 'assets/insipinia/' ?>css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name"></h1>
        </div>