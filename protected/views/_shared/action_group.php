<?php foreach ($actions as $action): ?>

    <?php if (isset($action['type']) && $action['type'] == 'close-modal'): ?>

        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
    <?php elseif (isset($action['type']) && $action['type'] == 'submit-form'): ?>
        <button type="button"
                class="<?php echo (isset($action['class']) && $action['class'] != '')
                    ? CHtml::encode($action['class']) : 'btn btn-primary btn-save' ?>">
            <?php echo (isset($action['label']) && $action['label'] != '') ? $action['label'] : 'Save Changes' ?>
        </button>
    <?php
    else: ?>

        <a href="<?php echo CHtml::encode($action['url']) ?>"
           class="<?php echo CHtml::encode($action['class']) ?>"
           id="<?php echo (isset($action['id']) && $action['id'] != '') ?CHtml::encode($action['id']):'' ?>" <?php echo (isset($action['attributes']) && $action['attributes'] != '') ? $action['attributes'] : ''; ?>>
            <?php echo $action['label'] ?>

        </a>
    <?php endif ?>

<?php endforeach; ?>