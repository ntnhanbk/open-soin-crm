<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo HelperUrl::baseUrl(); ?>assets/css/img/favicon.ico">

    <title><?php echo CHtml::encode($this->pageTitle) ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/styles.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body style="padding: 10px 0 20px;">