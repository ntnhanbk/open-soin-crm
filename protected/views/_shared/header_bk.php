<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo HelperUrl::baseUrl(); ?>assets/css/img/favicon.ico">

    <title><?php echo CHtml::encode($this->pageTitle) ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo HelperUrl::baseUrl(); ?>assets/css/styles.css" rel="stylesheet">

    <script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/jquery.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo HelperUrl::baseUrl();?>assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo HelperUrl::baseUrl(); ?>">Soin CRM</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">Pre Sales <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>campaigns">Campaigns</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>leads">Leads</a></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>contacts">Contacts</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>tasks">Tasks</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">Sales <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>requests">Requests</a></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>quotes">Quotes</a></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>deals">Deals</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">Post Sales <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>orders">Orders</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>milestones">Milestones</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>invoices">Invoices</a></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>expenses">Expenses</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>receivers">Receivers</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">Data <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>categories">Categories</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>products">Products</a></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>services">Services</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">System <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>users">Users</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>logs">System Logs</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown">Welcome <?php echo Yii::app()->user->username ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="#" class="btn-object-modal"
                               modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl(); ?>site/profile"
                               modal-title="<?php echo Yii::app()->user->username; ?>">
                                Account Settings
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl(); ?>site/change"
                               modal-title="<?php echo Yii::app()->user->username; ?>">
                                Change Password
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo HelperUrl::baseUrl(); ?>site/sign_out">Sign Out</a></li>
                </li>
            </ul>
            </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>