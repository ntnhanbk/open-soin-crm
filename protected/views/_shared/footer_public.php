<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo HelperUrl::baseUrl(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>