<form method="post" action="<?php echo HelperUrl::baseUrl() . 'campaigns/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Campaign Name</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Campaign Name"
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
</form>