<form method="post" action="<?php echo HelperUrl::baseUrl() . 'deliverables/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control" name="title" value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" rows="5"><?php echo CHtml::encode($item['description']) ?></textarea>
    </div>
    <div class="form-group">
        <label>Sort Order</label>
        <input type="number" class="form-control" name="sort_order" value="<?php echo CHtml::encode($item['sort_order']) ?>">
    </div>
</form>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>