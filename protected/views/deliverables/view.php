<table class="table table-striped">
    <thead>
    <tr>
        <th colspan="2">
            <?php echo CHtml::encode($item['title']) ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="25%"><strong>Request</strong></td>
        <td width="75%"><?php echo CHtml::encode($item['request_title']) ?></td>
    </tr>
    <tr>
        <td><strong>Description</strong></td>
        <td><?php echo nl2br($item['description']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>
    </tbody>
</table>