<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>deliverables/add_handler" enctype="multipart/form-data">
    <input type="hidden" name="request_id" value="<?php echo $request_id;?>"/>
    <input type="hidden" name="parent_id" value="<?php echo $parent_id;?>"/>

    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control" name="title" value="">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label>Sort Order</label>
        <input type="number" class="form-control" name="sort_order" value="">
    </div>

</form>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>