<form method="post"
      action="<?php echo HelperUrl::baseUrl() . 'deliverables/in_request_handler/id/' . $request['id'] . '/field/' . $field ?>"
      enctype="multipart/form-data">
    <table class="table table-striped space10">
        <thead>
        <tr>
            <th>Deliverable</th>
            <th class="text-center" width="20%"><input type="checkbox" id="selecctall" name="selecctall" value="0"/>
            </th>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="2">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php
        foreach ($items as $k => $v) {
            $this->renderPartial('application.views.deliverables._row_in_request', array('item' => $v, 'field' => $field, 'number' => $k, 'request' => $request, 'level' => 1));

            if (isset($v['subdeliverable-list'])) {
                foreach ($v['subdeliverable-list'] as $sk => $sv) {
                    $this->renderPartial('application.views.deliverables._row_in_request', array('item' => $sv, 'field' => $field, 'number' => $sk, 'request' => $request, 'level' => 2));
                    if (isset($sv['subdeliverable-list']))
                        foreach ($sv['subdeliverable-list'] as $svk => $svv)
                            $this->renderPartial('application.views.deliverables._row_in_request', array('item' => $svv, 'field' => $field, 'number' => $svk, 'request' => $request, 'level' => 3));
                }
            }

        }
        ?>

        </tbody>
    </table>
</form>

<script>
    $(document).ready(function () {
        $('#selecctall').click(function (event) {  //on click
            if (this.checked) { // check select status
                $('.selecctall').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            } else {
                $('.selecctall').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });

    });
</script>