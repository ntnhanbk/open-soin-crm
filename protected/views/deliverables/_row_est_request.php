<?php
$level_repeat = 1;
if ($level == 1)
    $number = ($number + 1);

if ($level == 2) {
    $number = chr($number + 65);
    $level_repeat = 2;
}

if ($level == 3) {
    $number = strtolower(chr($number + 65));
    $level_repeat = 5;
}


?>

<tr>
    <td>
        <span class="<?php echo ($level == 1) ? 'text-bold text-info' : ''; ?>">
            <?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
            <?php echo $number . '. ' . CHtml::encode($item['title']) ?>
            <?php
            if (trim($item['description']) != ''):
                ?>
                <br/><?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
                <em style="font-size: 11px;"><?php echo CHtml::encode($item['description']) ?></em>
            <?php
            endif;
            ?>
        </span>
    </td>

    <td class="text-center"><input type="<?php echo ($item['subdeliverable-list'])?'hidden':'text';?>" class="text-center" name="est_request[<?php echo $item['id']; ?>]"
                                   value="<?php echo ($item['subdeliverable-list'])?0:(($copy_from!='')?$item[$copy_from]:$item[$field]); ?>"/></td>
</tr>