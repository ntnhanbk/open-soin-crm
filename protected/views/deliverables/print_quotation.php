<style>
    table th {
        text-align: left;
        text-transform: uppercase;
        font-size: 13px !important;
    }

    .text-center {
        text-align: center !important;
    }

    .text-right {
        text-align: right !important;
    }

    table td {
        font-size: 13px;
    }

    .text-info {
        font-weight: bold;
        color: #3071A9;
    }

    #signature_table td {
        font-weight: bold;
    }
</style>

<table border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td width="25%" class="text-center">
            <?php if (Helper::is_serialized($user['company_logo'])): ?>
                <img src="<?php echo Helper::get_thumbnail($user['company_logo'], 'full') ?>" width="120px"/>
            <?php endif; ?>
        </td>
        <td class="text-center" width="">
            <h1>QUOTATION</h1>
        </td>
        <td width="25%">
            <p>Doc. NO.:</p>

            <p>Rev. No.:</p>

            <p>Rev. Date:</p>
        </td>
    </tr>
</table>

<br/>

<?php /* ?>
<table border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <th colspan="2" class="text-center">PACKED COLUMN</th>
    </tr>
    <tr>
        <td width="25%">Unit/Item No.</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Service</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Licensor</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Reference Doc. (QA/QC)</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Reference Drawings</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
</table>
<br />
 <?php */ ?>


<table class="table table-striped space10" border="1" width="100%" cellspacing="0" cellpadding="5">
    <thead>
    <tr>
        <th>Features</th>
        <th class="text-center" width="15%">Hours</th>
        <?php if ($rate != 0 && $currency != ""): ?>
            <th class="text-right" width="15%">Sub Total</th>
        <?php endif; ?>
        <th class="text-center" width="15%">Remarks</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="4">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php
    foreach ($items as $k => $v) {
        $this->renderPartial('application.views.deliverables._row-quote-pdf', array('item' => $v, 'number' => $k, 'request' => $request, 'field' => $field, 'currency' => $currency, 'rate' => $rate, 'level' => 1));

        if (isset($v['subdeliverable-list'])) {
            foreach ($v['subdeliverable-list'] as $sk => $sv) {
                $this->renderPartial('application.views.deliverables._row-quote-pdf', array('item' => $sv, 'number' => $sk, 'request' => $request, 'field' => $field, 'currency' => $currency, 'rate' => $rate, 'level' => 2));
                if (isset($sv['subdeliverable-list']))
                    foreach ($sv['subdeliverable-list'] as $svk => $svv)
                        $this->renderPartial('application.views.deliverables._row-quote-pdf', array('item' => $svv, 'number' => $svk, 'request' => $request, 'field' => $field, 'currency' => $currency, 'rate' => $rate, 'level' => 3));
            }
        }

    }
    ?>

    </tbody>
    <tfoot>
    <tr>

        <th class="text-center">
            <h3>Total</h3>
        </th>
        <th class="text-center"><?php echo $total; ?></th>
        <?php if ($rate != 0 && $currency != ""): ?>
            <td class="text-right"><?php echo $currency.' '.number_format($rate * $total, 2); ?></td>
        <?php endif; ?>
        <th>&nbsp;</th>
    </tr>
    </tfoot>
</table>