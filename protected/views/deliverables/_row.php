<?php
$level_repeat = 1;
if ($level == 1)
    $number = ($number + 1);

if ($level == 2) {
    $number = chr($number + 65);
    $level_repeat = 2;
}

if ($level == 3) {
    $number = strtolower(chr($number + 65));
    $level_repeat = 5;
}


?>

<tr>
    <td class="text-center">
        <?php if ($level < 3): ?>
            <a href="" class="btn btn-primary btn-sm btn-object-modal" modal-size="modal-md"
               modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/add/request_id/<?php echo $request['id'] . '/parent_id/' . $item['id']; ?>"
               modal-title="<i class='fa fa-plus'></i> Add New Deliverable"><i class="fa fa-plus"></i></a>
        <?php else: ?>
            -
        <?php endif; ?>
    </td>

    <td>
        <span class="<?php echo ($level == 1) ? 'text-bold text-info' : ''; ?>">
            <?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
            <?php echo $number . '. ' . CHtml::encode($item['title']) ?>
            <?php
            if (trim($item['description']) != ''):
                ?>
                <br/><?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
                <em style="font-size: 11px;"><?php echo CHtml::encode($item['description']) ?></em>
            <?php
            endif;
            ?>
        </span>
    </td>

    <td class="text-center"><i class="<?php echo ($item['in_request']) ? 'fa fa-check' : ''; ?>"></i></td>
    <td class="text-center"><i class="<?php echo ($item['in_quote']) ? 'fa fa-check' : ''; ?>"></i></td>
    <td class="text-center"><i class="<?php echo ($item['in_deal']) ? 'fa fa-check' : ''; ?>"></i></td>
    <td class="text-center"><i class="<?php echo ($item['in_order']) ? 'fa fa-check' : ''; ?>"></i></td>

    <td class="text-center"><?php echo ($item['est_request'] != 0) ? $item['est_request'] : ''; ?></td>
    <td class="text-center"><?php echo ($item['est_quote'] != 0) ? $item['est_quote'] : ''; ?></td>
    <td class="text-center"><?php echo ($item['est_deal'] != 0) ? $item['est_deal'] : ''; ?></td>
    <td class="text-center"><?php echo ($item['est_order'] != 0) ? $item['est_order'] : ''; ?></td>

    <td width="5%" class="text-center">
        <a class="btn btn-primary btn-sm btn-object-modal" modal-size=""
           modal-url="<?php echo HelperUrl::baseUrl() . 'deliverables/view/id/' . $item['id'] ?>"
           modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($item['title']) ?>">
            <i class="fa fa-info"></i>
        </a>
    </td>

    <td width="5%" class="text-center">
        <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
           modal-url="<?php echo HelperUrl::baseUrl() . 'deliverables/edit/id/' . $item['id'] ?>"
           modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($item['title']) ?>">
            <i class="fa fa-edit"></i>
        </a>
    </td>

    <td width="5%" class="text-center">
        <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
           modal-url="<?php echo HelperUrl::baseUrl() . 'deliverables/delete/id/' . $item['id'] ?>"
           modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($item['title']) ?>">
            <i class="fa fa-trash-o"></i>
        </a>
    </td>
</tr>