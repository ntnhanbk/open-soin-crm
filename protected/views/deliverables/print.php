<style>
    table th {
        text-align: left;
        text-transform: uppercase;
        font-size: 13px !important;
    }

    .text-center {
        text-align: center !important;
    }

    table td {
        font-size: 13px;
    }

    .text-info {
        font-weight: bold;
        color: #3071A9;
    }

    #signature_table td {
        font-weight: bold;
    }
</style>

<table border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td width="25%" class="text-center">
            <?php if (Helper::is_serialized($user['company_logo'])): ?>
                <img src="<?php echo Helper::get_thumbnail($user['company_logo'], 'full') ?>" width="120px"/>
            <?php endif; ?>
        </td>
        <td class="text-center" width="">
            <h1>
                COMMISSIONING CHECKLIST
            </h1>
        </td>
        <td width="25%">
            <p><strong>Commissioning checklist</strong></p>

            <p>Doc. NO.:</p>

            <p>
                Rev. No.:
            </p>

            <p>
                Rev. Date:
            </p>
        </td>
    </tr>
</table>

<br/>

<?php /* ?>
<table border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <th colspan="2" class="text-center">PACKED COLUMN</th>
    </tr>
    <tr>
        <td width="25%">Unit/Item No.</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Service</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Licensor</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Reference Doc. (QA/QC)</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
    <tr>
        <td>Reference Drawings</td>
        <td class="text-center" width="">&nbsp;</td>
    </tr>
</table>
<br />
 <?php */ ?>


<table class="table table-striped space10" border="1" width="100%" cellspacing="0" cellpadding="5">
    <thead>
    <tr>
        <th>Features</th>
        <th class="text-center hidden-xs" width="10%">DONE</th>
        <th class="text-center" width="15%">By</th>
        <th class="text-center" width="15%">Date</th>
        <th class="text-center" width="15%">Remarks</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="5">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php
    foreach ($items as $k => $v) {
        $this->renderPartial('application.views.deliverables._row-pdf', array('item' => $v, 'number' => $k, 'request' => $request, 'level' => 1));

        if (isset($v['subdeliverable-list'])) {
            foreach ($v['subdeliverable-list'] as $sk => $sv) {
                $this->renderPartial('application.views.deliverables._row-pdf', array('item' => $sv, 'number' => $sk, 'request' => $request, 'level' => 2));
                if (isset($sv['subdeliverable-list']))
                    foreach ($sv['subdeliverable-list'] as $svk => $svv)
                        $this->renderPartial('application.views.deliverables._row-pdf', array('item' => $svv, 'number' => $svk, 'request' => $request, 'level' => 3));
            }
        }

    }
    ?>

    </tbody>
</table>

<br/>

<table id="signature_table" border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td>ATTESTED BY</td>
        <td class="text-center">NAME</td>
        <td class="text-center">DATE</td>
        <td class="text-center">SIGNATURE</td>
    </tr>
    <tr>
        <td><?php echo ($contact['company'] != '') ? $contact['company'] : '&nbsp;' ?></td>
        <td class="text-center">&nbsp;</td>
        <td class="text-center">&nbsp;</td>
        <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
        <td><?php echo ($user['company_name'] != '') ? $user['company_name'] : '&nbsp;' ?></td>
        <td class="text-center">&nbsp;</td>
        <td class="text-center">&nbsp;</td>
        <td class="text-center">&nbsp;</td>
    </tr>
</table>