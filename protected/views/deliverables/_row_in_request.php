<?php
$level_repeat = 1;
if ($level == 1)
    $number = ($number + 1);

if ($level == 2) {
    $number = chr($number + 65);
    $level_repeat = 2;
}

if ($level == 3) {
    $number = strtolower(chr($number + 65));
    $level_repeat = 5;
}


?>

<tr>
    <td>
        <span class="<?php echo ($level == 1) ? 'text-bold text-info' : ''; ?>">
            <?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
            <?php echo $number . '. ' . CHtml::encode($item['title']) ?>
        </span>
    </td>

    <td class="text-center"><input type="checkbox" class="selecctall" name="in_request[]" value="<?php echo $item['id'];?>" <?php echo ($item[$field]) ? 'checked="checked"' : ''; ?>/></td>
</tr>