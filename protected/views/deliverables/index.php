<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Deliverable Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>requests">Requests</a></li>
            <li><?php echo $request['title']; ?></li>
            <li class="active">Deliverable List</li>
        </ol>
    </div>
</div>


<div class="container-fluid">
    <div class="text-right space10">
        <a href="" class="btn btn-default btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/in_request/request_id/<?php echo $request['id']; ?>/field/in_request"
           modal-title="<i class='fa fa-info'></i> In Request">In Request</a>
        <a href="" class="btn btn-default btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/in_request/request_id/<?php echo $request['id']; ?>/field/in_quote"
           modal-title="<i class='fa fa-info'></i> In Quote">In Quote</a>
        <a href="" class="btn btn-default btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/in_request/request_id/<?php echo $request['id']; ?>/field/in_deal"
           modal-title="<i class='fa fa-info'></i> In Deal">In Deal</a>
        <a href="" class="btn btn-default btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/in_request/request_id/<?php echo $request['id']; ?>/field/in_order"
           modal-title="<i class='fa fa-info'></i> In Order">In Order</a>

        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/est_request"
           modal-title="<i class='fa fa-info'></i> Estimation Request">Est. Request</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/est_quote"
           modal-title="<i class='fa fa-info'></i> Estimation Quote">Est. Quote</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/est_deal"
           modal-title="<i class='fa fa-info'></i> Estimation Deal">Est. Deal</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/est_order"
           modal-title="<i class='fa fa-info'></i> Estimation Order">Est. Order</a>
    </div>
    <div class="text-right space10">
        <a target="_blank" class="btn btn-primary"
           href="<?php echo HelperUrl::baseUrl() . 'deliverables/print_quotation/request_id/' . $request['id'].'/field/est_request' ?>">
            <i class="fa fa-print"></i>
            Print Request
        </a>
        <a target="_blank" class="btn btn-primary"
           href="<?php echo HelperUrl::baseUrl() . 'deliverables/print_quotation/request_id/' . $request['id'].'/field/est_quote' ?>">
            <i class="fa fa-print"></i>
            Print Quote
        </a>
        <a target="_blank" class="btn btn-primary"
           href="<?php echo HelperUrl::baseUrl() . 'deliverables/print_quotation/request_id/' . $request['id'].'/field/est_deal' ?>">
            <i class="fa fa-print"></i>
            Print Deal
        </a>
        <a target="_blank" class="btn btn-primary"
           href="<?php echo HelperUrl::baseUrl() . 'deliverables/print_quotation/request_id/' . $request['id'].'/field/est_order' ?>">
            <i class="fa fa-print"></i>
            Print Order
        </a>

        <a target="_blank" class="btn btn-success"
           href="<?php echo HelperUrl::baseUrl() . 'deliverables/print/request_id/' . $request['id'] ?>">
            <i class="fa fa-print"></i>
            Export to PDF
        </a>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Deliverable List
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th class="text-center" width="5%">
                        <a href="" class="btn btn-primary btn-object-modal btn-sm" modal-size="modal-md"
                           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/add/request_id/<?php echo $request['id']; ?>"
                           modal-title="<i class='fa fa-plus'></i> Add New Deliverable"><i class="fa fa-plus"></i></a>
                    </th>
                    <th>
                        <a href="<?php echo HelperUrl::baseUrl() . "deliverables/index/request_id/" . $request['id'] . "/order_by/title/order_asc/$next_order_asc"; ?>">Deliverable</a>
                    </th>
                    <th class="text-center" width="5%">Request</th>
                    <th class="text-center" width="5%">Quote</th>
                    <th class="text-center" width="5%">Deal</th>
                    <th class="text-center" width="5%">Order</th>
                    <th class="text-center" width="5%">Est. Request</th>
                    <th class="text-center" width="5%">Est. Quote</th>
                    <th class="text-center" width="5%">Est. Deal</th>
                    <th class="text-center" width="5%">Est. Order</th>
                    <th colspan="3">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="10">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php
                foreach ($items as $k => $v) {
                    $this->renderPartial('application.views.deliverables._row', array('item' => $v, 'number' => $k, 'request' => $request, 'level' => 1));

                    if (isset($v['subdeliverable-list'])) {
                        foreach ($v['subdeliverable-list'] as $sk => $sv) {
                            $this->renderPartial('application.views.deliverables._row', array('item' => $sv, 'number' => $sk, 'request' => $request, 'level' => 2));
                            if (isset($sv['subdeliverable-list']))
                                foreach ($sv['subdeliverable-list'] as $svk => $svv)
                                    $this->renderPartial('application.views.deliverables._row', array('item' => $svv, 'number' => $svk, 'request' => $request, 'level' => 3));
                        }
                    }

                }
                ?>

                </tbody>
                <tfoot>
                <tr>

                    <th colspan="6" class="text-center">
                        <h3>Total</h3>
                    </th>
                    <th class="text-center" width="5%"><?php echo $total_request;?></th>
                    <th class="text-center" width="5%"><?php echo $total_quote;?></th>
                    <th class="text-center" width="5%"><?php echo $total_deal;?></th>
                    <th class="text-center" width="5%"><?php echo $total_order;?></th>
                    <th colspan="3">&nbsp;</th>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>
