<form method="post"
      action="<?php echo HelperUrl::baseUrl() . 'deliverables/est_request_handler/id/' . $request['id'] . '/field/' . $field ?>"
      enctype="multipart/form-data">
    <div>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/<?php echo $field; ?>/copy_from/est_request"
           modal-title="<i class='fa fa-info'></i> Estimation Request">Copy Est. Request</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/<?php echo $field; ?>/copy_from/est_quote"
           modal-title="<i class='fa fa-info'></i> Estimation Quote">Copy Est. Quote</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/<?php echo $field; ?>/copy_from/est_deal"
           modal-title="<i class='fa fa-info'></i> Estimation Deal">Copy Est. Deal</a>
        <a href="" class="btn btn-info btn-object-modal" modal-size="modal-md"
           modal-url="<?php echo HelperUrl::baseUrl(); ?>deliverables/est_request/request_id/<?php echo $request['id']; ?>/field/<?php echo $field; ?>/copy_from/est_order"
           modal-title="<i class='fa fa-info'></i> Estimation Order">Copy Est. Order</a>
    </div>
    <table class="table table-striped space10">
        <thead>
        <tr>
            <th>Deliverable</th>
            <th class="text-center" width="20%">Estimation</th>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="2">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php
        foreach ($items as $k => $v) {
            $this->renderPartial('application.views.deliverables._row_est_request', array('item' => $v, 'field' => $field, 'copy_from' => $copy_from, 'number' => $k, 'request' => $request, 'level' => 1));
            if (isset($v['subdeliverable-list'])) {
                foreach ($v['subdeliverable-list'] as $sk => $sv) {
                    $this->renderPartial('application.views.deliverables._row_est_request', array('item' => $sv, 'field' => $field, 'copy_from' => $copy_from, 'number' => $sk, 'request' => $request, 'level' => 2));
                    if (isset($sv['subdeliverable-list']))
                        foreach ($sv['subdeliverable-list'] as $svk => $svv)
                            $this->renderPartial('application.views.deliverables._row_est_request', array('item' => $svv, 'field' => $field, 'copy_from' => $copy_from, 'number' => $svk, 'request' => $request, 'level' => 3));
                }
            }

        }
        ?>
        </tbody>
    </table>
</form>