<?php
$level_repeat = 1;
if ($level == 1)
    $number = ($number + 1);

if ($level == 2) {
    $number = chr($number + 65);
    $level_repeat = 2;
}

if ($level == 3) {
    $number = strtolower(chr($number + 65));
    $level_repeat = 5;
}


?>

<tr>
    <td>
        <span class="<?php echo ($level == 1) ? 'text-bold text-info' : ''; ?>">
            <?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
            <?php echo $number . '. ' . CHtml::encode($item['title']) ?>
            <?php
            if (trim($item['description']) != ''):
                ?>
                <br/><?php echo ($level > 1) ? str_repeat('&nbsp;', ($level_repeat * 3)) : ''; ?>
                <em style="font-size: 11px;"><?php echo CHtml::encode($item['description']) ?></em>
            <?php
            endif;
            ?>
        </span>
    </td>
    <td class="text-center"><?php echo ($item[$field] > 0) ? $item[$field] : ''; ?></td>
    <?php if ($rate != 0 && $currency != ""): ?>
        <td class="text-right"><?php echo ($item[$field] > 0) ? $currency . ' ' . number_format($rate * $item[$field], 2) : ''; ?></td>
    <?php endif; ?>
    <td class="text-center">&nbsp;</td>
</tr>