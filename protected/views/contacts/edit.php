<form method="post" action="<?php echo HelperUrl::baseUrl() . 'contacts/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Contact Name</label>
        <input type="text" class="form-control" name="title" placeholder=""
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>

    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone" placeholder=""
               value="<?php echo CHtml::encode($item['phone']) ?>">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" placeholder=""
               value="<?php echo CHtml::encode($item['email']) ?>">
    </div>

    <div class="form-group">
        <label>Skype</label>
        <input type="text" class="form-control" name="skype" placeholder=""
               value="<?php echo CHtml::encode($item['skype']) ?>">
    </div>

    <div class="form-group">
        <label>Viber</label>
        <input type="text" class="form-control" name="viber" placeholder=""
               value="<?php echo CHtml::encode($item['viber']) ?>">
    </div>

    <div class="form-group">
        <label>What's app</label>
        <input type="text" class="form-control" name="whatsapp" placeholder=""
               value="<?php echo CHtml::encode($item['whatsapp']) ?>">
    </div>

</form>