<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contact Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Contacts</li>
        </ol>
    </div>
</div>


<div class="container-fluid">

    <div class="text-right space10">
        <a href="" class="btn btn-primary btn-object-modal" modal-size=""
           modal-url="<?php echo HelperUrl::baseUrl(); ?>contacts/add"
           modal-title="<i class='fa fa-plus'></i> Add New Contact"><i class="fa fa-plus"></i> Add New Contact</a>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="animated fadeInRight">
        <div class="row">

            <?php $flag = 0; ?>
            <?php foreach ($items as $k => $v): $flag++; ?>
                <div class="col-lg-4">
                    <div class="contact-box btn-object-modal" modal-size="modal-md"
                         modal-url="<?php echo HelperUrl::baseUrl() . 'contacts/view/id/' . $v['id'] ?>"
                         modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="text-center">
                                    <img alt="image" class="img-circle m-t-xs img-responsive space10"
                                         src="<?php echo Helper::get_gravatar($v['email'], 255) //HelperApp::get_thumbnail($v['admin_thumbnail'], 'small');  ?>">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h4><strong><?php echo CHtml::encode($v['title']) ?></strong></h4>

                                <p>
                                    <strong>Phone</strong>: <?php echo CHtml::encode($v['phone']) ?> <br/>
                                    <strong>Email</strong>:
                                    <a href="mailto:<?php echo CHtml::encode($v['email']) ?>">
                                        <?php echo CHtml::encode($v['email']) ?>
                                    </a>
                                    <br/>
                                    <strong>Skype</strong>:
                                    <a href="skype:<?php echo CHtml::encode($v['skype']) ?>">
                                        <?php echo CHtml::encode($v['skype']) ?>
                                    </a>
                                    <br/>
                                    <strong>What's App</strong>: <?php echo CHtml::encode($v['whatsapp']) ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($flag == 3): $flag = 0; ?>
                    <div class="clearfix">&nbsp;</div>
                <?php endif; ?>

            <?php endforeach; ?>

        </div>
    </div>

    <?php /* ?>
    <table class="table table-bordered table-striped space10">
        <thead>
        <tr>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/title/order_asc/$next_order_asc"; ?>">Contact
                    Name</a></th>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/phone/order_asc/$next_order_asc"; ?>">Phone</a>
            </th>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/email/order_asc/$next_order_asc"; ?>">Email</a>
            </th>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/skype/order_asc/$next_order_asc"; ?>">Skype</a>
            </th>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/viber/order_asc/$next_order_asc"; ?>">Viber</a>
            </th>
            <th>
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/whatsapp/order_asc/$next_order_asc"; ?>">What's
                    app</a></th>
            <th class="text-center" width="20%">
                <a href="<?php echo HelperUrl::baseUrl() . "contacts/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created
                    Date</a>
            </th>
            <th class="text-center" width="15%" colspan="5">&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        <?php if (!count($items)): ?>
            <tr>
                <td colspan="12">No record found!</td>
            </tr>
        <?php endif; ?>

        <?php foreach ($items as $k => $v): ?>
            <tr>
                <td>

                    <a class="btn-object-modal" modal-size="modal-sm"
                       modal-url="<?php echo HelperUrl::baseUrl() . 'contacts/view/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <?php echo CHtml::encode($v['title']) ?>
                    </a>

                </td>
                <td><?php echo CHtml::encode($v['phone']) ?></td>
                <td><?php echo CHtml::encode($v['email']) ?></td>
                <td><?php echo CHtml::encode($v['skype']) ?></td>
                <td><?php echo CHtml::encode($v['viber']) ?></td>
                <td><?php echo CHtml::encode($v['whatsapp']) ?></td>
                <td class="text-center">
                    <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-primary btn-sm btn-object-modal" modal-size="modal-sm"
                       modal-url="<?php echo HelperUrl::baseUrl() . 'contacts/view/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-info"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-success btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'comments/index/type/contact/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-comment'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-comment"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-info btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'attachments/index/type/contact/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-file'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-file"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl() . 'contacts/edit/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>

                <td width="5%" class="text-center">
                    <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                       modal-url="<?php echo HelperUrl::baseUrl() . 'contacts/delete/id/' . $v['id'] ?>"
                       modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <?php */ ?>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>
</div>