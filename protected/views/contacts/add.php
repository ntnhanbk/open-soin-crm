<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>contacts/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Lead</label>

        <select class="form-control" name="lead_id">
            <option value="0">-- Choose Lead --</option>
            <?php foreach ($leads as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>" <?php echo (isset($selected_lead['id']) && $selected_lead['id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>

    </div>
    <div class="form-group">
        <label>Contact Name</label>
        <input type="text" class="form-control" name="title" placeholder="" value="<?php echo (isset($selected_lead)) ? $selected_lead['title'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone" placeholder="" value="<?php echo (isset($selected_lead)) ? $selected_lead['phone'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" placeholder="" value="<?php echo (isset($selected_lead)) ? $selected_lead['email'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Skype</label>
        <input type="text" class="form-control" name="skype" placeholder="">
    </div>
    <div class="form-group">
        <label>Viber</label>
        <input type="text" class="form-control" name="viber" placeholder="">
    </div>
    <div class="form-group">
        <label>What's app</label>
        <input type="text" class="form-control" name="whatsapp" placeholder="">
    </div>
</form>