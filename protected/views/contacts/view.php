<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['title']) ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="40%"><strong>Phone</strong></td>
        <td width="60%"><?php echo CHtml::encode($item['phone']) ?></td>
    </tr>
    <tr>
        <td><strong>Email</strong></td>
        <td><?php echo CHtml::encode($item['email']) ?></td>
    </tr>
    <tr>
        <td><strong>Skype</strong></td>
        <td><?php echo CHtml::encode($item['skype']) ?></td>
    </tr>
    <tr>
        <td><strong>Viber</strong></td>
        <td><?php echo CHtml::encode($item['viber']) ?></td>
    </tr>
    <tr>
        <td><strong>What's app</strong></td>
        <td><?php echo CHtml::encode($item['whatsapp']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>