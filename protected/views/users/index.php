<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>User Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">User</li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="text-right space10">
        <a href="" class="btn btn-primary btn-object-modal" modal-size=""
           modal-url="<?php echo HelperUrl::baseUrl(); ?>users/add"
           modal-title="<i class='fa fa-plus'></i> Add New User"><i class="fa fa-plus"></i> Add New User</a>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Users
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th width="21%">
                        <a href="<?php echo HelperUrl::baseUrl() . "users/index/order_by/username/order_asc/$next_order_asc"; ?>">Username</a>
                    </th>
                    <th width="15%"><a
                            href="<?php echo HelperUrl::baseUrl() . "users/index/order_by/display_name/order_asc/$next_order_asc"; ?>">Full
                            Name</a></th>
                    <th width="20%"><a
                            href="<?php echo HelperUrl::baseUrl() . "users/index/order_by/email/order_asc/$next_order_asc"; ?>">Email</a>
                    </th>
                    <th class="text-center" width="10%">
                        <a href="<?php echo HelperUrl::baseUrl() . "users/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created</a>
                    </th>
                    <th class="text-center" width="10%" colspan="4">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="8">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td>
                            <a class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'users/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['display_name']) ?>">
                    <span
                        class="label label-info"><?php echo (isset($user_roles[$v['role']])) ? CHtml::encode($user_roles[$v['role']]) : 'Anonymous' ?></span>
                                <?php echo CHtml::encode($v['username']) ?>
                            </a>
                        </td>

                        <td>
                            <?php echo CHtml::encode($v['display_name']) ?>
                        </td>

                        <td>
                            <a href="mailto:<?php echo CHtml::encode($v['email']) ?>">
                                <?php echo CHtml::encode($v['email']) ?>
                            </a>
                        </td>

                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                        <td class="text-center">
                            <a href="<?php echo HelperUrl::baseUrl() . 'users/sign_in_as/id/' . $v['id'] ?>"
                               class="btn btn-success btn-sm">
                                <i class="fa fa-sign-in"></i>
                            </a>
                        </td>

                        <td class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'users/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['display_name']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'users/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['display_name']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>


    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>


</div>

