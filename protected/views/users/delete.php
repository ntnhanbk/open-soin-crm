<form method="post" action="<?php echo HelperUrl::baseUrl() . 'users/delete_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <p>
        Are you sure to delete <strong><?php echo CHtml::encode($item['display_name']) ?></strong>?
    </p>

    <input type="hidden" name="action" value="delete" />
</form>