<form method="post" action="<?php echo HelperUrl::baseUrl() . 'users/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data" autocomplete="off">

    <div class="form-group">
        <label>First Name</label>
        <input type="text" class="form-control" name="first_name" placeholder="Enter First Name"
               value="<?php echo CHtml::encode($item['first_name']) ?>">
    </div>

    <div class="form-group">
        <label>Lats Name</label>
        <input type="text" class="form-control" name="last_name" placeholder="Enter Lats Name"
               value="<?php echo CHtml::encode($item['last_name']) ?>">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email"
               value="<?php echo CHtml::encode($item['email']) ?>">
    </div>

    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control" name="username"
               value="<?php echo CHtml::encode($item['username']) ?>">
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password">
    </div>

    <div class="form-group">
        <label>Confirm Password</label>
        <input type="password" class="form-control" name="confirm_password">
    </div>

    <div class="form-group">
        <label>Role</label>
        <select class="form-control" name="role">
            <option value=""> -- User Role --</option>
            <?php foreach (Helper::get_app_data('user_roles') as $key => $role): ?>
                <option value="<?php echo CHtml::encode($key); ?>"
                    <?php if (Helper::request('role', $item['role']) == $key) echo 'selected'; ?>>
                    <?php echo CHtml::encode($role); ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

</form>