<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2"><?php echo CHtml::encode($item['display_name']) ?></th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td width="40%"><strong>Username</strong></td>
        <td width="60%"><?php echo  CHtml::encode($item['username']) ?></td>
    </tr>

    <tr>
        <td><strong>Email</strong></td>
        <td><?php echo CHtml::encode($item['email']) ?></td>
    </tr>

    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>

    </tbody>
</table>