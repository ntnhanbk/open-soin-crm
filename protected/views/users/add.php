<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>users/add_handler" enctype="multipart/form-data" autocomplete="off">

    <div class="form-group">
        <label>First Name</label>
        <input type="text" class="form-control" name="first_name" placeholder="Enter First Name">
    </div>

    <div class="form-group">
        <label>Lats Name</label>
        <input type="text" class="form-control" name="last_name" placeholder="Enter Lats Name">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email">
    </div>

    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control" name="username">
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password">
    </div>

    <div class="form-group">
        <label>Confirm Password</label>
        <input type="password" class="form-control" name="confirm_password">
    </div>

    <div class="form-group">
        <label>Role</label>
        <select class="form-control" name="role">
            <option value=""> -- User Role --</option>
            <?php foreach (Helper::get_app_data('user_roles') as $key => $role): ?>
                <option value="<?php echo CHtml::encode($key); ?>"
                    <?php if (Helper::request('role') == $key) echo 'selected'; ?>>
                    <?php echo CHtml::encode($role); ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

</form>
