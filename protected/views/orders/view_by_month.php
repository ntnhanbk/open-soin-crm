<table class="table table-bordered table-striped "
    <thead>
    <tr>
        <th width="20%">Order Name</th>
        <th width="20%">Deal Name</th>
        <th width="5%" class="text-center">Currency</th>
        <th width="5%" class="text-right">Amount</th>
        <th width="5%" class="text-right">Invoices</th>
        <th width="5%" class="text-right">Expenses</th>
        <th width="5%" class="text-right">Profit</th>
        <th width="10%" class="text-center">Created</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="8">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php $sum=0; foreach ($items as $k => $v): $sum+=$v['amt'];?>
        <tr>
            <td>
                <?php echo Helper::display_order_status($v['status']);?>
                <a class="btn-object-modal" modal-size="modal-lg"
                   modal-url="<?php echo HelperUrl::baseUrl() . 'orders/view/id/' . $v['id'] ?>"
                   modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                    <?php echo CHtml::encode($v['title']) ?>
                </a>
            </td>
            <td><?php echo CHtml::encode($v['deal_title']) ?></td>
            <td class="text-center"><?php echo CHtml::encode($v['currency']) ?></td>
            <td class="text-right"><?php echo CHtml::encode(number_format($v['amt'], 2)) ?></td>
            <td class="text-right"><?php echo CHtml::encode(number_format($v['total_invoices'], 2)) ?></td>
            <td class="text-right"><?php echo CHtml::encode(number_format($v['total_expenses'], 2)) ?></td>
            <td class="text-right"><?php echo CHtml::encode(number_format($v['total_invoices'] - $v['total_expenses'], 2)) ?></td>
            <td class="text-center">
                <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tfooter>
        <tr>
            <td colspan="3" class="text-center"><strong>Total</strong></td>
            <td class="text-right"><strong>$ <?php echo number_format($sum);?></strong></td>
            <td colspan="4"></td>
        </tr>
    </tfooter>
    </tbody>
</table>