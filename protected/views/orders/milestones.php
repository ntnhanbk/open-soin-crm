<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">
                    <?php echo Helper::display_order_status($item['status']); ?>
                    <?php echo CHtml::encode($item['title']) ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="40%"><strong>Deal Name</strong></td>
                <td width="60%"><?php echo CHtml::encode($item['deal_title']) ?></td>
            </tr>
            <tr>
                <td><strong>Amount</strong></td>
                <td><?php echo CHtml::encode($item['currency'] . ' ' . number_format($item['amt'], 2)) ?></td>
            </tr>
            <tr>
                <td><strong>Created Date</strong></td>
                <td><?php echo Helper::date($item['date_added']) ?></td>
            </tr>
            <tr>
                <td><strong>Created By</strong></td>
                <td><?php echo CHtml::encode($item['author_name']) ?></td>
            </tr>
            </tbody>
        </table>

    </div>
    <div class="col-md-8">
        <?php if ($milestones): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Milestones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($milestones as $k => $v): ?>
                    <tr>
                        <td width="20%"><?php echo Helper::date($v['milestone']) ?></td>
                        <td width="80%"><?php echo Helper::display_milestone_status($v['status']); ?> <a
                                class="btn-object-modal" modal-size=""
                                modal-url="<?php echo HelperUrl::baseUrl() . 'milestones/view/id/' . $v['id'] ?>"
                                modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo CHtml::encode($v['title']) ?>
                            </a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>
