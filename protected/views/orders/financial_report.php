<div class="row">
    <div class='col-sm-4'>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">
                    <?php echo Helper::display_order_status($item['status']); ?>
                    <?php echo CHtml::encode($item['title']) ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="40%"><strong>Deal Name</strong></td>
                <td width="60%"><?php echo CHtml::encode($item['deal_title']) ?></td>
            </tr>
            <tr>
                <td><strong>Amount</strong></td>
                <td><?php echo CHtml::encode($item['currency'] . ' ' . number_format($item['amt'], 2)) ?></td>
            </tr>
            <tr>
                <td><strong>Created Date</strong></td>
                <td><?php echo Helper::date($item['date_added']) ?></td>
            </tr>
            <tr>
                <td><strong>Created By</strong></td>
                <td><?php echo CHtml::encode($item['author_name']) ?></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Invoice Name</th>
                <th class="text-right">Amount</th>
                <th class="text-right">Percentage</th>
            </tr>
            </thead>
            <tbody>

            <?php if (!count($invoices)): ?>
                <tr>
                    <td colspan="3">No record found!</td>
                </tr>
            <?php endif; ?>

            <?php $sum=0; foreach ($invoices as $k => $v): $sum+=$v['amt'];?>
                <tr>
                    <td>
                        <?php echo Helper::display_invoice_status($v['status']); ?>

                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'invoices/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?>
                        </a>

                    </td>
                    <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
                    <td class="text-right"><?php echo CHtml::encode(number_format($v['amt']/$item['amt']*100, 2)) ?>%</td>
                </tr>
            <?php endforeach; ?>
            <tfooter>
                <tr>
                    <td class="text-center"><strong>Total</strong></td>
                    <td class="text-right">
                        <strong><?php echo CHtml::encode($item['currency'] . ' ' . number_format($sum, 2)) ?></strong>
                    </td>
                    <td></td>
                </tr>
            </tfooter>
            </tbody>
        </table>
    </div>
    <div class='col-sm-8'>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Expense Name</th>
                <th class="text-right">Amount</th>
                <th class="text-right">Percentage</th>
                <th class="text-left">Receiver</th>
                <th class="text-center">Created</th>
            </tr>
            </thead>
            <tbody>

            <?php if (!count($expenses)): ?>
                <tr>
                    <td colspan="5">No record found!</td>
                </tr>
            <?php endif; ?>

            <?php $sum = 0;
            foreach ($expenses as $k => $v): $sum += $v['amt']; ?>
                <tr>
                    <td>
                        <?php echo Helper::display_expense_status($v['status']); ?>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?>
                        </a>
                    </td>
                    <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
                    <td class="text-right"><?php echo CHtml::encode(number_format($v['amt']/$item['amt']*100, 2)) ?>%</td>
                    <td><?php echo CHtml::encode($v['receiver']) ?></td>
                    <td class="text-center">
                        <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tfooter>
            <tr>
                <td class="text-center"><strong>Profit</strong></td>
                <td class="text-right">
                    <strong><?php echo CHtml::encode($item['currency'] . ' ' . number_format($item['amt'] - $sum, 2)) ?></strong>
                </td>
                <td class="text-right">
                    <strong><?php echo CHtml::encode(number_format(($item['amt'] - $sum)/$item['amt']*100, 2)) ?>%</strong>
                </td>
                <td colspan="2"></td>
            </tr>
            </tfooter>
            </tbody>
        </table>
    </div>
</div>