<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Order Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Orders</li>
        </ol>
    </div>
</div>


<div class="container-fluid">

    <div class="space10">
        <div class="clearfix">
            <div class="pull-left">
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>orders/index/status/0" class="btn btn-info">New Orders</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>orders/index/status/1" class="btn btn-warning">Completed Orders</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>orders/index/status/2" class="btn btn-danger">Delivered Orders</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>orders/index/status/3" class="btn btn-success">Paid Orders</a>
                </div>
                <div class="btn-group">
                    <a href="<?php echo HelperUrl::baseUrl(); ?>orders/index/status/-1" class="btn btn-default">Invalid Orders</a>
                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="" class="btn btn-primary btn-object-modal" modal-size=""
                       modal-url="<?php echo HelperUrl::baseUrl(); ?>orders/add"
                       modal-title="<i class='fa fa-plus'></i> Add New Order"><i class="fa fa-plus"></i> Add New Order</a>
                </div>
            </div>
        </div>
    </div>

    <div class="space10">&nbsp;</div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Orders
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th width="20%"><a
                            href="<?php echo HelperUrl::baseUrl() . "orders/index/order_by/title/order_asc/$next_order_asc"; ?>">Order
                            Name</a></th>
                    <th width="20%"><a
                            href="<?php echo HelperUrl::baseUrl() . "orders/index/order_by/deal_title/order_asc/$next_order_asc"; ?>">Deal
                            Name</a></th>
                    <th width="5%" class="text-center">Currency</th>
                    <th width="5%" class="text-right"><a
                            href="<?php echo HelperUrl::baseUrl() . "orders/index/order_by/amt/order_asc/$next_order_asc"; ?>">Amount</a>
                    </th>
                    <th width="5%" class="text-right">Invoices</th>
                    <th width="5%" class="text-right">Expenses</th>
                    <th width="5%" class="text-right">Profit</th>
                    <th width="10%" class="text-center">
                        <a href="<?php echo HelperUrl::baseUrl() . "orders/index/order_by/date_added/order_asc/$next_order_asc"; ?>">Created</a>
                    </th>
                    <th class="text-center" width="15%" colspan="4">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="15">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td>

                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo Helper::display_order_status($v['status']);?>
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>

                        </td>
                        <td><?php echo CHtml::encode($v['deal_title']) ?></td>
                        <td class="text-center"><?php echo CHtml::encode($v['currency']) ?></td>
                        <td class="text-right"><?php echo CHtml::encode(number_format($v['amt'], 2)) ?></td>
                        <td class="text-right"><?php echo CHtml::encode(number_format($v['total_invoices'], 2)) ?></td>
                        <td class="text-right"><?php echo CHtml::encode(number_format($v['total_expenses'], 2)) ?></td>
                        <td class="text-right"><?php echo CHtml::encode(number_format($v['total_invoices'] - $v['total_expenses'], 2)) ?></td>
                        <td class="text-center">
                            <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-primary btn-sm btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/financial_report/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-dollar'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-area-chart"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-info btn-sm btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/milestones/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-calendar'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-calendar"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>

        </div>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

</div>
