<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">
                    <?php echo Helper::display_order_status($item['status']); ?>
                    <?php echo CHtml::encode($item['title']) ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="40%"><strong>Deal Name</strong></td>
                <td width="60%"><?php echo CHtml::encode($item['deal_title']) ?></td>
            </tr>
            <tr>
                <td><strong>Amount</strong></td>
                <td><?php echo CHtml::encode($item['currency'] . ' ' . number_format($item['amt'], 2)) ?></td>
            </tr>
            <tr>
                <td><strong>Created Date</strong></td>
                <td><?php echo Helper::date($item['date_added']) ?></td>
            </tr>
            <tr>
                <td><strong>Created By</strong></td>
                <td><?php echo CHtml::encode($item['author_name']) ?></td>
            </tr>
            </tbody>
        </table>

        <?php if ($milestones): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Milestones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($milestones as $k => $v): ?>
                    <tr>
                        <td width="40%"><?php echo Helper::date($v['milestone']) ?></td>
                        <td width="60%"><?php echo Helper::display_milestone_status($v['status']);?> <a class="btn-object-modal" modal-size=""
                                           modal-url="<?php echo HelperUrl::baseUrl() . 'milestones/view/id/' . $v['id'] ?>"
                                           modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo CHtml::encode($v['title']) ?>
                            </a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($request_attachments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Request Attachments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($request_attachments as $k => $v): $attachment = unserialize($v['attachment'])?>
                    <tr><td><a href='<?php echo HelperUrl::upload_url() . $attachment['url'] ?>' target='_blank'><i class="fa fa-cloud-download"></i> <?php echo CHtml::encode($v['attachment_path']) ?></a></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($quote_attachments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Quote Attachments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quote_attachments as $k => $v): $attachment = unserialize($v['attachment'])?>
                    <tr><td><a href='<?php echo HelperUrl::upload_url() . $attachment['url'] ?>' target='_blank'><i class="fa fa-cloud-download"></i> <?php echo CHtml::encode($v['attachment_path']) ?></a></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($deal_attachments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Deal Attachments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($deal_attachments as $k => $v): $attachment = unserialize($v['attachment'])?>
                    <tr><td><a href='<?php echo HelperUrl::upload_url() . $attachment['url'] ?>' target='_blank'><i class="fa fa-cloud-download"></i> <?php echo CHtml::encode($v['attachment_path']) ?></a></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($order_attachments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Order Attachments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order_attachments as $k => $v): $attachment = unserialize($v['attachment'])?>
                    <tr><td><a href='<?php echo HelperUrl::upload_url() . $attachment['url'] ?>' target='_blank'><i class="fa fa-cloud-download"></i> <?php echo CHtml::encode($v['attachment_path']) ?></a></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <div class="col-md-8">
        <?php if ($request['description']): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Request Description</th>
                </tr>
                </thead>
                <tbody>
                <tr><td><?php echo Helper::make_url_clickable(nl2br($request['description']));?></td></tr>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($request_comments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Request Comments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($request_comments as $k => $v): ?>
                    <tr><td><?php echo Helper::make_url_clickable(CHtml::encode($v['comment'])) ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($quote_comments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Quote Comments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quote_comments as $k => $v): ?>
                    <tr><td><?php echo Helper::make_url_clickable(CHtml::encode($v['comment'])) ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php if ($deal_comments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Deal Comments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($deal_comments as $k => $v): ?>
                    <tr><td><?php echo Helper::make_url_clickable(CHtml::encode($v['comment'])) ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>



        <?php if ($order_comments): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Order Comments</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order_comments as $k => $v): ?>
                    <tr><td><?php echo Helper::make_url_clickable(CHtml::encode($v['comment'])) ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>
