<p>
    Hi <?php echo CHtml::encode($user['first_name']); ?>,
</p>

<p>
    You have been registered a new account in SoinCRM system.
</p>

<p>Account information:</p>
<ul>
    <li><strong>Username: </strong> <?php echo CHtml::encode($user['username']) ?></li>
    <li><strong>Password: </strong> <?php echo CHtml::encode($raw_password) ?></li>
</ul>

<p>
    Please click <a href="<?php echo HelperUrl::baseUrl(true) . 'site/verify_account/token/' . $token; ?>">HERE</a> to verify your account!
</p>

<p>
    Link not working? Copy and paste the one below into your browser:
    <br />
    <?php echo HelperUrl::baseUrl(true) . 'site/verify_account/token/' . $token; ?>
</p>

<p>
    Thank you,
</p>
<p>
    SoinCRM support team
</p>