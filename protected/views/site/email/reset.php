<p>
    Hi <?php echo CHtml::encode($user['first_name']); ?>,
</p>

<p>
    We have received a request to change your password.
</p>

<p>
    Your account is now ready for use. Please use the below information to log in.
</p>

<p>
    Login here: <?php echo HelperUrl::baseUrl(true) . 'site/sign_in'; ?>
</p>

<p>
    <ul>
    <li><strong>Username: </strong> <?php echo CHtml::encode($user['username']) ?></li>
    <li><strong>Password: </strong> <?php echo CHtml::encode($new_password) ?></li>
    </ul>
</p>

<p>
    Thank you,
</p>
<p>
    SoinCRM support team
</p>