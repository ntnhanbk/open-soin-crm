<p>
    Hi <?php echo CHtml::encode($user['first_name']); ?>,
</p>

<p>
    Welcome to SoinCRM system!
</p>

<p>
    Your account is now ready for use. Please use the below information to log in.
</p>

<p>
    Login here:
    <a href="<?php echo HelperUrl::baseUrl(true) . 'site/sign_in'; ?>">
        <?php echo HelperUrl::baseUrl(true) . 'site/sign_in'; ?>
    </a>
</p>

<p>
    Thank you,
</p>
<p>
    SoinCRM support team
</p>