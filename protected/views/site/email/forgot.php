<p>
    Hi <?php echo CHtml::encode($user['first_name']); ?>,
</p>

<p>
    To get back into your account, you'll need to create a new password.
</p>

<p>
    It's easy:
    <ol>
    <li>
        Click the link below to open a secure browser window.
    </li>
    <li>
        Confirm that you're the owner of the account, and then follow the instructions.
    </li>
    </ol>
</p>

<p>
    Please click <a href="<?php echo HelperUrl::baseUrl(true) . 'site/reset_password/token/' . $token; ?>">HERE</a> to reset your password!
</p>

<p>
    Link not working? Copy and paste the one below into your browser:
    <br />
    <?php echo HelperUrl::baseUrl(true) . 'site/reset_password/token/' . $token; ?>
</p>

<p>
    Thank you,
</p>
<p>
    SoinCRM support team
</p>