<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Invoice</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Invoices</li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="ibox-content p-xl">
        <div class="row">
            <div class="col-sm-6">
                <h5>From:</h5>
                <address>
                    <strong><?php echo $invoice['title'] ?></strong>
                </address>
            </div>

            <div class="col-sm-6 text-right">
                <h4>Invoice No.</h4>
                <h4 class="text-navy">INV-<?php echo $invoice['token_key'] ?></h4>
                <span>To:</span>
                <address>
                    <strong><?php echo $invoice['admin_email'] ?></strong><br>
                </address>
                <p>
                    <span><strong>Invoice Date:</strong> <?php echo date('M d, Y', $invoice['date_added']) ?></span><br/>
                    <span><strong>Paid Date:</strong> </strong> <?php echo $invoice['last_modified'] ?  date('M d, Y', $invoice['last_modified']) : '' ?></span>
                </p>
            </div>
        </div>

        <table class="table invoice-total">
            <tbody>
            <tr>
                <td><strong>Price:</strong></td>
                <td>$<?php echo number_format($invoice['amt']) ?></td>
            </tr>
            <tr>
                <td><strong>TAX :</strong></td>
                <td>$0</td>
            </tr>
            <tr>
                <td><strong>TOTAL :</strong></td>
                <td>$<?php echo number_format($invoice['amt']) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>