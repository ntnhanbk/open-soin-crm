<div class="row">
    <div class="col-sm-4">
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-3">
                    <h1 class="text-center"><i class="fa fa-shopping-cart"></i></h1>
                </div>
                <div class="col-xs-9">
                    <h4>Total Orders</h4>

                    <h1>$<?php echo number_format($total_orders); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-3">
                    <h1 class="text-center"><i class="fa fa-inbox"></i></h1>
                </div>
                <div class="col-xs-9">
                    <h4>Total Invoices</h4>

                    <h1>$<?php echo number_format($total_invoices); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-3">
                    <h1 class="text-center"><i class="fa fa-tags"></i></h1>
                </div>
                <div class="col-xs-9">
                    <h4>Total Expenses</h4>

                    <h1>$<?php echo number_format($total_expenses); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td colspan="2">Upcoming Milestones</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($upcoming_milestones as $k => $v): ?>
                <tr>
                    <td width="30%"><?php echo Helper::date($v['milestone']) ?></td>
                    <td width="70%">
                        <a class="btn-object-modal" modal-size="modal-lg"
                           modal-url="<?php echo HelperUrl::baseUrl() . 'orders/view/id/' . $v['order_id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Order: <?php echo CHtml::encode($v['order_title']) ?>">
                            <span class="label label-info"><?php echo CHtml::encode($v['order_title']) ?></span>
                        </a>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'milestones/edit/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo $v['title'];?>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        <?php
        if ($monthly_reports):
            ?>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td class="text-center">Month</td>
                    <td class="text-center">Quotes</td>
                    <td class="text-center">Deals</td>
                    <td class="text-center">Orders</td>
                    <td class="text-center">Invoices</td>
                    <td class="text-center">Expenses</td>
                </tr>
                </thead>
                <tbody>
                <?php
                $monthly_report_quotes = 0;
                $monthly_report_deals = 0;
                $monthly_report_orders = 0;
                $monthly_report_invoices = 0;
                $monthly_report_expenses = 0;
                foreach ($monthly_reports as $k => $v):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $k; ?></td>
                        <td class="text-right">
                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'quotes/view_by_month/month/' . $k ?>"
                               modal-title="Quotes - <?php echo CHtml::encode($k) ?>">
                                $ <?php echo number_format($v['quotes']);
                                $monthly_report_quotes += $v['quotes']; ?>
                            </a>

                        </td>
                        <td class="text-right">
                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'deals/view_by_month/month/' . $k ?>"
                               modal-title="Deals - <?php echo CHtml::encode($k) ?>">
                                $ <?php echo number_format($v['deals']);
                                $monthly_report_deals += $v['deals']; ?>
                            </a>
                        </td>
                        <td class="text-right">
                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'orders/view_by_month/month/' . $k ?>"
                               modal-title="Orders - <?php echo CHtml::encode($k) ?>">
                                $ <?php echo number_format($v['orders']);
                                $monthly_report_orders += $v['orders']; ?>
                            </a>
                        </td>
                        <td class="text-right">
                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'invoices/view_by_month/month/' . $k ?>"
                               modal-title="Invoices - <?php echo CHtml::encode($k) ?>">
                                $ <?php echo number_format($v['invoices']);
                                $monthly_report_invoices += $v['invoices']; ?>
                            </a>
                        </td>
                        <td class="text-right">
                            <a class="btn-object-modal" modal-size="modal-lg"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view_by_month/month/' . $k ?>"
                               modal-title="Expenses - <?php echo CHtml::encode($k) ?>">
                                $ <?php echo number_format($v['expenses']);
                                $monthly_report_expenses += $v['expenses']; ?>
                            </a>
                        </td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
                <tfooter>
                    <td class="text-center"><strong>Total</strong></td>
                    <td class="text-right"><strong>$ <?php echo number_format($monthly_report_quotes);?></strong></td>
                    <td class="text-right"><strong>$ <?php echo number_format($monthly_report_deals);?></strong></td>
                    <td class="text-right"><strong>$ <?php echo number_format($monthly_report_orders);?></strong></td>
                    <td class="text-right"><strong>$ <?php echo number_format($monthly_report_invoices);?></strong></td>
                    <td class="text-right"><strong>$ <?php echo number_format($monthly_report_expenses);?></strong></td>
                </tfooter>
            </table>
        <?php
        endif;
        ?>
    </div>
</div>


<hr/>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="highcharts" id="sale-funnel-highcharts">

        </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="highcharts" id="business-highcharts">

        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="highcharts" id="invoice-expense-highcharts">

        </div>
    </div>
</div>

<hr/>

<div class="row dashboard">
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Requests <span class="badge"><?php echo $request_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($requests as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size="modal-lg"
                           modal-url="<?php echo HelperUrl::baseUrl() . 'requests/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Request: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span class="label label-primary"><?php echo $v['contact_title']; ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Quotes <span class="badge"><?php echo $quote_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($quotes as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'quotes/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Quote: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span
                                class="label label-info"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td class="text-right">$ <?php echo number_format($quote_sum, 2); ?></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Deals <span class="badge"><?php echo $deal_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($deals as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'deals/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Deal: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span
                                class="label label-warning"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td class="text-right">$ <?php echo number_format($deal_sum, 2); ?></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Orders <span class="badge"><?php echo $order_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size="modal-lg"
                           modal-url="<?php echo HelperUrl::baseUrl() . 'orders/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Order: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span
                                class="label label-success"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td class="text-right">$ <?php echo number_format($order_sum, 2); ?></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Invoices <span class="badge"><?php echo $invoice_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($invoices as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'invoices/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Invoice: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span
                                class="label label-<?php echo Helper::label_invoice_status($v['status']); ?>"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td class="text-right">$ <?php echo number_format($invoice_sum, 2); ?></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Expenses <span class="badge"><?php echo $expense_count; ?></span></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($expenses as $k => $v): ?>
                <tr>
                    <td>
                        <a class="btn-object-modal" modal-size=""
                           modal-url="<?php echo HelperUrl::baseUrl() . 'expenses/view/id/' . $v['id'] ?>"
                           modal-title="<i class='fa fa-info'></i> Expense: <?php echo CHtml::encode($v['title']) ?>">
                            <?php echo CHtml::encode($v['title']) ?><br/>
                            <span
                                class="label label-<?php echo Helper::label_expense_status($v['status']); ?>"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td class="text-right">$ <?php echo number_format($expense_sum, 2); ?></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<hr/>

<div class="row">
    <?php foreach ($orders as $k => $v): ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div id="order<?php echo $v['id']; ?>-pie-highcharts"></div>
        </div>
    <?php endforeach; ?>
</div>
<script>
    $(document).ready(function () {
        $('#business-highcharts').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Business Monitor'
            },
            xAxis: {
                categories: [<?php echo $chart_date;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'USD'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>${point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Quotes',
                    data: [<?php echo $chart_quotes;?>]
                },
                {
                    name: 'Deals',
                    data: [<?php echo $chart_deals;?>]
                },
                {
                    name: 'Orders',
                    data: [<?php echo $chart_orders;?>]
                }
            ]
        });

        $('#invoice-expense-highcharts').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Cashflow'
            },
            xAxis: {
                categories: [<?php echo $chart_date;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'USD'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>${point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Invoices',
                    data: [<?php echo $chart_invoices;?>]
                },
                {
                    name: 'Expenses',
                    data: [<?php echo $chart_expenses;?>]
                }
            ]
        });

        $('#sale-funnel-highcharts').highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: {
                text: 'Sales funnel',
                x: -50
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '10%',
                    neckHeight: '0%'

                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent
                }
            },
            legend: {
                enabled: false
            },
            series: [
                {
                    name: 'Units',
                    data: [
                        ['Requests', <?php echo $request_count; ?>],
                        ['Quotes', <?php echo $quote_count; ?>],
                        ['Deals', <?php echo $deal_count; ?>],
                        ['Orders', <?php echo $order_count; ?>],
                    ]
                }
            ]
        });


        <?php foreach ($orders as $k => $v): ?>
        $('#order<?php echo $v['id']; ?>-pie-highcharts').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
            },
            title: {
                text: '<?php echo str_replace(' ','<br/>',$v['title']);?>',
                align: 'center',
                verticalAlign: 'middle',
                y: 50
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            series: [
                {
                    type: 'pie',
                    name: '<?php echo $v['title'];?>',
                    innerSize: '50%',
                    data: [
                        <?php $sum=0; foreach ($all_expenses as $ek => $ev): if ($ev['order_id']!=$v['id']) continue; $sum+=$ev['group_amt'];?>
                        ['<?php echo $ev['receiver'];?>', <?php echo $ev['group_amt'];?>],
                        <?php endforeach; ?>
                        ['Remain', <?php echo $v['amt']-$sum;?>],
                    ]
                }
            ]
        });
        <?php endforeach; ?>
    });
</script>