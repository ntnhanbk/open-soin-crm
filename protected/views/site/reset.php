<form id="login-form" method="post" action="" autocomplete="off" style="max-width: 500px">
    <h2 class="form-signin-heading">Reset Password</h2>

    <div class="alert alert-success">
        Hi <?php echo CHtml::encode($user['first_name']) ?>, <br />
        An email have been sent to <strong><?php echo CHtml::encode($user['email']) ?></strong>. <br/>
        Please check inbox of your email to get account information!
    </div>


    <div class="space10">
        <a href="<?php echo HelperUrl::baseUrl() . 'site/sign_in'; ?>">
            Go Back
        </a>
</form>
