<ul class="space10 list-group">
    <?php foreach ($sum_results as $k => $v): ?>
        <li class="list-group-item">
            <?php //echo $k; ?>
            <?php foreach ($v as $vk => $vv): ?>
                <span
                    class="label label-<?php echo Helper::label_expense_status($vv['status']); ?>"><?php echo CHtml::encode($vv['currency'] . ' ' . number_format($vv['total'], 2)) ?></span>
            <?php endforeach; ?>
        </li>
    <?php endforeach; ?>
</ul>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="25%">Order Name</th>
        <th width="35%">Expense Name</th>
        <th width="25%" class="text-right">Amount</th>
        <th class="text-center" width="15%">Created</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($expenses)): ?>
        <tr>
            <td colspan="4">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php foreach ($expenses as $k => $v): ?>
        <tr>
            <td><?php echo CHtml::encode($v['order_title']) ?></td>
            <td><?php echo Helper::display_expense_status($v['status']); ?> <?php echo CHtml::encode($v['title']) ?></td>
            <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
            <td class="text-center"><?php echo CHtml::encode(Helper::date($v['date_added'])) ?></td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>