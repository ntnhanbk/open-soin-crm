<div class="container-fluid space30">

    <div class="row">
        <?php foreach ($orders as $k => $v): ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?php echo $v['title']; ?></h5>
                    </div>
                    <div class="ibox-content text-center">
                        <div id="order<?php echo $v['id']; ?>-pie-highcharts"></div>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
    <script>
        $(document).ready(function () {

            <?php foreach ($orders as $k => $v): ?>
            $('#order<?php echo $v['id']; ?>-pie-highcharts').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                },
                title: {
                    text: '',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 50
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%']
                    }
                },
                series: [
                    {
                        type: 'pie',
                        name: '<?php echo $v['title'];?>',
                        innerSize: '50%',
                        data: [
                            <?php $sum=0; foreach ($all_expenses as $ek => $ev): if ($ev['order_id']!=$v['id']) continue; $sum+=$ev['group_amt'];?>
                            ['<?php echo $ev['receiver_title'];?>', <?php echo $ev['group_amt'];?>],
                            <?php endforeach; ?>
                            ['Remain', <?php echo $v['amt']-$sum;?>],
                        ]
                    }
                ]
            });
            <?php endforeach; ?>
        });
    </script>
</div>