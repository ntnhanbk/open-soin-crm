<form method="post" action="<?php echo HelperUrl::baseUrl() . 'site/profile_handler/' ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>User Name</label>
        <input type="text" class="form-control" name="username" disabled
               value="<?php echo CHtml::encode($item['username']) ?>">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email"
               value="<?php echo CHtml::encode($item['email']) ?>">
    </div>
    <div class="form-group">
        <label>First Name</label>
        <input type="text" class="form-control" name="first_name"
               value="<?php echo CHtml::encode($item['first_name']) ?>">
    </div>
    <div class="form-group">
        <label>Last Name</label>
        <input type="text" class="form-control" name="last_name"
               value="<?php echo CHtml::encode($item['last_name']) ?>">
    </div>

    <div class="form-group">
        <label>Company Name</label>
        <input type="text" class="form-control" name="company_name"
               value="<?php echo CHtml::encode($item['company_name']) ?>">
    </div>

    <div class="form-group">
        <label>Avatar</label>
        <input type="file" name="thumbnail" />
    </div>

    <?php if (Helper::is_serialized($item['thumbnail'])): ?>
        <div class="img-thumbnail">
            <img src="<?php echo Helper::get_thumbnail($item['thumbnail'], 'full') ?>" width="120px"/>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label>Company Logo</label>
        <input type="file" name="company_logo" />
    </div>

    <?php if (Helper::is_serialized($item['company_logo'])): ?>
        <div class="img-thumbnail">
            <img src="<?php echo Helper::get_thumbnail($item['company_logo'], 'full') ?>" width="120px"/>
        </div>
    <?php endif; ?>

</form>