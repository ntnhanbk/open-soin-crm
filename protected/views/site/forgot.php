<form id="login-form" method="post" action="" autocomplete="off"
    <?php if (Helper::request('sent')) echo "style='max-width:500px'"; ?>>
    <h2 class="form-signin-heading">Forgot Password</h2>

    <?php if (isset($form_result['status']) && $form_result['status'] == 'error'): ?>
        <?php echo $form_result['message']; ?>
    <?php endif; ?>

    <?php if (Helper::request('sent')): ?>

        <div class="alert alert-success text-center">
            An email sent to <strong><?php echo Helper::request('email') ?></strong>. <br/>
            Please check inbox of your email to get more information!
        </div>

    <?php else: ?>

        <?php if (Helper::request('token') == 'expired'): ?>
            <div class="alert alert-danger text-center">
                <strong>Your verify code</strong> is expired. <br /> Please contact us to get more information!
            </div>
        <?php endif; ?>

        <?php if (Helper::request('token') == 'invalid'): ?>
            <div class="alert alert-danger text-center">
                <strong>Your verify key</strong> is invalid. <br /> Please contact us to get more information!
            </div>
        <?php endif; ?>

        <input type="text" required name="email" class="form-control" placeholder="Email Address"
               value="<?php echo CHtml::encode(Helper::request('email')); ?>"/>

        <button class="btn btn-lg btn-primary btn-block space10" type="submit">
            <i class="fa fa-envelope"></i>
            Send
        </button>

        <div class="small text-center">
            <i>Please enter your email address to receive instructions for retrieving password!</i>
        </div>
    <?php endif; ?>


    <div class="space10">
        <a href="<?php echo HelperUrl::baseUrl() . 'site/sign_in'; ?>">
            Go Back
        </a>
</form>
