<form id="signup-form" method="post" action="" autocomplete="off"
      onsubmit="javascript: return confirm('Are you sure to confirm your information?');">
    <h2 class="form-signin-heading">Create a new account</h2>

    <?php if (Helper::request('sent')): ?>
        <div class="alert alert-success text-success">
            <h4>Welcome to SoinCRM system</h4>

            <p>
                A verification email sent to your email. <br/>
                Please check your inbox to verify your information!
            </p>
        </div>
    <?php endif; ?>

    <?php if (!Helper::request('sent')): ?>

        <?php if (isset($form_result['status']) && $form_result['status'] == 'error'): ?>
            <?php echo $form_result['message']; ?>
        <?php endif; ?>

        <div class="form-group">
            <input type="text" required name="first_name" class="form-control" placeholder="First Name"
                   value="<?php echo CHtml::encode(Helper::request('first_name')); ?>"/>
        </div>

        <div class="form-group">
            <input type="text" required name="last_name" class="form-control" placeholder="Last Name"
                   value="<?php echo CHtml::encode(Helper::request('last_name')); ?>"/>
        </div>

        <div class="form-group">
            <input type="email" required name="email" class="form-control" placeholder="Email Address"
                   value="<?php echo CHtml::encode(Helper::request('email')); ?>"/>
        </div>

        <div class="form-group">
            <input type="text" required name="username" class="form-control" placeholder="Username"
                   value="<?php echo CHtml::encode(Helper::request('username')); ?>"/>
        </div>

        <div class="form-group">
            <input type="password" required name="password" class="form-control" placeholder="Password"
                   value="<?php echo CHtml::encode(Helper::request('password')); ?>"/>
        </div>

        <div class="form-group">
            <input type="password" required name="confirm_password" class="form-control" placeholder="Confirm Password"
                   value="<?php echo CHtml::encode(Helper::request('confirm_password')); ?>"/>
        </div>

        <button class="btn btn-lg btn-primary btn-block space10" type="submit">
            Register new account
        </button>

        <div class="small text-center">
            <i>Please enter correct information. We will send an email to verify your account.</i>
        </div>
    <?php endif; ?>

    <div class="space10">
        <a href="<?php echo HelperUrl::baseUrl() . 'site/sign_in'; ?>">
            Go Back
        </a>
</form>
