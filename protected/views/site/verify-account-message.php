<form id="login-form" method="post" action="" autocomplete="off" style="max-width: 500px">
    <h2 class="form-signin-heading">Register a new account</h2>

    <?php if (Helper::request('token') == 'expired'): ?>
        <div class="alert alert-danger text-center">
            <strong>Your verify code</strong> is expired. <br /> Please contact us to get more information!
        </div>
    <?php endif; ?>

    <?php if (Helper::request('token') == 'invalid'): ?>
        <div class="alert alert-danger text-center">
            <strong>Your verify key</strong> is invalid. <br /> Please contact us to get more information!
        </div>
    <?php endif; ?>

    <?php if (!Helper::request('token') || Helper::request('token') == 'success'): ?>
    <div class="alert alert-success">
        An email sent to <strong><?php echo CHtml::encode(Helper::request('email')) ?></strong>. <br/>
        Please check inbox of your email to get account information!
    </div>
    <?php endif ?>


    <div class="space10">
        <a href="<?php echo HelperUrl::baseUrl() . 'site/sign_in'; ?>">
            Go Back
        </a>
</form>
