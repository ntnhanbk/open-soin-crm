<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>site/change_handler" enctype="multipart/form-data" autocomplete="off">
    <div class="form-group">
        <label>Old Password</label>
        <input type="password" class="form-control" name="old_password">
    </div>
    <div class="form-group">
        <label>New Password</label>
        <input type="password" class="form-control" name="new_password">
    </div>
    <div class="form-group">
        <label>Confirm New Password</label>
        <input type="password" class="form-control" name="confirm_new_password">
    </div>
</form>