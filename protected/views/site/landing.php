<div id="inSlider" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>We craft<br/>
                        brands, web apps,<br/>
                        and user interfaces<br/>
                        we are IN+ studio</h1>

                    <p>Lorem Ipsum is simply dummy text of the printing.</p>

                    <p>
                        <a class="btn btn-lg btn-primary" href="#" role="button">READ MORE</a>
                        <a class="caption-link" href="#" role="button">Inspinia Theme</a>
                    </p>
                </div>
                <div class="carousel-image wow zoomIn">
                    <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/laptop.png" alt="laptop"/>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>

        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>We create meaningful <br/> interfaces that inspire.</h1>

                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>

                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<section id="features" class="container services">
    <div class="row">
        <div class="col-sm-3">
            <h2>Full responsive</h2>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus.</p>

            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>LESS/SASS Files</h2>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus.</p>

            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>6 Charts Library</h2>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus.</p>

            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Advanced Forms</h2>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus.</p>

            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
    </div>
</section>

<section class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Over 40+ unique view<br/> <span class="navy"> with many custom components</span></h1>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-mobile features-icon"></i>

                <h2>Full responsive</h2>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                    vehicula ut id elit. Morbi leo risus.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-bar-chart features-icon"></i>

                <h2>6 Charts Library</h2>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                    vehicula ut id elit. Morbi leo risus.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/perspective.png" alt="dashboard"
                 class="img-responsive">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-envelope features-icon"></i>

                <h2>Mail pages</h2>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                    vehicula ut id elit. Morbi leo risus.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-google features-icon"></i>

                <h2>AngularJS version</h2>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                    vehicula ut id elit. Morbi leo risus.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Discover great feautres</h1>

            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
        </div>
    </div>
    <div class="row features-block">
        <div class="col-lg-6 features-text wow fadeInLeft">
            <small>INSPINIA</small>
            <h2>Perfectly designed </h2>

            <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It
                has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
            <a href="" class="btn btn-primary">Learn more</a>
        </div>
        <div class="col-lg-6 text-right wow fadeInRight">
            <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/dashboard.png" alt="dashboard"
                 class="img-responsive pull-right">
        </div>
    </div>
</section>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Our Team</h1>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 wow fadeInLeft">
                <div class="team-member">
                    <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/avatar3.jpg"
                         class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Amelia</span> Smith</h4>

                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an
                        soluta sensibus. </p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member wow zoomIn">
                    <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/avatar1.jpg"
                         class="img-responsive img-circle" alt="">
                    <h4><span class="navy">John</span> Novak</h4>

                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an
                        soluta sensibus.</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
                <div class="team-member">
                    <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/avatar2.jpg"
                         class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Peter</span> Johnson</h4>

                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an
                        soluta sensibus.</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non
                    quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
            </div>
        </div>
    </div>
</section>

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Even more great feautres</h1>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-3 features-text wow fadeInLeft">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
            <div class="col-lg-6 text-right m-t-n-lg wow zoomIn">
                <img src="<?php echo HelperUrl::baseUrl() . 'assets/landing/' ?>img/iphone.jpg" class="img-responsive"
                     alt="dashboard">
            </div>
            <div class="col-lg-3 features-text text-right wow fadeInRight">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
        </div>
    </div>

</section>

<section id="testimonials" class="navy-section testimonials">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow zoomIn">
                <i class="fa fa-comment big-icon"></i>

                <h1>
                    What our users say
                </h1>

                <div class="testimonials-text">
                    <i>"Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model
                        text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
                        versions have evolved over the years, sometimes by accident, sometimes on purpose (injected
                        humour and the like)."</i>
                </div>
                <small>
                    <strong>12.02.2014 - Andy Smith</strong>
                </small>
            </div>
        </div>
    </div>

</section>
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>More and more extra great feautres</h1>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-bar-chart big-icon pull-right"></i>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with.</p>
            </div>
            <div class="col-lg-5 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-bolt big-icon pull-right"></i>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-clock-o big-icon pull-right"></i>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with.</p>
            </div>
            <div class="col-lg-5 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-users big-icon pull-right"></i>

                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully
                    responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.
                    It has a huge collection of reusable UI components and integrated with.</p>
            </div>
        </div>
    </div>

</section>
<section id="pricing" class="pricing">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>App Pricing</h1>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row">

            <?php foreach ($pricings as $pricing_key => $pricing_item): ?>
                <div class="col-lg-4 wow zoomIn">
                    <ul class="pricing-plan list-unstyled <?php echo $pricing_item['is_suggest'] ? 'selected' : '' ?>">
                        <li class="pricing-title">
                            <?php echo CHtml::encode($pricing_item['title']) ?>
                        </li>
                        <li class="pricing-desc">
                            <?php echo CHtml::encode($pricing_item['description']) ?>
                        </li>
                        <li class="pricing-price">
                            <span>$<?php echo floatval($pricing_item['amt']) ?></span>
                            / <?php echo $pricing_item['time'] . ' ' . $pricing_item['time_type'] ?>
                        </li>

                        <?php foreach ($pricing_item['features'] as $feature_key => $feature_item): ?>
                            <li>
                                <?php echo CHtml::encode($feature_item) ?>
                            </li>
                        <?php endforeach; ?>
                        <li>

                            <?php if ($pricing_item['is_payment']): ?>

                                <?php if (Yii::app()->user->isGuest): ?>
                                    <a class="btn btn-primary btn-xs"
                                       href="<?php echo HelperUrl::baseUrl() . 'site/sign_up' ?>">Sign Up</a>
                                <?php else: ?>

                                    <?php if ($pricing_item['amt'] > 0): ?>
                                        <button data-toggle="modal"
                                                data-target="#pricingModal<?php echo $pricing_key ?>"
                                                class="btn btn-primary btn-xs">Payment
                                        </button>
                                    <?php else: ?>
                                        <a class="btn btn-primary btn-xs"
                                           href="<?php echo HelperUrl::baseUrl() ?>">Dashboard</a>
                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php else: ?>
                                <a class="btn btn-primary btn-xs"
                                   href="<?php echo HelperUrl::baseUrl() . $pricing_item['url'] ?>">Contact Us</a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            <?php endforeach; ?>

        </div>
        <div class="row m-t-lg">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">
                <p>*Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model
                    text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. <span
                        class="navy">Various versions</span> have evolved over the years, sometimes by accident,
                    sometimes on purpose (injected humour and the like).</p>
            </div>
        </div>
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>

                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Company name, Inc.</span></strong><br/>
                    795 Folsom Ave, Suite 600<br/>
                    San Francisco, CA 94107<br/>
                    <abbr title="Phone">P:</abbr> (123) 456-7890
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis,
                    totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis,
                    totam corporis ea,
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:test@email.com" class="btn btn-primary">Send us mail</a>

                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; 2015 Company Name</strong><br/> consectetur adipisicing elit. Aut eaque, laboriosam
                    veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
            </div>
        </div>
    </div>
</section>

<!-- Modal for pricing -->

<?php foreach ($pricings as $pricing_key => $pricing_item): ?>

    <?php if ($pricing_item['is_payment']): ?>
        <div class="modal fade" id="pricingModal<?php echo $pricing_key ?>" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"
                            id="myModalLabel"><?php echo CHtml::encode($pricing_item['title']) ?></h4>
                    </div>
                    <div class="modal-body">
                        <h1 class="text-center">
                            $<?php echo CHtml::encode($pricing_item['amt']) ?> /
                            <?php echo CHtml::encode($pricing_item['time'] . ' ' . $pricing_item['time_type']) ?>
                        </h1>

                        <table class="table table-striped">
                            <tr>
                                <td width="40%"><strong>Package Name</strong></td>
                                <td><?php echo CHtml::encode($pricing_item['title']) ?></td>
                            </tr>
                            <tr>
                                <td><strong>Price</strong></td>
                                <td>$<?php echo $pricing_item['amt'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Currency</strong></td>
                                <td><?php echo $pricing_item['currency'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Number of Accounts</strong></td>
                                <td><?php echo $pricing_item['accounts'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Time</strong></td>
                                <td><?php echo CHtml::encode($pricing_item['time'] . ' ' . $pricing_item['time_type']) ?></td>
                            </tr>
                        </table>

                        <hr/>
                        <div class="text-center">
                            <a href="<?php echo HelperUrl::baseUrl() . 'site/payment/package/' . $pricing_key ?>">
                                <img src="<?php echo HelperUrl::baseUrl() . 'assets/img/' ?>paypal-button.png"
                                     style="max-width: 200px;"/>
                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
<!-- End Modal for pricing -->