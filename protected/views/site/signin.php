<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

    <h3>Welcome to <?php echo CHtml::encode(Yii::app()->name) ?></h3>
    <p>
        Customer Relationship Management
    </p>

<?php if ($model->hasErrors()): ?>
    <div class="alert alert-danger">
        <?php echo $form->error($model, 'username'); ?>
        <?php echo $form->error($model, 'password'); ?>
        <?php echo $form->error($model, 'rememberMe'); ?>
    </div>
<?php endif; ?>

    <input type="text" name="LoginForm[username]" class="form-control" placeholder="Username" required autofocus>
    <input type="password" name="LoginForm[password]" class="form-control" placeholder="Password" required>
    <input type="checkbox" value="1" id="LoginForm_rememberMe" name="LoginForm[rememberMe]" checked> Remember Me
    <button class="btn btn-primary btn-block space10" type="submit">Sign in</button>

    <a href="<?php echo HelperUrl::baseUrl() . 'site/forgot'; ?>"><small>Forgot password?</small></a>
    <p class="text-muted text-center"><small>Do not have an account?</small></p>
    <a class="btn btn-sm btn-white btn-block" href="<?php echo HelperUrl::baseUrl() . 'site/sign_up'; ?>">Create an account</a>

<?php $this->endWidget(); ?>