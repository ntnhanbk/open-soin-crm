<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <h1 class="error-title">404 - Page Not Found</h1>
        <p class="lead">The page you've requested could not be found on the server. Please contact your webmaster, or use the back button in your browser to navigate back to your most recent active page.</p>
        <a class="btn btn-primary" href="<?php echo HelperUrl::baseUrl();?>"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;&nbsp;&nbsp;Back to Dashboard</a>
    </div>
    <!-- /.col-lg-6 -->
</div>