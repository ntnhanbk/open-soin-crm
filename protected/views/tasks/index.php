<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Task Management</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo HelperUrl::baseUrl(); ?>">Dashboard</a></li>
            <li class="active">Tasks</li>
        </ol>
    </div>
</div>


<div class="container-fluid">
    <div class="clearfix space10">
        <div class="pull-left">
            <div class="btn-group">
                <a href="<?php echo HelperUrl::baseUrl(); ?>tasks/index/status/0" class="btn btn-info">New Tasks</a>
            </div>
            <div class="btn-group">
                <a href="<?php echo HelperUrl::baseUrl(); ?>tasks/index/status/1" class="btn btn-success">Completed
                    Tasks</a>
            </div>
            <div class="btn-group">
                <a href="<?php echo HelperUrl::baseUrl(); ?>tasks/index/status/-1" class="btn btn-default">Invalid
                    Tasks</a>
            </div>
        </div>
        <div class="pull-right">
            <div class="btn-group">
                <a href="" class="btn btn-danger btn-object-modal" modal-size="modal-lg"
                   modal-url="<?php echo HelperUrl::baseUrl(); ?>tasks/deadline"
                   modal-title="<i class='fa fa-calendar'></i> Update Deadline"><i class="fa fa-calendar"></i> Update
                    Deadline</a>
            </div>
            <div class="btn-group">
                <a href="" class="btn btn-primary btn-object-modal" modal-size=""
                   modal-url="<?php echo HelperUrl::baseUrl(); ?>tasks/add"
                   modal-title="<i class='fa fa-plus'></i> Add New Task"><i class="fa fa-plus"></i> Add New Task</a>
            </div>
        </div>
    </div>

    <div class="space10">&nbsp;</div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                <i class="fa fa-list-alt"></i>
                Tasks
            </h5>
        </div>
        <div class="ibox-content">
            <table class="table table-striped space10">
                <thead>
                <tr>
                    <th class="text-center" width="15%"><a
                            href="<?php echo HelperUrl::baseUrl() . "tasks/index/order_by/start_date/order_asc/$next_order_asc"; ?>">Start
                            Date</a></th>
                    <th class="text-center" width="15%"><a
                            href="<?php echo HelperUrl::baseUrl() . "tasks/index/order_by/deadline/order_asc/$next_order_asc"; ?>">Deadline</a>
                    </th>
                    <th><a href="<?php echo HelperUrl::baseUrl() . "tasks/index/order_by/title/order_asc/$next_order_asc"; ?>">Task
                            Name</a></th>
                    <th><a href="<?php echo HelperUrl::baseUrl() . "tasks/index/order_by/tags/order_asc/$next_order_asc"; ?>">Tags</a>
                    </th>
                    <th class="text-center" width="15%" colspan="5">&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                <?php if (!count($items)): ?>
                    <tr>
                        <td colspan="9">No record found!</td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($items as $k => $v): ?>
                    <tr>
                        <td class="text-center"><?php echo CHtml::encode(Helper::date($v['start_date'], 'M d, Y H:i a')) ?></td>
                        <td class="text-center"><?php echo CHtml::encode(Helper::date($v['deadline'], 'M d, Y H:i a')) ?></td>
                        <td>
                            <a class="btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'tasks/view/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <?php echo Helper::display_task_status($v['status']); ?>
                                <?php echo CHtml::encode($v['title']) ?>
                            </a>
                        </td>
                        <td><?php echo CHtml::encode($v['tags']) ?></td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-warning btn-sm btn-object-modal" modal-size=""
                               modal-url="<?php echo HelperUrl::baseUrl() . 'tasks/edit/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-edit'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td width="5%" class="text-center">
                            <a class="btn btn-danger btn-sm btn-object-modal" modal-size="modal-sm"
                               modal-url="<?php echo HelperUrl::baseUrl() . 'tasks/delete/id/' . $v['id'] ?>"
                               modal-title="<i class='fa fa-trash'></i> <?php echo CHtml::encode($v['title']) ?>">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>

    <?php $this->renderPartial('application.views._shared.paging', array(
        'paging' => $paging,
        'search' => true,
        'total' => $total
    )); ?>
</div>