<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2">
            <?php echo Helper::display_task_status($item['status']); ?>
            <?php echo CHtml::encode($item['title']) ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><strong>Tags</strong></td>
        <td><?php echo CHtml::encode($item['tags']) ?></td>
    </tr>
    <tr>
        <td width="40%"><strong>Start Date</strong></td>
        <td width="60%"><?php echo Helper::date($item['start_date']) ?></td>
    </tr>
    <tr>
        <td><strong>Deadline</strong></td>
        <td><?php echo Helper::date($item['deadline']) ?></td>
    </tr>
    <tr>
        <td><strong>Created Date</strong></td>
        <td><?php echo Helper::date($item['date_added']) ?></td>
    </tr>
    <tr>
        <td><strong>Created By</strong></td>
        <td><?php echo CHtml::encode($item['author_name']) ?></td>
    </tr>

    </tbody>
</table>