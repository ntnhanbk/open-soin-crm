<form method="post" action="<?php echo HelperUrl::baseUrl() . 'tasks/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="title" rows="5"><?php echo CHtml::encode($item['title']) ?></textarea>
    </div>
    <div class="form-group">
        <label>Tags</label>
        <input type="text" class="form-control" name="tags" value="<?php echo CHtml::encode($item['tags']) ?>">
    </div>
    <div class="form-group">
        <label>Start Date</label>
        <input type="text" class="form-control datepicker" name="start_date" value="<?php echo date('m/d/Y H:i',$item['start_date']) ?>">
    </div>
    <div class="form-group">
        <label>Deadline</label>
        <input type="text" class="form-control datepicker" name="deadline" value="<?php echo date('m/d/Y H:i',$item['deadline']) ?>">
    </div>
    <div class="form-group">
        <label>Status</label><br/>
        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_task_status($k);?> <?php echo ($item['status']==$k)?'active':''; ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>" <?php echo ($item['status']==$k)?'checked':''; ?>> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>