<form method="post" action="<?php echo HelperUrl::baseUrl() . 'quotes/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Request:</label>
        <?php echo CHtml::encode($item['request_title']) ?>
    </div>
    <div class="form-group">
        <label>Quote Name</label>
        <input type="text" class="form-control" name="title" placeholder=""
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-control" name="currency">
            <option value="0">-- Choose Currency --</option>
            <?php foreach ($currencies as $k => $v): ?>
                <option
                    value="<?php echo $k; ?>" <?php echo ($item['currency'] == $k) ? 'selected' : ''; ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control" name="amt" placeholder=""
               value="<?php echo CHtml::encode($item['amt']) ?>">
    </div>

    <div class="form-group">
        <label>Created Date</label>
        <input type="text" class="form-control datepicker" name="date_added" value="<?php echo date('m/d/Y H:i',$item['date_added']) ?>">
    </div>

    <div class="form-group">
        <label>Status</label><br/>
        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_quote_status($k);?> <?php echo ($item['status']==$k)?'active':''; ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>" <?php echo ($item['status']==$k)?'checked':''; ?>> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({format: 'mm/dd/yyyy hh:ii'});
    });
</script>