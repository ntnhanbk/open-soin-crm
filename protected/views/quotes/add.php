<form method="post" action="<?php echo HelperUrl::baseUrl(); ?>quotes/add_handler" enctype="multipart/form-data">
    <div class="form-group">
        <label>Request</label>

        <select class="form-control" name="request_id">
            <option value="0">-- Choose Request --</option>
            <?php foreach ($requests as $k => $v): ?>
                <option value="<?php echo $v['id']; ?>" <?php echo (isset($selected_request['id']) && $selected_request['id'] == $v['id']) ? 'selected' : ''; ?>><?php echo $v['title']; ?></option>
            <?php endforeach; ?>
        </select>

    </div>
    <div class="form-group">
        <label>Quote Name</label>
        <input type="text" class="form-control" name="title" placeholder="" value="<?php echo (isset($selected_request)) ? $selected_request['title'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-control" name="currency">
            <option value="0">-- Choose Currency --</option>
            <?php foreach ($currencies as $k => $v): ?>
                <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control" name="amt" placeholder="">
    </div>
    <div class="form-group">
        <label>Status</label><br/>

        <div class="btn-group" data-toggle="buttons">
            <?php foreach ($statuses as $k => $v): ?>
                <label class="btn btn-<?php echo Helper::label_quote_status($k); ?>">
                    <input type="radio" name="status" value="<?php echo $k; ?>"> <?php echo $v; ?>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
</form>