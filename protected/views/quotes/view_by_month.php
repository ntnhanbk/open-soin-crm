<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Quote Name</th>
        <th>Request Name</th>
        <th class="text-right">Amount</th>
        <th class="text-center" width="20%">Created</th>
    </tr>
    </thead>
    <tbody>

    <?php if (!count($items)): ?>
        <tr>
            <td colspan="4">No record found!</td>
        </tr>
    <?php endif; ?>

    <?php $sum=0; foreach ($items as $k => $v): $sum+=$v['amt'];?>
        <tr>
            <td>

                <a class="btn-object-modal" modal-size=""
                   modal-url="<?php echo HelperUrl::baseUrl() . 'quotes/view/id/' . $v['id'] ?>"
                   modal-title="<i class='fa fa-info'></i> <?php echo CHtml::encode($v['title']) ?>">
                    <?php echo Helper::display_quote_status($v['status']); ?>
                    <?php echo CHtml::encode($v['title']) ?>
                </a>

            </td>
            <td><?php echo CHtml::encode($v['request_title']) ?></td>
            <td class="text-right"><?php echo CHtml::encode($v['currency'] . ' ' . number_format($v['amt'], 2)) ?></td>
            <td class="text-center">
                <?php echo CHtml::encode(Helper::date($v['date_added'])) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tfooter>
        <tr>
            <td colspan="2" class="text-center"><strong>Total</strong></td>
            <td class="text-right"><strong>$ <?php echo number_format($sum);?></strong></td>
            <td></td>
        </tr>
    </tfooter>
    </tbody>
</table>