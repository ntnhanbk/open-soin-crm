<form method="post" action="<?php echo HelperUrl::baseUrl() . 'categories/edit_handler/id/' . $item['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label>Category Name</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Category Name"
               value="<?php echo CHtml::encode($item['title']) ?>">
    </div>
</form>