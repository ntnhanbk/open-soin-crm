<?php

class UserController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function actionSign_in() {
        $this->_checkAPI_token();

        $username = trim(Yii::app()->request->getParam('username'));
        $password = trim(Yii::app()->request->getParam('password'));

        /** Check input data: username */
        if (!$username || $username == '') {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('NoName'),
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        /** Check input data: password */
        if (!$password || $password == '') {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('EmptyPass'),
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        /** Find user */
        $UserModel = new UserModel();
        $user = $UserModel->get_by_username($username);

        /** Check valid user */
        if (!$user) {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('Illegal'),
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        /** Check blocked */
        if ($user['disabled']) {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('Blocked'),
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        /** Check password */
        if ($UserModel->validate_password($password, $user['password'], $user['secret_key']) == false) {
            $this->result = array(
                'status' => 0,
                'message' => $this->login_errors('WrongPass'),
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        /** SUCCESSFULLY */
        $TokenModel = new TokenModel();
        $token_key = Ultilities::random('sha1', 100);
        $expired_date = strtotime('+7 days');

        //$TokenModel->clean_api_keys($user['id']);
        $TokenModel->add(array(
            'token' => $token_key,
            'type' => 'api',
            'user_id' => $user['id'],
            'date_added' => time(),
            'expired_date' => $expired_date,
            'disabled' => 0,
            'deleted' => 0
        ));

        $this->result['status'] = 1;
        $this->result['message'] = "Login Successfully";
        $this->result['summary'] = array(
            'access_key' => $token_key,
            'expired_date' => date('Y-m-d H:i:s', $expired_date),
        );
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionSign_up() {
        $this->_checkAPI_token();

        $Validator = new FormValidator();
        $UserModel = new UserModel();

        $firstname = trim(Yii::app()->request->getParam('firstname'));
        $lastname = trim(Yii::app()->request->getParam('lastname'));
        $email = trim(Yii::app()->request->getParam('email'));
        $username = trim(Yii::app()->request->getParam('username'));
        $password = trim(Yii::app()->request->getParam('password'));

        /** Validate input data */
        if (!$firstname || $firstname == ''){
            $this->result = array(
                'status' => 0,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        if (!$lastname || $lastname == ''){
            $this->result = array(
                'status' => 0,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }


        if (!$Validator->is_email($email)){
            $this->result = array(
                'status' => -1,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }
        else if ($UserModel->get_by_email($email)){
            $this->result = array(
                'status' => -2,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }


        if (!$username || $username == ''){
            $this->result = array(
                'status' => 0,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }
        else if ($UserModel->get_by_username($username)){
            $this->result = array(
                'status' => -3,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        if (!$password || $password == ''){
            $this->result = array(
                'status' => 0,
                'message' => "",
                'content' => array(),
            );
            $this->_sendResponse(200, CJSON::encode($this->result));
        }

        $secret_key = Helper::generate_secret_key($username, $email);
        $encoded_password = crypt($password, $secret_key);

        $args = array(
            'username' => $username,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'display_name' => $firstname . ' ' . $lastname,
            'password' => $encoded_password,
            'role' => 'sub-admin',
            'secret_key' => $secret_key,
            'email' => $email,
            'img' => '',
            'thumbnail' => '',
            'date_added' => time(),
            'last_modified' => 0,
            'author_id' => 0,
            'disabled' => 0,
            'deleted' => 0
        );

        $user_id = $UserModel->add($args);
        Helper::add_log($user_id, Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "Register Successfully";
        $this->result['summary'] = array(
        );
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionValidate_access_key()
    {
        $this->_checkAPI_token();

        $access_key = trim(Yii::app()->request->getParam('access_key'));

        /** Find user */
        $TokenModel = new TokenModel();
        $token = $TokenModel->get_by_token($access_key);

        $status = 0;
        if ($token['expired_date'] > time())
            $status = 1;

        $this->result = array(
            'status' => $status,
            'message' => '',
            'content' => $token,
        );
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}