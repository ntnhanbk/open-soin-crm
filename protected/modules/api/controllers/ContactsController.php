<?php

class ContactsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $ContactModel = new ContactModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $ContactModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['phone'] = $v['phone'];
            $my_items[$k]['email'] = $v['email'];
            $my_items[$k]['skype'] = $v['skype'];
            $my_items[$k]['viber'] = $v['viber'];
            $my_items[$k]['whatsapp'] = $v['whatsapp'];
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $LeadModel = new LeadModel();
        $args['author_id'] = $this->_user['id'];
        $items = $LeadModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $ContactModel = new ContactModel();

        $contact = $ContactModel->get($id);

        $this->result['summary']['id'] = $contact['id'];
        $this->result['summary']['lead_id'] = $contact['lead_id'];
        $this->result['summary']['title'] = $contact['title'];
        $this->result['summary']['phone'] = $contact['phone'];
        $this->result['summary']['email'] = $contact['email'];
        $this->result['summary']['skype'] = $contact['skype'];
        $this->result['summary']['viber'] = $contact['viber'];
        $this->result['summary']['whatsapp'] = $contact['whatsapp'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $ContactModel = new ContactModel();

        $contact = $ContactModel->get($id);
        $args['lead_id'] = trim(Helper::request('lead_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['phone'] = trim(Helper::request('phone'));
        $args['email'] = trim(Helper::request('email'));
        $args['skype'] = trim(Helper::request('skype'));
        $args['viber'] = trim(Helper::request('viber'));
        $args['whatsapp'] = trim(Helper::request('whatsapp'));
        $args['last_modified'] = time();

        if ($contact) {
            $args['id'] = $id;
            $ContactModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $ContactModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $ContactModel = new ContactModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $ContactModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}