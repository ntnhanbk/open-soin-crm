<?php

class MilestonesController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status = 0)
    {
        $MilestoneModel = new MilestoneModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $MilestoneModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['order'] = ($v['order_title']) ? $v['order_title'] : "";
            $my_items[$k]['milestone'] = Helper::date($v['milestone']);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $OrderModel = new OrderModel();
        $args['author_id'] = $this->_user['id'];
        $items = $OrderModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $MilestoneModel = new MilestoneModel();
        $milestone = $MilestoneModel->get($id);

        $this->result['summary']['id'] = $milestone['id'];
        $this->result['summary']['order_id'] = $milestone['order_id'];
        $this->result['summary']['order'] = ($milestone['order_title']) ? $milestone['order_title'] : "";
        $this->result['summary']['title'] = $milestone['title'];
        $this->result['summary']['milestone'] = date('Y-m-d\TH:i:sO', ($id) ? $milestone['milestone'] : time());
        $this->result['summary']['milestone_display'] = Helper::date($milestone['milestone']);
        $this->result['summary']['status'] = $milestone['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $MilestoneModel = new MilestoneModel();

        $milestone = $MilestoneModel->get($id);
        $args['order_id'] = trim(Helper::request('order_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['milestone'] = strtotime(Helper::request('milestone'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($milestone) {
            $args['id'] = $id;
            $MilestoneModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $MilestoneModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $MilestoneModel = new MilestoneModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $MilestoneModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}