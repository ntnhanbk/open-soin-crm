<?php

class DashboardController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe()
    {
        $author_id = $this->_user['id'];

        $OrderModel = new OrderModel();
        $InvoiceModel = new InvoiceModel();
        $ExpenseModel = new ExpenseModel();

        $start_date = strtotime(date('1 M Y') . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);
        $sum = $OrderModel->sum(array('created_from' => $start_date, 'created_to' => $end_date,'author_id'=>$author_id));
        $summary['current_orders'] = '$'.number_format(($sum) ? $sum : 0);

        $sum = $InvoiceModel->sum(array('created_from' => $start_date, 'created_to' => $end_date,'author_id'=>$author_id));
        $summary['current_invoices'] = '$'.number_format(($sum) ? $sum : 0);

        $sum = $ExpenseModel->sum(array('created_from' => $start_date, 'created_to' => $end_date,'author_id'=>$author_id));
        $summary['current_expenses']= '$'.number_format(($sum) ? $sum : 0);

        $my_items = array();
        $sum_orders=0;
        $sum_invoices=0;
        $sum_expenses=0;
        $base_start_date = strtotime(date('1 M Y') . " 00:00:00");
        for ($i = 0; $i <= 23; $i++) {
            $from = strtotime('-' . $i . ' month', $base_start_date);
            $to = Helper::get_end_date_of_month($from);

            $my_items[$i]['title'] = date('M Y', $from);

            $sum = $OrderModel->sum(array('created_from' => $from, 'created_to' => $to,'author_id'=>$author_id));
            $sum_orders+=$sum;
            $my_items[$i]['orders'] = '$'.number_format(($sum) ? round($sum, 2) : 0);

            $sum = $InvoiceModel->sum(array('created_from' => $from, 'created_to' => $to,'author_id'=>$author_id));
            $sum_invoices+=$sum;
            $my_items[$i]['invoices']= '$'.number_format(($sum) ? round($sum, 2) : 0);

            $sum = $ExpenseModel->sum(array('created_from' => $from, 'created_to' => $to,'author_id'=>$author_id));
            $sum_expenses+=$sum;
            $my_items[$i]['expenses'] = '$'.number_format(($sum) ? round($sum, 2) : 0);
        }
        $summary['sum_orders']='$'.number_format($sum_orders);;
        $summary['sum_invoices']='$'.number_format($sum_invoices);;
        $summary['sum_expenses']='$'.number_format($sum_expenses);;

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->result['summary'] = $summary;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}