<?php

class CampaignsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe()
    {
        $CampaignModel = new CampaignModel();
        $args['author_id'] = $this->_user['id'];
        $items = $CampaignModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['created_date'] = Helper::date($v['date_added']);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $CampaignModel = new CampaignModel();

        $campaign = $CampaignModel->get($id);

        $this->result['summary']['id'] = $campaign['id'];
        $this->result['summary']['title'] = $campaign['title'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = '';
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $CampaignModel = new CampaignModel();

        $campaign = $CampaignModel->get($id);
        $args['title'] = trim(Helper::request('title'));
        $args['last_modified'] = time();

        if ($campaign) {
            $args['id'] = $id;
            $CampaignModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $CampaignModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $CampaignModel = new CampaignModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $CampaignModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}