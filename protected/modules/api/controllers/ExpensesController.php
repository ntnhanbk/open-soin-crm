<?php

class ExpensesController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status = 0)
    {
        $ExpenseModel = new ExpenseModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $ExpenseModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['order'] = ($v['order_title']) ? $v['order_title'] : "";
            $my_items[$k]['receiver'] = $v['receiver_title'];
            $my_items[$k]['amt'] = '$' . number_format($v['amt'], 2);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $OrderModel = new OrderModel();
        $args['author_id'] = $this->_user['id'];
        $items = $OrderModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $ExpenseModel = new ExpenseModel();
        $expense = $ExpenseModel->get($id);

        $this->result['summary']['id'] = $expense['id'];
        $this->result['summary']['order_id'] = $expense['order_id'];
        $this->result['summary']['order'] = $expense['order_id'];
        $this->result['summary']['title'] = ($expense['order_title']) ? $expense['order_title'] : "";
        $this->result['summary']['currency'] = $expense['currency'];
        $this->result['summary']['amt'] = $expense['amt'];
        $this->result['summary']['amt_display'] = '$' . number_format($expense['amt'], 2);
        $this->result['summary']['receiver'] = $expense['receiver'];
        $this->result['summary']['status'] = $expense['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $ExpenseModel = new ExpenseModel();

        $expense = $ExpenseModel->get($id);
        $args['order_id'] = trim(Helper::request('order_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['currency'] = trim(Helper::request('currency'));
        $args['amt'] = trim(Helper::request('amt'));
        $args['receiver'] = trim(Helper::request('receiver'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($expense) {
            $args['id'] = $id;
            $ExpenseModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $ExpenseModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $ExpenseModel = new ExpenseModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $ExpenseModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}