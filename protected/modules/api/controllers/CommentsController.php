<?php

class CommentsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionList()
    {
        $CommentModel = new CommentModel();
        $args['object_id'] = trim(Helper::request('object_id'));
        $args['object_type'] = trim(Helper::request('object_type'));
        $items = $CommentModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['comment'];
            $my_items[$k]['author'] = $v['author_name'];
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->result['summary']['object_id'] = $args['object_id'];
        $this->result['summary']['object_type'] = $args['object_type'];
        $this->result['summary']['count'] = count($items);
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $CommentModel = new CommentModel();

        $comment = $CommentModel->get($id);

        $this->result['summary']['id'] = $comment['id'];
        $this->result['summary']['title'] = $comment['comment'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = '';
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $CommentModel = new CommentModel();

        $comment = $CommentModel->get($id);
        $args['comment'] = trim(Helper::request('title'));
        $args['object_id'] = trim(Helper::request('object_id'));
        $args['object_type'] = trim(Helper::request('object_type'));
        $args['last_modified'] = time();

        if ($comment) {
            $args['id'] = $id;
            $CommentModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $CommentModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $CommentModel = new CommentModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $CommentModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}