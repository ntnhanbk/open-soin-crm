<?php

class OrdersController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $OrderModel = new OrderModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $OrderModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['deal'] = ($v['deal_title']) ? $v['deal_title'] : "";
            $my_items[$k]['amt'] = '$'.number_format($v['amt'],2);
            $my_items[$k]['total_invoices'] = '$'.number_format($v['total_invoices'],2);
            $my_items[$k]['total_expenses'] = '$'.number_format($v['total_expenses'],2);
            $my_items[$k]['profit'] = '$'.number_format($v['total_invoices']-$v['total_expenses'],2);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $DealModel = new DealModel();
        $args['author_id'] = $this->_user['id'];
        $items = $DealModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $OrderModel = new OrderModel();
        $order = $OrderModel->get($id);

        $this->result['summary']['id'] = $order['id'];
        $this->result['summary']['deal_id'] = $order['deal_id'];
        $this->result['summary']['deal'] = ($order['deal_title']) ? $order['deal_title'] : "";
        $this->result['summary']['title'] = $order['title'];
        $this->result['summary']['currency'] = $order['currency'];
        $this->result['summary']['amt'] = $order['amt'];
        $this->result['summary']['amt_display'] = '$'.number_format($order['amt'],2);

        $this->result['summary']['total_invoices'] = '$'.number_format($order['total_invoices'],2);
        $this->result['summary']['total_expenses'] = '$'.number_format($order['total_expenses'],2);
        $this->result['summary']['profit'] = '$'.number_format($order['total_invoices']-$order['total_expenses'],2);

        $this->result['summary']['status'] = $order['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $OrderModel = new OrderModel();

        $order = $OrderModel->get($id);
        $args['deal_id'] = trim(Helper::request('deal_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['currency'] = trim(Helper::request('currency'));
        $args['amt'] = trim(Helper::request('amt'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($order) {
            $args['id'] = $id;
            $OrderModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $OrderModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $OrderModel = new OrderModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $OrderModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}