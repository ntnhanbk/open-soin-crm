<?php

class InvoicesController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $InvoiceModel = new InvoiceModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $InvoiceModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['order'] = ($v['order_title']) ? $v['order_title'] : "";
            $my_items[$k]['amt'] ='$'.number_format($v['amt'],2);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }


    public function actionGet($id = 0)
    {
        $OrderModel = new OrderModel();
        $args['author_id'] = $this->_user['id'];
        $items = $OrderModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $InvoiceModel = new InvoiceModel();
        $invoice = $InvoiceModel->get($id);

        $this->result['summary']['id'] = $invoice['id'];
        $this->result['summary']['order_id'] = $invoice['order_id'];
        $this->result['summary']['order'] = ($invoice['order_title']) ? $invoice['order_title'] : "";
        $this->result['summary']['title'] = $invoice['title'];
        $this->result['summary']['currency'] = $invoice['currency'];
        $this->result['summary']['amt'] = $invoice['amt'];
        $this->result['summary']['amt_display'] = '$'.number_format($invoice['amt'],2);
        $this->result['summary']['status'] = $invoice['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $InvoiceModel = new InvoiceModel();

        $invoice = $InvoiceModel->get($id);
        $args['order_id'] = trim(Helper::request('order_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['currency'] = trim(Helper::request('currency'));
        $args['amt'] = trim(Helper::request('amt'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($invoice) {
            $args['id'] = $id;
            $InvoiceModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $InvoiceModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $InvoiceModel = new InvoiceModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $InvoiceModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}