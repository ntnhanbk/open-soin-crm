<?php

class ReportsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionThis_month()
    {
        $author_id = $this->_user['id'];

        $OrderModel = new OrderModel();
        $InvoiceModel = new InvoiceModel();
        $ExpenseModel = new ExpenseModel();

        $my_items = array();

        $start_date = strtotime(date('1 M Y') . " 00:00:00");
        $end_date = Helper::get_end_date_of_month($start_date);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->result['summary']['sumorders'] = number_format($OrderModel->sum(array('created_from' => $start_date, 'created_to' => $end_date),$author_id));
        $this->result['summary']['suminvoices'] =number_format($InvoiceModel->sum(array('created_from' => $start_date, 'created_to' => $end_date),$author_id));
        $this->result['summary']['sumexpenses'] = number_format($ExpenseModel->sum(array('created_from' => $start_date, 'created_to' => $end_date),$author_id));
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

}