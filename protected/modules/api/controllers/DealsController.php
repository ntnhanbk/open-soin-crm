<?php

class DealsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $DealModel = new DealModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $DealModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['quote'] = ($v['quote_title']) ? $v['quote_title'] : "";
            $my_items[$k]['amt'] = '$'.number_format($v['amt'],2);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $QuoteModel = new QuoteModel();
        $args['author_id'] = $this->_user['id'];
        $items = $QuoteModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $DealModel = new DealModel();
        $deal = $DealModel->get($id);

        $this->result['summary']['id'] = $deal['id'];
        $this->result['summary']['quote_id'] = $deal['quote_id'];
        $this->result['summary']['quote'] = ($deal['quote_title']) ? $deal['quote_title'] : "";
        $this->result['summary']['title'] = $deal['title'];
        $this->result['summary']['currency'] = $deal['currency'];
        $this->result['summary']['amt'] = $deal['amt'];
        $this->result['summary']['amt_display'] = '$'.number_format($deal['amt'],2);
        $this->result['summary']['status'] = $deal['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $DealModel = new DealModel();

        $deal = $DealModel->get($id);
        $args['quote_id'] = trim(Helper::request('quote_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['currency'] = trim(Helper::request('currency'));
        $args['amt'] = trim(Helper::request('amt'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($deal) {
            $args['id'] = $id;
            $DealModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $DealModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $DealModel = new DealModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $DealModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}