<?php

class TasksController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */
    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $TaskModel = new TaskModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $TaskModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['tags'] = $v['tags'];
            $my_items[$k]['start_date'] =Helper::date($v['start_date'],'d/m/y');
            $my_items[$k]['end_date'] =Helper::date($v['deadline'],'d/m/y');
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $TaskModel = new TaskModel();

        $task = $TaskModel->get($id);

        $this->result['summary']['id'] = $task['id'];
        $this->result['summary']['title'] = $task['title'];
        $this->result['summary']['tags'] = $task['tags'];
        $this->result['summary']['start_date_display'] = Helper::date($task['start_date'],'d/m/y');
        $this->result['summary']['deadline_display'] = Helper::date($task['deadline'],'d/m/y');
        $this->result['summary']['start_date'] = date('Y-m-d\TH:i:sO', $task['start_date']);
        $this->result['summary']['deadline'] = date('Y-m-d\TH:i:sO', $task['deadline']);
        $this->result['summary']['status'] = $task['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = '';
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $TaskModel = new TaskModel();

        $task = $TaskModel->get($id);
        $args['title'] = trim(Helper::request('title'));
        $args['tags'] = trim(Helper::request('tags'));
        $args['start_date'] = strtotime(Helper::request('start_date'));
        $args['deadline'] = strtotime(Helper::request('deadline'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();


        if ($task) {
            $args['id'] = $id;
            $TaskModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $TaskModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $TaskModel = new TaskModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $TaskModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}
