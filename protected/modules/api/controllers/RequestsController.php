<?php

class RequestsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status = 0)
    {
        $RequestModel = new RequestModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $RequestModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['contact'] = ($v['contact_title']) ? $v['contact_title'] : "";
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $ContactModel = new ContactModel();
        $args['author_id'] = $this->_user['id'];
        $items = $ContactModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $RequestModel = new RequestModel();
        $request = $RequestModel->get($id);

        $this->result['summary']['id'] = $request['id'];
        $this->result['summary']['contact'] =  ($request['contact_title']) ? $request['contact_title'] : "";
        $this->result['summary']['contact_id'] = $request['contact_id'];
        $this->result['summary']['title'] = $request['title'];
        $this->result['summary']['description'] = $request['description'];
        $this->result['summary']['status'] = $request['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $RequestModel = new RequestModel();

        $request = $RequestModel->get($id);
        $args['contact_id'] = trim(Helper::request('contact_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['description'] = trim(Helper::request('description'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($request) {
            $args['id'] = $id;
            $RequestModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $RequestModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $RequestModel = new RequestModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $RequestModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}