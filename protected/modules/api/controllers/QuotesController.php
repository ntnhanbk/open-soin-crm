<?php

class QuotesController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init() {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status=0)
    {
        $QuoteModel = new QuoteModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $QuoteModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['request'] = ($v['request_title']) ? $v['request_title'] : "";
            $my_items[$k]['amt'] = '$'.number_format($v['amt'],2);
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $RequestModel = new RequestModel();
        $args['author_id'] = $this->_user['id'];
        $items = $RequestModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $QuoteModel = new QuoteModel();
        $quote = $QuoteModel->get($id);

        $this->result['summary']['id'] = $quote['id'];
        $this->result['summary']['request_id'] = $quote['request_id'];
        $this->result['summary']['request'] = ($quote['request_title']) ? $quote['request_title'] : "";
        $this->result['summary']['title'] = $quote['title'];
        $this->result['summary']['currency'] = $quote['currency'];
        $this->result['summary']['amt'] = $quote['amt'];
        $this->result['summary']['amt_display'] = '$'.number_format($quote['amt'],2);
        $this->result['summary']['status'] = $quote['status'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $QuoteModel = new QuoteModel();

        $quote = $QuoteModel->get($id);
        $args['request_id'] = trim(Helper::request('request_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['currency'] = trim(Helper::request('currency'));
        $args['amt'] = trim(Helper::request('amt'));
        $args['status'] = trim(Helper::request('status'));
        $args['last_modified'] = time();

        if ($quote) {
            $args['id'] = $id;
            $QuoteModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $QuoteModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $QuoteModel = new QuoteModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $QuoteModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}