<?php

class LeadsController extends ApiController
{
    /**
     * Document: http://www.mediawiki.org/wiki/API:Login
     */

    public function init()
    {
        $this->_checkAPI_token();
        $this->_checkAuth_by_token();
    }

    public function actionMe($status = 0)
    {
        $LeadModel = new LeadModel();
        $args['status'] = $status;
        $args['author_id'] = $this->_user['id'];
        $items = $LeadModel->gets($args, 0);

        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['campaign'] = ($v['campaign_title']) ? $v['campaign_title'] : "";
            $my_items[$k]['title'] = $v['title'];
            $my_items[$k]['phone'] = $v['phone'];
            $my_items[$k]['email'] = $v['email'];
        }

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionGet($id = 0)
    {
        $CampaignModel = new CampaignModel();
        $args['author_id'] = $this->_user['id'];
        $items = $CampaignModel->gets($args, 0);
        $my_items = array();
        foreach ($items as $k => $v) {
            $my_items[$k]['id'] = $v['id'];
            $my_items[$k]['title'] = $v['title'];
        }

        $LeadModel = new LeadModel();
        $lead = $LeadModel->get($id);

        $this->result['summary']['id'] = $lead['id'];
        $this->result['summary']['campaign'] = ($lead['campaign_title']) ? $lead['campaign_title'] : "";
        $this->result['summary']['campaign_id'] = $lead['campaign_id'];
        $this->result['summary']['title'] = $lead['title'];
        $this->result['summary']['phone'] = $lead['phone'];
        $this->result['summary']['email'] = $lead['email'];

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['content'] = $my_items;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionUpdate($id = 0)
    {
        $LeadModel = new LeadModel();

        $lead = $LeadModel->get($id);
        $args['campaign_id'] = trim(Helper::request('campaign_id'));
        $args['title'] = trim(Helper::request('title'));
        $args['phone'] = trim(Helper::request('phone'));
        $args['email'] = trim(Helper::request('email'));
        $args['last_modified'] = time();

        if ($lead) {
            $args['id'] = $id;
            $LeadModel->update($args);
        } else {
            $args['date_added'] = time();
            $args['author_id'] = $this->_user['id'];
            $id = $LeadModel->add($args);
        }

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }

    public function actionDelete($id = 0)
    {
        $LeadModel = new LeadModel();

        $args['id'] = $id;
        $args['deleted'] = 1;
        $args['last_modified'] = time();
        $LeadModel->update($args);

        Helper::add_log($this->_user['id'], Yii::app()->controller->id, Yii::app()->controller->action->id, $args);

        $this->result['status'] = 1;
        $this->result['message'] = "OK";
        $this->result['item_id'] = $id;
        $this->_sendResponse(200, CJSON::encode($this->result));
    }
}