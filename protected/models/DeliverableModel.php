<?php

class DeliverableModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND ta.title like :title";

        if (isset($args['request_id']))
            $where .= " AND ta.request_id = :request_id";

        if (isset($args['parent_id']))
            $where .= " AND ta.parent_id = :parent_id";

        if (isset($args['status']))
            $where .= " AND ta.status = :status";

        if (isset($args['status_in']) && is_array($args['status_in'])) {
            $status_in = "('" . implode("','", $args['status_in']) . "')";
            $where .= " AND ta.status IN " . $status_in;
        }
        unset($args['status_in']);

        if (isset($args['deadline']))
            $where .= " AND ta.deadline = :deadline";

        if (isset($args['min_deadline']))
            $where .= " AND ta.deadline >= :min_deadline";

        if (isset($args['max_deadline']))
            $where .= " AND ta.deadline <= :max_deadline";

        if (isset($args['user_id']))
            $where .= " AND ta.user_id = :user_id";

        if (isset($args['user_id_not']))
            $where .= " AND ta.user_id != :user_id_not";

        if (isset($args['in_request']))
            $where .= " AND ta.in_request = :in_request";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':title', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['request_id']))
            $params[] = array('name' => ':request_id', 'value' => "$args[request_id]", 'type' => PDO::PARAM_INT);

        if (isset($args['parent_id'])) {
            $params[] = array('name' => ':parent_id', 'value' => "$args[parent_id]", 'type' => PDO::PARAM_INT);
        }

        if (isset($args['status']))
            $params[] = array('name' => ':status', 'value' => "$args[status]", 'type' => PDO::PARAM_INT);

        if (isset($args['author_id']))
            $params[] = array('name' => ':author_id', 'value' => "$args[author_id]", 'type' => PDO::PARAM_INT);
        else
            $params[] = array('name' => ':author_id', 'value' => Yii::app()->user->id, 'type' => PDO::PARAM_INT);

        if (isset($args['deadline']))
            $params[] = array('name' => ':deadline', 'value' => "$args[deadline]", 'type' => PDO::PARAM_INT);

        if (isset($args['min_deadline']))
            $params[] = array('name' => ':min_deadline', 'value' => "$args[min_deadline]", 'type' => PDO::PARAM_INT);

        if (isset($args['max_deadline']))
            $params[] = array('name' => ':max_deadline', 'value' => "$args[max_deadline]", 'type' => PDO::PARAM_INT);

        if (isset($args['user_id'])) {
            $params[] = array('name' => ':user_id', 'value' => "$args[user_id]", 'type' => PDO::PARAM_INT);
        }
        if (isset($args['user_id_not'])) {
            $params[] = array('name' => ':user_id_not', 'value' => "$args[user_id_not]", 'type' => PDO::PARAM_INT);
        }

        if (isset($args['in_request'])) {
            $params[] = array('name' => ':in_request', 'value' => "$args[in_request]", 'type' => PDO::PARAM_INT);
        }

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " ta.sort_order ASC, ta.title ASC, ta.date_added ASC";
        }

        $filter_by_author = "AND (ta.author_id=:author_id)";


        if (isset($args['count_sub_deliverables'])) {
            $custom .= " ANd (SELECT count(distinct st2.id) FROM {{deliverables}} st2 WHERE st2.parent_id = ta.id AND ta.deleted=0) = :count_sub_deliverables";
            $params[] = array('name' => ':count_sub_deliverables', 'value' => "$args[count_sub_deliverables]", 'type' => PDO::PARAM_INT);
        }

        $sql = "SELECT ta.*, re.title as request_title,
                       (SELECT count(distinct st.id) FROM {{deliverables}} st WHERE st.parent_id = ta.id AND ta.deleted=0) as count_sub_deliverables,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.deleted=0) as count_all_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status!=-1 AND co.deleted=0) as count_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status=1 AND co.deleted=0) as count_resolved_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status=2 AND co.deleted=0) as count_completed_comments
                FROM {{deliverables}} ta
                LEFT JOIN {{requests}} re ON re.id = ta.request_id
                WHERE ta.deleted=0 $filter_by_author
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $filter_by_author = "AND (ta.author_id=:author_id)";

        $sql = "SELECT count(*) as total
                FROM {{deliverables}} ta
                WHERE ta.deleted = 0 $filter_by_author
                $custom";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id, $author_id = 0)
    {
        $filter_by_author = "AND (ta.author_id=:author_id OR :author_id)";

        $sql = "SELECT ta.*, usr.display_name author_name, re.title as request_title,
                       (SELECT count(st.id) FROM {{deliverables}} st WHERE st.parent_id = ta.id) as count_sub_deliverables,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable') as count_all_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status!=-1) as count_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status=1) as count_resolved_comments,
                       (SELECT count(co.id) FROM {{comments}} co WHERE co.object_id= ta.id AND co.object_type = 'deliverable' AND co.status=2) as count_completed_comments
                FROM {{deliverables}} ta
                LEFT JOIN {{requests}} re ON re.id = ta.request_id
                LEFT JOIN {{users}} usr On usr.id = ta.author_id
                WHERE ta.id = :id AND ta.deleted = 0 $filter_by_author";
        $command = Yii::app()->db->createCommand($sql);

        if ($author_id == 0)
            $author_id = Yii::app()->user->id;
        $command->bindParam(":author_id", $author_id);

        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{deliverables}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{deliverables}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }

    public function get_task_updates($user_id)
    {
        $sql = "SELECT *
                FROM {{task_updates}} tu
                WHERE tu.user_id=:user_id";

        $command = Yii::app()->db->createCommand($sql);

        $command->bindParam(":user_id", $user_id);

        return $command->queryAll();
    }

    public function add_task_update($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{task_updates}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }
}