<?php

class UserModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND username like :username";

        if (isset($args['role']))
            $where .= " AND role=:role";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':username', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['role']))
            $params[] = array('name' => ':role', 'value' => "$args[role]", 'type' => PDO::PARAM_STR);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT us.*
                FROM {{users}} us
                WHERE us.deleted=0
                $custom
                ORDER BY username ASC";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{users}} us
                WHERE us.deleted = 0
                $custom";
        $command = Yii::app()->db->createCommand($sql);
        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT us.*
                FROM {{users}} us
                WHERE us.id = :id AND us.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function get_by_secret_key($secret_key)
    {
        $sql = "SELECT *
                FROM {{users}}
                WHERE secret_key = :secret_key
                AND deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":secret_key", $secret_key);
        return $command->queryRow();
    }

    public function get_by_username($username)
    {
        $sql = "SELECT *
                FROM {{users}}
                WHERE username = :username
                AND deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":username", $username);
        return $command->queryRow();
    }

    public function get_by_email($email)
    {
        $sql = "SELECT *
                FROM {{users}}
                WHERE email = :email
                AND deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":email", $email);
        return $command->queryRow();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{users}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{users}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }

    public function encode_password($password, $secret_key = '') {
        return crypt($password, $secret_key);
    }

    public function validate_password($new_password, $old_password, $secret_key = '') {
        $encoded_new_password = $this->encode_password($new_password, $secret_key);
        return $encoded_new_password === $old_password;
    }
}