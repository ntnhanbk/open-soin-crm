<?php

class MilestoneModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND (ms.title like :title OR od.title like :title)";

        $where .= " AND ms.author_id=:author_id";

        if (isset($args['order_id']))
            $where .= " AND ms.order_id=:order_id";

        if (isset($args['status']))
            $where .= " AND ms.status=:status";

        if (isset($args['created_from']))
            $where .= " AND ms.date_added >= :created_from";
        if (isset($args['created_to']))
            $where .= " AND ms.date_added <= :created_to";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':title', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['order_id']))
            $params[] = array('name' => ':order_id', 'value' => "$args[order_id]", 'type' => PDO::PARAM_INT);

        if (isset($args['status']))
            $params[] = array('name' => ':status', 'value' => "$args[status]", 'type' => PDO::PARAM_INT);

        if (isset($args['created_from']))
            $params[] = array('name' => ':created_from', 'value' => "$args[created_from]", 'type' => PDO::PARAM_INT);
        if (isset($args['created_to']))
            $params[] = array('name' => ':created_to', 'value' => "$args[created_to]", 'type' => PDO::PARAM_INT);

        if (isset($args['author_id']))
            $params[] = array('name' => ':author_id', 'value' => "$args[author_id]", 'type' => PDO::PARAM_INT);
        else
            $params[] = array('name' => ':author_id', 'value' => Yii::app()->user->id, 'type' => PDO::PARAM_INT);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " ms.milestone ASC";
        }

        $sql = "SELECT ms.*, od.title order_title
                FROM {{milestones}} ms
                LEFT JOIN {{orders}} od On od.id = ms.order_id
                WHERE ms.deleted=0
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{milestones}} ms
                LEFT JOIN {{orders}} od On od.id = ms.order_id
                WHERE ms.deleted=0
                $custom";

        $command = Yii::app()->db->createCommand($sql);

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT ms.*, usr.display_name author_name, od.title order_title
                FROM {{milestones}} ms
                LEFT JOIN {{users}} usr On usr.id = ms.author_id
                LEFT JOIN {{orders}} od On od.id = ms.order_id
                WHERE ms.id = :id AND ms.deleted = 0";

        $command = Yii::app()->db->createCommand($sql);

        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{milestones}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{milestones}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }
}