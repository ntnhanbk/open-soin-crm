<?php

class TokenModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND tok.token like :token";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':token', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['deleted']))
            $params[] = array('name' => ':deleted', 'value' => "$args[deleted]", 'type' => PDO::PARAM_INT);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " tok.date_added DESC";
        }

        $sql = "SELECT *
                FROM {{tokens}} tok
                WHERE tok.deleted=0
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{tokens}} tok
                WHERE tok.deleted = 0
                $custom";
        $command = Yii::app()->db->createCommand($sql);
        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT tok.*, usr.display_name author_name
                FROM {{tokens}} tok
                LEFT JOIN {{users}} usr On usr.id = tok.user_id
                WHERE tok.id = :id AND tok.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":id", $id);
        return $command->queryRow();
    }


    public function get_by_token($token)
    {
        $sql = "SELECT tok.*, usr.display_name author_name
                FROM {{tokens}} tok
                LEFT JOIN {{users}} usr On usr.id = tok.user_id
                WHERE tok.token = :token AND tok.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":token", $token);
        return $command->queryRow();
    }

    public function delete_all_tokens($user_id, $type) {
        $sql = "UPDATE {{tokens}}
                SET deleted = 1
                WHERE user_id = :user_id
                AND type = :type";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":user_id", $user_id);
        $command->bindParam(":type", $type);
        return $command->execute();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{tokens}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{tokens}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }

    public function clean_api_keys($user_id) {
        $sql = "Update {{tokens}}
                set deleted = 1
                Where user_id = :user_id
                And type = 'api'";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $command->execute();
    }
}