<?php

class PaymentModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND pri.title like :title";

        if (isset($args['deleted']))
            $where .= " AND pri.deleted=:deleted";

        if (isset($args['author_id']))
            $where .= " AND pri.author_id=:author_id";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':title', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['deleted']))
            $params[] = array('name' => ':deleted', 'value' => "$args[deleted]", 'type' => PDO::PARAM_INT);

        if (isset($args['author_id']))
            $params[] = array('name' => ':author_id', 'value' => "$args[author_id]", 'type' => PDO::PARAM_INT);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " pri.title ASC";
        }

        $sql = "SELECT pri.*, us.username
                FROM {{pricing_payments}} pri
                LEFT JOIN {{users}} us ON us.id = pri.author_id
                WHERE pri.deleted=0
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{pricing_payments}} pri
                WHERE pri.deleted = 0
                $custom";
        $command = Yii::app()->db->createCommand($sql);
        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT pri.*, usr.display_name author_name
                FROM {{pricing_payments}} pri
                LEFT JOIN {{users}} usr On usr.id = pri.author_id
                WHERE pri.id = :id AND pri.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function get_by_token($id)
    {
        $sql = "SELECT pri.*, usr.display_name author_name
                FROM {{pricing_payments}} pri
                LEFT JOIN {{users}} usr On usr.id = pri.author_id
                WHERE pri.token_key = :id AND pri.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{pricing_payments}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{pricing_payments}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }
}