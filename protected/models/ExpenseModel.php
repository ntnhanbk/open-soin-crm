<?php

class ExpenseModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND ep.title like :title";

            $where .= " AND ep.author_id=:author_id";

        if (isset($args['receiver']) && $args['receiver'] != "")
            $where .= " AND ep.receiver LIKE :receiver";

        if (isset($args['order_id']))
            $where .= " AND ep.order_id=:order_id";

        if (isset($args['status']))
            $where .= " AND ep.status=:status";

        if (isset($args['created_from']))
            $where .= " AND ep.date_added >= :created_from";
        if (isset($args['created_to']))
            $where .= " AND ep.date_added <= :created_to";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':title', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        if (isset($args['receiver']) && $args['receiver'] != "")
            $params[] = array('name' => ':receiver', 'value' => "$args[receiver]", 'type' => PDO::PARAM_STR);

        if (isset($args['order_id']))
            $params[] = array('name' => ':order_id', 'value' => "$args[order_id]", 'type' => PDO::PARAM_INT);

        if (isset($args['status']))
            $params[] = array('name' => ':status', 'value' => "$args[status]", 'type' => PDO::PARAM_INT);

        if (isset($args['created_from']))
            $params[] = array('name' => ':created_from', 'value' => "$args[created_from]", 'type' => PDO::PARAM_INT);
        if (isset($args['created_to']))
            $params[] = array('name' => ':created_to', 'value' => "$args[created_to]", 'type' => PDO::PARAM_INT);

        if (isset($args['author_id']))
            $params[] = array('name' => ':author_id', 'value' => "$args[author_id]", 'type' => PDO::PARAM_INT);
        else
            $params[] = array('name' => ':author_id', 'value' => Yii::app()->user->id, 'type' => PDO::PARAM_INT);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " ep.date_added DESC";
        }

        $sql = "SELECT ep.*, od.title order_title, re.title receiver_title, re.id receiver_id
                FROM {{expenses}} ep
                LEFT JOIN {{orders}} od On od.id = ep.order_id
                LEFT JOIN {{receivers}} re On re.id = ep.receiver
                WHERE ep.deleted=0
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function get_all_group_by_receiver($order_id = 0)
    {

        $sql = "SELECT ep.order_id, ep.receiver, re.title receiver_title, sum(ep.amt) as group_amt
                FROM {{expenses}} ep
                LEFT JOIN {{receivers}} re On re.id = ep.receiver
                WHERE ep.deleted=0 AND ep.author_id=:author_id
                GROUP BY ep.order_id, ep.receiver";
        if ($order_id) $sql .= " HAVING ep.order_id=:order_id";
        $sql .= " ORDER BY group_amt ASC";

        $command = Yii::app()->db->createCommand($sql);

        $author_id = Yii::app()->user->id;
        $command->bindParam(":author_id", $author_id);

        if ($order_id)
            $command->bindParam(":order_id", $order_id);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{expenses}} ep
                WHERE ep.deleted = 0
                $custom";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function sum($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT sum(ep.amt) as total
                FROM {{expenses}} ep
                WHERE ep.deleted = 0
                $custom";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT ep.*, usr.display_name author_name, od.title order_title, re.title receiver_title, re.id receiver_id
                FROM {{expenses}} ep
                LEFT JOIN {{users}} usr On usr.id = ep.author_id
                LEFT JOIN {{orders}} od On od.id = ep.order_id
                LEFT JOIN {{receivers}} re On re.id = ep.receiver
                WHERE ep.id = :id AND ep.deleted = 0";
        $command = Yii::app()->db->createCommand($sql);

        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function add($args)
    {
        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{expenses}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{expenses}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }

    public function sum_expenses($author_id = 0, $receiver = '')
    {
        $sql = "SELECT ep.receiver, re.title receiver_title, ep.status, ep.currency, sum(ep.amt) as total
                FROM {{expenses}} ep
                Inner Join {{receivers}} re On re.id = ep.receiver
                WHERE ep.deleted=0
                GROUP BY ep.receiver,ep.status,ep.currency, ep.author_id
                HAVING ep.author_id=:author_id";

        if ($receiver!='')
            $sql.=' AND receiver like :receiver ';

        $sql .= " ORDER BY ep.receiver,ep.status,ep.currency ";
        $command = Yii::app()->db->createCommand($sql);

        if ($receiver!='')
            $command->bindParam(":receiver", $receiver);

        if ($author_id == 0)
            $author_id = Yii::app()->user->id;

        $command->bindParam(":author_id", $author_id);

        return $command->queryAll();
    }
}