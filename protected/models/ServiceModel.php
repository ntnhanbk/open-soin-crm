<?php

class ServiceModel extends CFormModel
{

    public function __construct()
    {

    }

    private function generate_where_clause($args)
    {
        $where = "";

        if (isset($args['s']) && $args['s'] != "")
            $where .= " AND se.title like :title";

        return $where;
    }

    private function generate_params($args)
    {
        $params = array();

        if (isset($args['s']) && $args['s'] != "")
            $params[] = array('name' => ':title', 'value' => "%$args[s]%", 'type' => PDO::PARAM_STR);

        return $params;
    }

    public function gets($args, $page = 1, $ppp = 20)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        if (isset($args['order_by']) && $args['order_by'] != '') {
            $order_by = " " . $args['order_by'] . ' ' . $args['order_asc'];
        } else {
            $order_by = " se.title ASC";
        }

        $sql = "SELECT se.*
                FROM {{products}} se
                WHERE se.deleted=0 AND se.author_id=:author_id AND se.product_type like 'service'
                $custom
                ORDER BY $order_by";

        if ($page > 0) $sql .= " LIMIT :page,:ppp";

        $command = Yii::app()->db->createCommand($sql);

        $author_id = Yii::app()->user->id;
        $command->bindParam(":author_id", $author_id);

        if ($page > 0) {
            $page = ($page - 1) * $ppp;
            $command->bindParam(":page", $page);
            $command->bindParam(":ppp", $ppp);
        }

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        return $command->queryAll();
    }

    public function counts($args)
    {
        $custom = $this->generate_where_clause($args);
        $params = $this->generate_params($args);

        $sql = "SELECT count(*) as total
                FROM {{products}} se
                WHERE se.deleted = 0 AND se.author_id=:author_id AND se.product_type like 'service'
                $custom";
        $command = Yii::app()->db->createCommand($sql);

        $author_id = Yii::app()->user->id;
        $command->bindParam(":author_id", $author_id);

        foreach ($params as $a)
            $command->bindParam($a['name'], $a['value'], $a['type']);

        $count = $command->queryRow();
        return $count['total'];
    }

    public function get($id)
    {
        $sql = "SELECT se.*, usr.display_name author_name
                FROM {{products}} se
                LEFT JOIN {{users}} usr On usr.id = se.author_id
                WHERE se.id = :id AND se.deleted = 0 AND se.author_id=:author_id AND se.product_type like 'service'";
        $command = Yii::app()->db->createCommand($sql);

        $author_id = Yii::app()->user->id;
        $command->bindParam(":author_id", $author_id);

        $command->bindParam(":id", $id);
        return $command->queryRow();
    }

    public function add($args)
    {
        $args['product_type'] = 'service';

        $field_arr = array();
        $field_holder_arr = array();

        foreach ($args as $k => $v) {
            $field_arr[] = $k;
            $field_holder_arr[] = ':' . $k;
        }
        $field_str = implode(',', $field_arr);
        $field_holder_str = implode(',', $field_holder_arr);

        $sql = "INSERT INTO {{products}} (" . $field_str . ") VALUES(" . $field_holder_str . ")";
        $command = Yii::app()->db->createCommand($sql);

        foreach ($args as $k => $v) {
            $command->bindParam(':' . $k, $args[$k]);
        }

        $command->execute();

        return Yii::app()->db->lastInsertID;
    }

    public function update($args)
    {
        $keys = array_keys($args);
        $custom = '';
        foreach ($keys as $k)
            $custom .= $k . ' = :' . $k . ', ';
        $custom = substr($custom, 0, strlen($custom) - 2);
        $sql = 'update {{products}} set ' . $custom . ' where id = :id';
        $command = Yii::app()->db->createCommand($sql);
        return $command->execute($args);
    }
}