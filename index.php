<?php

// include Yii bootstrap file
$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

ini_set('memory_limit', '128M');

// comment 4 lines below in production server
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 10);
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once($yii);

Yii::createWebApplication($config)->run();